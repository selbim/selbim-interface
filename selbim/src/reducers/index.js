import { combineReducers } from 'redux';
import form from 'redux-form/lib/reducer';
import authReducer from './auth_reducer';
import langReducer from './lang_reducer';
import user from './user_reducer';
import { UNAUTH } from '../actions/actionTypes';

const appReducer = combineReducers({
  auth: authReducer,
  form,
  lang: langReducer,
  user,
});

const rootReducer = (state, action) => {
  if (action.type === UNAUTH.SUCCESS) {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
