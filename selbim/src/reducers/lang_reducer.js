import { DEFAULT_LOCALE, CHANGE_LOCALE } from '../actions/actionTypes';

const initialState = {
  locale: DEFAULT_LOCALE,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CHANGE_LOCALE:
      initialState.locale = action.locale;
      return { ...state, locale: action.locale };
    default:
      return state;
  }
}
