import {
  AUTH,
  UNAUTH,
  INVALIDATE_AUTH_ERROR,
  CAPTCHA_VERIFIED,
  CAPTCHA_LOADED,
  ACCOUNT_ACTIVATION,
  ACCOUNT_ACTIVATION_RESEND,
  RESET_PASSWORD,
  RESET_PASSWORD_COMPLETE,
  CLEAR_AUTH_SUCCESS_STATE,
  INVALIDATE_RESET_PASSWORD,
  CHANGE_PASSWORD,
  INVALIDATE_CHANGE_PASSWORD,
} from '../actions/actionTypes';

const initialState = {
  authenticated: false,
  faultyLoginCount: 0,
  error: '',
  message: null,
  isCaptchaCompleted: true,
  accountActivation: {},
  accountActivationLoading: false,
  accountActivationError: false,
  accountActivationResend: {},
  accountActivationResendLoading: false,
  accountActivationResendError: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CAPTCHA_LOADED:
      return { ...state, isCaptchaCompleted: false };
    case CAPTCHA_VERIFIED:
      return { ...state, isCaptchaCompleted: true };
    case AUTH.SUCCESS:
      initialState.faultyLoginCount = 0;
      return {
        ...state,
        error: '',
        requestSuccess: true,
        authenticated: true,
        faultyLoginCount: initialState.faultyLoginCount,
      };
    case AUTH.FAIL:
      if (action.payload !== '') {
        initialState.faultyLoginCount += 1;
      }
      return {
        ...state,
        requestSuccess: false,
        faultyLoginCount: initialState.faultyLoginCount,
        error: action.payload.response.error,
      };
    case CLEAR_AUTH_SUCCESS_STATE:
      return {
        ...state,
        requestSuccess: false,
      };
    case UNAUTH.SUCCESS:
      return { ...state, authenticated: false, faultyLoginCount: 0 };
    case INVALIDATE_AUTH_ERROR:
      return { ...state, error: '' };
    case ACCOUNT_ACTIVATION.REQUEST:
      return { ...state, accountActivationLoading: true, accountActivationError: false };
    case ACCOUNT_ACTIVATION.SUCCESS:
      return {
        ...state,
        accountActivationError: false,
        accountActivationLoading: false,
        accountActivation: action.payload,
      };
    case ACCOUNT_ACTIVATION.FAIL:
      return {
        ...state,
        accountActivationError: true,
        accountActivationLoading: false,
        accountActivation: action.payload,
      };
    case ACCOUNT_ACTIVATION_RESEND.REQUEST:
      return { ...state, accountActivationResendLoading: true, accountActivationResendError: false };
    case ACCOUNT_ACTIVATION_RESEND.SUCCESS:
      return {
        ...state,
        accountActivationResendError: false,
        accountActivationResendLoading: false,
        accountActivationResend: action.payload,
      };
    case ACCOUNT_ACTIVATION_RESEND.FAIL:
      return {
        ...state,
        accountActivationResendError: true,
        accountActivationResendLoading: false,
        accountActivationResend: action.payload,
      };
    case RESET_PASSWORD.REQUEST:
      return {
        ...state,
        resetPasswordError: false,
        resetPasswordLoading: true,
        resetPasswordSuccess: false,
      };
    case RESET_PASSWORD.SUCCESS:
      return {
        ...state,
        resetPasswordError: false,
        resetPasswordLoading: false,
        resetPasswordSuccess: true,
      };
    case RESET_PASSWORD.FAIL:
      return {
        ...state,
        resetPasswordError: true,
        resetPasswordErrorMessage: action.payload.response.error,
        resetPasswordLoading: false,
        resetPasswordSuccess: false,
      };
    case RESET_PASSWORD_COMPLETE.REQUEST:
      return {
        ...state,
        resetPasswordCompleteError: false,
        resetPasswordCompleteLoading: true,
        resetPasswordCompleteSuccess: false,
      };
    case RESET_PASSWORD_COMPLETE.SUCCESS:
      return {
        ...state,
        resetPasswordCompleteError: false,
        resetPasswordCompleteLoading: false,
        resetPasswordCompleteSuccess: true,
      };
    case RESET_PASSWORD_COMPLETE.FAIL:
      return {
        ...state,
        resetPasswordCompleteError: true,
        resetPasswordCompleteErrorMessage: action.payload.response.error,
        resetPasswordCompleteLoading: false,
        resetPasswordCompleteSuccess: false,
      };
    case INVALIDATE_RESET_PASSWORD:
      return {
        ...state,
        resetPasswordSuccess: false,
        resetPasswordError: false,
        resetPasswordLoading: false,
        resetPasswordErrorMessage: '',
      };
    case CHANGE_PASSWORD.REQUEST:
      return {
        ...state,
        changePasswordError: false,
        changePasswordLoading: true,
        changePasswordSuccess: false,
      };
    case CHANGE_PASSWORD.SUCCESS:
      return {
        ...state,
        changePasswordError: false,
        changePasswordLoading: false,
        changePasswordSuccess: true,
      };
    case CHANGE_PASSWORD.FAIL:
      return {
        ...state,
        changePasswordError: true,
        changePasswordErrorMessage: action.payload.response.error,
        changePasswordLoading: false,
        changePasswordSuccess: false,
      };
    case INVALIDATE_CHANGE_PASSWORD:
      return {
        ...state,
        changePasswordError: false,
        changePasswordErrorMessage: '',
        changePasswordLoading: false,
        changePasswordSuccess: false,
      };
    default:
      return state;
  }
}
