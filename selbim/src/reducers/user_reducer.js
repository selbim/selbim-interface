import { AUTH } from '../actions/actionTypes';

const initialState = {
  email: '',
  permissions: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case AUTH.SUCCESS:
      return {
        ...state,
        email: action.payload.user.email,
        firstName: action.payload.user.firstname,
        lastName: action.payload.user.lastname,
        userID: action.payload.user.userID,
        authenticationKey: action.payload.user.authenticationKey,
        lastLoginDate: action.payload.user.lastLoginDate,
        permissions: action.payload.user.permissions,
      };
    default:
      return state;
  }
}
