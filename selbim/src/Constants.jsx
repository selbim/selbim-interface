import React from 'react';
import { FormattedMessage } from 'react-intl';

export const offerDialogUrl = 'http://rtgz.veloxity.net/scratch/dialog_trial.html';

export const routes = {
  '/apps': <FormattedMessage id="routes.apps" />,
  '/dashboard': <FormattedMessage id="routes.dashboard" />,
  '/insights': <FormattedMessage id="routes.insights" />,
  '/insights/maps': <FormattedMessage id="insights.maps" />,
  '/insights/competitors': <FormattedMessage id="insights.competitors" />,
  '/insights/trends': <FormattedMessage id="insights.trends" />,
  '/insights/branch-activity': <FormattedMessage id="insights.branchactivity" />,
  '/insights/app-activity': <FormattedMessage id="insights.appactivity" />,
  '/tv': <FormattedMessage id="routes.tv" />,
  '/engage': <FormattedMessage id="routes.engage" />,
  '/monitor': <FormattedMessage id="routes.monitor" />,
  '/documentation': <FormattedMessage id="routes.documentation" />,
  '/getsdk': <FormattedMessage id="routes.getsdk" />,
  '/apps/new': <FormattedMessage id="routes.newapplication" />,
  '/login': <FormattedMessage id="routes.login" />,
  '/logout': <FormattedMessage id="routes.logout" />,
  '/settings': <FormattedMessage id="routes.settings" />,
  '/recover': <FormattedMessage id="routes.recover" />,
  '/404': <FormattedMessage id="routes.404" />,
  '/500': <FormattedMessage id="routes.500" />,
  '/check': <FormattedMessage id="routes.check" />,
  '/terms': <FormattedMessage id="routes.terms" />,
  '/privacy': <FormattedMessage id="routes.privacy" />,
  '/': <FormattedMessage id="routes.home" />,
};

export function getRoutes(routeName, intl) {
  const routeList = {
    '/apps': intl.formatMessage({ id: 'routes.apps' }),
    '/dashboard': intl.formatMessage({ id: 'routes.dashboard' }),
    '/insights': intl.formatMessage({ id: 'routes.insights' }),
    '/insights/maps': intl.formatMessage({ id: 'insights.maps' }),
    '/insights/competitors': intl.formatMessage({ id: 'insights.competitors' }),
    '/insights/trends': intl.formatMessage({ id: 'insights.trends' }),
    '/insights/branch-activity': intl.formatMessage({ id: 'insights.branchactivity' }),
    '/insights/app-activity': intl.formatMessage({ id: 'insights.appactivity' }),
    '/tv': intl.formatMessage({ id: 'routes.tv' }),
    '/engage': intl.formatMessage({ id: 'routes.engage' }),
    '/monitor': intl.formatMessage({ id: 'routes.monitor' }),
    '/documentation': intl.formatMessage({ id: 'routes.documentation' }),
    '/getsdk': intl.formatMessage({ id: 'routes.getsdk' }),
    '/apps/new': intl.formatMessage({ id: 'routes.newapplication' }),
    '/login': intl.formatMessage({ id: 'routes.login' }),
    '/logout': intl.formatMessage({ id: 'routes.logout' }),
    '/settings': intl.formatMessage({ id: 'routes.settings' }),
    '/recover': intl.formatMessage({ id: 'routes.recover' }),
    '/404': intl.formatMessage({ id: 'routes.404' }),
    '/500': intl.formatMessage({ id: 'routes.500' }),
    '/check': intl.formatMessage({ id: 'routes.check' }),
    '/terms': intl.formatMessage({ id: 'routes.terms' }),
    '/privacy': intl.formatMessage({ id: 'routes.privacy' }),
    '/': intl.formatMessage({ id: 'routes.home' }),
  };
  for (const route in routeList) {
    if (routeName.startsWith(route)) return routeList[route];
  }
}

export const PERMISSIONS = {
  READ_MAIN_DASHBOARD: 'Read Main Dashboard',
  MANAGE_APPS: 'Manage Apps',
  READ_APPS: 'Read Apps',
  MANAGE_CAMPAIGNS: 'Manage Campaigns',
  READ_CAMPAIGNS: 'Read Campaigns',
  READ_RTCO_STATISTICS: 'Read RTCO Statististics',
  MANAGE_SYSTEM: 'Manage System',
  READ_INSIGHT: 'Read Insight',
  WATCH_REGISTRATION_TV: 'Watch Registration TV',
};
