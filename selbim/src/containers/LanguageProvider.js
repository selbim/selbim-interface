import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import tr from 'react-intl/locale-data/tr';
import localeData from '../locales/data.json';

addLocaleData([...en, ...tr]);

const LanguageProvider = ({ locale, children }) =>
  (<IntlProvider locale={locale} key={locale} messages={localeData[locale]}>
    {React.Children.only(children)}
  </IntlProvider>);

LanguageProvider.defaultProps = {
  locale: '',
};

LanguageProvider.propTypes = {
  locale: PropTypes.string,
  children: PropTypes.element.isRequired,
};

const mapStateToProps = function (state) {
  return {
    locale: state.lang.locale,
  };
};

export default connect(mapStateToProps)(LanguageProvider);
