import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import MainRouter from '../MainRouter';
import LanguageProvider from './LanguageProvider';

const Root = ({ store }) => (
  <Provider store={store}>
    <LanguageProvider>
      <MainRouter />
    </LanguageProvider>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
};

export default Root;
