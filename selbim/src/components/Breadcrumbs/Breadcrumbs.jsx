import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import Breadcrumb from 'react-bootstrap/lib/Breadcrumb';
import { routes } from '../../Constants';

const findRouteName = url => routes[url];

const getPaths = (pathname) => {
  const paths = ['/'];

  if (pathname === '/') return paths;

  pathname.split('/').reduce((prev, curr) => {
    const currPath = `${prev}/${curr}`;
    paths.push(currPath);
    return currPath;
  });
  return paths;
};

const BreadcrumbsItem = ({ match }) => {
  const routeName = findRouteName(match.url);
  if (routeName) {
    return match.isExact
      ? <Breadcrumb.Item active>{routeName}</Breadcrumb.Item>
      : <LinkContainer to={match.url || ''}>
        <Breadcrumb.Item>{routeName}</Breadcrumb.Item>
      </LinkContainer>;
  }
  return null;
};

const Breadcrumbs = ({ location: { pathname } }) => {
  const paths = getPaths(pathname);
  return (
    <Breadcrumb>
      {paths.map(p => <Route key={p} path={p} component={BreadcrumbsItem} />)}
    </Breadcrumb>
  );
};

Breadcrumbs.propTypes = {
  location: PropTypes.object.isRequired,
};

BreadcrumbsItem.propTypes = {
  match: PropTypes.object.isRequired,
};

export default props => <Route path="/:path" component={Breadcrumbs} {...props} />;
