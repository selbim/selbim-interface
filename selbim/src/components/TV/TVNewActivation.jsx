import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Map, TileLayer } from 'react-leaflet';
import L from 'leaflet';
import Col from 'react-bootstrap/lib/Col';
import MarkerClusterGroup from 'react-leaflet-markercluster';

let webSocket;
let counter;
let timerId;
let timeCounter;

const customMarker = L.icon({
  iconUrl: '/img/marker_android.svg',
  iconSize: [40, 40],
  iconAnchor: [20, 40],
});

const Notification = ({ status, message }) => {
  let bgColor;
  switch (status) {
    case 'error':
      bgColor = 'gray';
      break;
    case 'loading':
      bgColor = '#eaab17';
      break;
    case 'success':
      bgColor = 'red';
      break;
    default:
      bgColor = 'black';
  }
  return (
    <span
      className="pull-right"
      style={{
        position: 'absolute',
        zIndex: 1,
        right: '20px',
        backgroundColor: bgColor,
        color: 'white',
        paddingRight: '10px',
        paddingLeft: '10px',
        fontSize: '16px',
      }}>
      {message}
    </span>
  );
};

Notification.propTypes = {
  status: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

class TVNewActivation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 41.023945,
      lng: 29.013691,
      devices: [],
      startDeviceCount: 0,
      status: 'loading',
      statusMessage: this.props.intl.formatMessage({ id: 'tv.onconnecting' }),
    };
    this.onConnectionOpen = this.onConnectionOpen.bind(this);
    this.onConnectionClose = this.onConnectionClose.bind(this);
    this.onConnectionError = this.onConnectionError.bind(this);
    this.onConnectionMessage = this.onConnectionMessage.bind(this);
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    webSocket = new WebSocket(`wss://dvgw.veloxity.net/vlx-dp/vlx-tv/devices/news2/${token}/`);
    webSocket.onopen = this.onConnectionOpen;
    webSocket.onclose = this.onConnectionClose;
    webSocket.onerror = this.onConnectionError;
    webSocket.onmessage = this.onConnectionMessage;
  }

  componentWillUnmount() {
    if (webSocket) webSocket.close();
  }

  onConnectionOpen() {
    if (this.refNewActivation) {
      this.setState({
        status: 'success',
        statusMessage: (
          <span>
            {this.props.intl.formatMessage({ id: 'tv.onconnected' })} <i className="fa fa-circle" />
          </span>
        ),
      });
    }
    this.keepAlive();
  }

  onConnectionClose() {
    if (this.refNewActivation) {
      this.setState({ status: 'error', statusMessage: this.props.intl.formatMessage({ id: 'tv.disconnected' }) });
    }
    if (timeCounter) timeCounter.stop();
    if (timerId) clearTimeout(timerId);
  }

  onConnectionError() {
    if (this.refNewActivation) {
      this.setState({ status: 'error', statusMessage: this.props.intl.formatMessage({ id: 'tv.onerror' }) });
    }
  }

  onConnectionMessage(event) {
    if (event.data === 'Y') {
      return;
    }
    const json = JSON.parse(event.data);
    if (json.offset) {
      if (this.refNewActivation) this.setState({ startDeviceCount: json.offset });
      counter = $('.clock').FlipClock(this.state.startDeviceCount, {
        clockFace: 'Counter',
      });
      timeCounter = $('.counter').FlipClock(0);
    } else {
      this.incrementCounter(json.devices.length);
      if (this.refNewActivation) {
        this.setState({ devices: this.state.devices.concat(json) });
      }
    }
  }

  keepAlive() {
    if (webSocket.readyState === webSocket.OPEN) {
      webSocket.send('X');
    }
    timerId = setTimeout(() => this.keepAlive(), 10000);
  }

  incrementCounter(times) {
    for (let i = 0; i < times; i += 1) {
      counter.increment();
    }
  }

  render() {
    const { intl } = this.props;
    const data = [];

    for (let i = 0; i < this.state.devices.length; i += 1) {
      this.state.devices[i].devices.map((item) => {
        // if (item.lt === 0 && item.ln === 0) {
        //  return;
        // }
        data.push({
          lat: item.lt,
          lng: item.ln,
          popup: `<div>
                    <ul class="list-group">
                      <li class="list-group-item" style=${item.dt === 'IOS'
                        ? 'color:white;background-color:black'
                        : 'color:white;background-color:green'}>
                        ${item.dt}
                      </li>
                      <li class="list-group-item">${intl.formatMessage({
                        id: 'tv.model',
                      })}: ${item.dm}</li>
                      <li class="list-group-item">${intl.formatMessage({
                        id: 'tv.version',
                      })}: ${item.os}</li>
                      <li class="list-group-item">${intl.formatMessage({
                        id: 'tv.operator',
                      })}: ${item.mo}</li>
                      <li class="list-group-item">${intl.formatMessage({
                        id: 'tv.deviceid',
                      })}: ${item.d}</li>
                    </ul>
                  </div>`,
          options: {
            icon:
              item.dt === 'IOS'
                ? L.icon({
                  iconUrl: '/img/marker_ios.svg',
                  iconSize: [40, 40],
                  iconAnchor: [20, 40],
                })
                : customMarker,
          },
        });
      });
    }

    return (
      <div>
        <Col lg={12} md={12} sm={12} xs={12} className="mb-lg">
          <Notification status={this.state.status} message={this.state.statusMessage} />
          <span className="new-register-label">
            <FormattedMessage id="tv.devicecount" />
          </span>
          <span className="device-counter-position clock" />
          <Map className="markercluster-map" center={[18.184465, 20.296939]} zoom={2} maxZoom={18}>
            <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
            <MarkerClusterGroup
              ref={(c) => {
                this.refNewActivation = c;
              }}
              markers={data}
              wrapperOptions={{ enableDefaultStyle: true }} />
          </Map>
          <span className="time-counter-position counter" />
        </Col>
      </div>
    );
  }
}

TVNewActivation.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(TVNewActivation);
