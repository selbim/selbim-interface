import React from 'react';
import Col from 'react-bootstrap/lib/Col';
import ProfileTable from './ProfileTable';
import AppActivityMap from './AppActivityMap';
import SessionDuration from './SessionDuration';

const TVAppActivity = () => (
  <div>
    <Col lg={12} md={12} sm={12} xs={12} className="mb-lg">
      <AppActivityMap />
      <SessionDuration />
      <ProfileTable />
    </Col>
  </div>
);

export default TVAppActivity;
