import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import { withRouter } from 'react-router-dom';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import Tab from 'react-bootstrap/lib/Tab';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';
import TVNewActivation from './TVNewActivation';
import TVAppActivity from './TVAppActivity';

const TVIndex = ({ intl }) => (
  <ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'sidebar.items.tv' })} />
    <Tab.Container id="tv" defaultActiveKey="newactivation">
      <Grid fluid>
        <Row>
          <Col lg={12} md={12} sm={12} xs={12}>
            <Nav bsStyle="pills" style={{ float: 'left' }} className="mb-lg pl-lg tv-nav">
              <NavItem role="button" eventKey="newactivation" className="tv-nav">
                <FormattedMessage id="tv.newactivation" />
              </NavItem>
              <NavItem role="button" eventKey="appactivity" className="tv-nav">
                <FormattedMessage id="tv.appactivity" />
              </NavItem>
            </Nav>
            <Tab.Content animation unmountOnExit mountOnEnter>
              <Tab.Pane eventKey="newactivation">
                <TVNewActivation />
              </Tab.Pane>
              <Tab.Pane eventKey="appactivity">
                <TVAppActivity />
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Grid>
    </Tab.Container>
  </ContentWrapper>
);

TVIndex.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(withRouter(TVIndex));
