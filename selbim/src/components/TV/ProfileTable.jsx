import React from 'react';
import isEmpty from 'lodash/isEmpty';
import map from 'lodash/map';
import { injectIntl, FormattedMessage } from 'react-intl';
import Col from 'react-bootstrap/lib/Col';
import Table from 'react-bootstrap/lib/Table';

let sessionWebSocket;
let timerId;

class ProfileTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profiles: [] };
    this.onConnectionOpen = this.onConnectionOpen.bind(this);
    this.onConnectionClose = this.onConnectionClose.bind(this);
    this.onConnectionMessage = this.onConnectionMessage.bind(this);
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    sessionWebSocket = new WebSocket(`wss://dvgw.veloxity.net/vlx-dp/vlx-tv/devices/profiles/${token}/`);
    sessionWebSocket.onopen = this.onConnectionOpen;
    sessionWebSocket.onclose = this.onConnectionClose;
    sessionWebSocket.onmessage = this.onConnectionMessage;
  }

  componentWillUnmount() {
    if (sessionWebSocket) sessionWebSocket.close();
  }

  onConnectionMessage(event) {
    if (event.data === 'Y') {
      return;
    }
    const json = JSON.parse(event.data);

    if (json.message) {
      return;
    }
    this.setState({ profiles: json.profiles });
  }

  onConnectionOpen() {
    this.keepAlive();
  }

  onConnectionClose() {
    if (timerId) clearTimeout(timerId);
  }

  keepAlive() {
    if (sessionWebSocket.readyState === sessionWebSocket.OPEN) {
      sessionWebSocket.send('X');
    }
    timerId = setTimeout(() => this.keepAlive(), 10000);
  }

  render() {
    const topProfiles = this.state.profiles.slice(0, 10);
    return (
      <Col lg={3} md={12} sm={12} xs={12} className="mb-lg">
        <div className="panel panel b" style={{ minHeight: '500px' }}>
          <div className="panel-heading text-center" style={{ fontSize: '16px' }}>
            <FormattedMessage id="tv.table.deviceprofiles" />
          </div>
          <div className="panel-body">
            <Table className="table-fixed" striped bordered condensed hover responsive>
              <thead>
                <tr>
                  <th className="text-center">
                    <FormattedMessage id="tv.table.segments" />
                  </th>
                  <th className="text-center">
                    <FormattedMessage id="tv.table.count" />
                  </th>
                </tr>
              </thead>
              {isEmpty(topProfiles) ? (
                <tbody style={{ width: '100%' }}>
                  <tr key={Math.random()}>
                    <td colSpan="2" className="text-center">
                      <div style={{ minHeight: '320px', paddingTop: '38%' }}>
                        <i className="fa fa-inbox fa-2x" />
                        <br />
                        <strong>
                          <FormattedMessage id="tv.devicelist.empty" />
                        </strong>
                      </div>
                    </td>
                  </tr>
                </tbody>
              ) : (
                <tbody style={{ width: '100%' }}>
                  {map(topProfiles, profile => (
                    <tr key={Math.random()}>
                      <td>{profile.name}</td>
                      <td>{profile.value}</td>
                    </tr>
                  ))}
                </tbody>
              )}
            </Table>
          </div>
        </div>
      </Col>
    );
  }
}

export default injectIntl(ProfileTable);
