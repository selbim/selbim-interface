import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import Col from 'react-bootstrap/lib/Col';
import MultipleLineChart from '../Widgets/MultipleLineChart';

let sessionWebSocket;
let timerId;

class ProfileTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { realtimeDuration: [], realtimeLength: [], dailyAvg: [], weeklyAvg: [] };
    this.onConnectionOpen = this.onConnectionOpen.bind(this);
    this.onConnectionClose = this.onConnectionClose.bind(this);
    this.onConnectionMessage = this.onConnectionMessage.bind(this);
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    sessionWebSocket = new WebSocket(`wss://dvgw.veloxity.net/vlx-dp/vlx-tv/apps/activity/stats/${token}/`);
    sessionWebSocket.onopen = this.onConnectionOpen;
    sessionWebSocket.onclose = this.onConnectionClose;
    sessionWebSocket.onmessage = this.onConnectionMessage;
  }

  componentWillUnmount() {
    if (sessionWebSocket) sessionWebSocket.close();
  }

  onConnectionMessage(event) {
    if (event.data === 'Y') {
      return;
    }
    const json = JSON.parse(event.data);
    if (json.da) {
      for (let i = 0; i < 2; i += 1) this.setState({ dailyAvg: this.state.dailyAvg.concat(json.da) });
    }
    if (json.wa) {
      for (let i = 0; i < 2; i += 1) this.setState({ weeklyAvg: this.state.weeklyAvg.concat(json.wa) });
    }
    if (json.av) {
      this.setState({
        realtimeDuration: this.state.realtimeDuration.concat(Math.round(json.av).toFixed(2) * 0.001),
        realtimeLength: this.state.realtimeLength.concat(''),
      });
      const realtimeLength = this.state.realtimeDuration.length;
      for (let i = 1; i < realtimeLength; i += 1) {
        this.setState({
          dailyAvg: this.state.dailyAvg.concat(this.state.dailyAvg[0]),
          weeklyAvg: this.state.weeklyAvg.concat(this.state.weeklyAvg[0]),
        });
      }
    }
  }

  onConnectionOpen() {
    this.keepAlive();
  }

  onConnectionClose() {
    if (timerId) clearTimeout(timerId);
  }

  keepAlive() {
    if (sessionWebSocket.readyState === sessionWebSocket.OPEN) {
      sessionWebSocket.send('X');
    }
    timerId = setTimeout(() => this.keepAlive(), 10000);
  }

  render() {
    const sessionDurationData = {
      labels: this.state.realtimeLength,
      datasets: [
        {
          label: 'Realtime (Avg.)',
          fill: false,
          lineTension: 0.1,
          backgroundColor: '#4472c4',
          borderColor: '#4472c4',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: '#4472c4',
          pointBackgroundColor: '#4472c4',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: '#4472c4',
          pointHoverBorderColor: '#4472c4',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: this.state.realtimeDuration,
        },
        {
          label: 'Daily (Avg.)',
          fill: false,
          lineTension: 0.1,
          backgroundColor: '#ed7d31',
          borderColor: '#ed7d31',
          borderCapStyle: 'butt',
          borderDash: [10, 5],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: '#ed7d31',
          pointBackgroundColor: '#ed7d31',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: '#ed7d31',
          pointHoverBorderColor: '#ed7d31',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: this.state.dailyAvg,
        },
        {
          label: 'Weekly (Avg.)',
          fill: false,
          lineTension: 0.1,
          backgroundColor: '#a5a5a5',
          borderColor: '#a5a5a5',
          borderCapStyle: 'butt',
          borderDash: [10, 5],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: '#a5a5a5',
          pointBackgroundColor: '#a5a5a5',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: '#a5a5a5',
          pointHoverBorderColor: '#a5a5a5',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: this.state.weeklyAvg,
        },
      ],
    };

    return (
      <Col lg={3} md={12} sm={12} xs={12} className="mb-lg">
        <MultipleLineChart
          title={this.props.intl.formatMessage({ id: 'tv.sessionduration' })}
          data={sessionDurationData}
          height={200}
          options={{
            scales: {
              yAxes: [
                {
                  scaleLabel: {
                    display: true,
                    labelString: 'Seconds',
                  },
                },
              ],
            },
            maintainAspectRatio: false,
            responsive: true,
            legend: {
              position: 'bottom',
            },
          }} />
      </Col>
    );
  }
}

ProfileTable.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(ProfileTable);
