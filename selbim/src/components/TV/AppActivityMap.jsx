import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Map, TileLayer } from 'react-leaflet';
import L from 'leaflet';
import Col from 'react-bootstrap/lib/Col';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import Humanizer from '../../utils/Humanizer';

let webSocket;
let counter;
let timerId;
let timeCounter;
let h;

const customMarker = L.icon({
  iconUrl: '/img/exit_marker.svg',
  iconSize: [40, 40],
  iconAnchor: [20, 40],
});

const Notification = ({ status, message }) => {
  let bgColor;
  switch (status) {
    case 'error':
      bgColor = 'gray';
      break;
    case 'loading':
      bgColor = '#eaab17';
      break;
    case 'success':
      bgColor = 'red';
      break;
    default:
      bgColor = 'black';
  }
  return (
    <span
      className="pull-right"
      style={{
        position: 'absolute',
        zIndex: 1,
        right: '20px',
        backgroundColor: bgColor,
        color: 'white',
        paddingRight: '10px',
        paddingLeft: '10px',
        fontSize: '16px',
      }}>
      {message}
    </span>
  );
};

Notification.propTypes = {
  status: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

class AppActivityMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 41.023945,
      lng: 29.013691,
      devices: [],
      startDeviceCount: 0,
      status: 'loading',
      statusMessage: this.props.intl.formatMessage({ id: 'tv.onconnecting' }),
    };
    this.onConnectionOpen = this.onConnectionOpen.bind(this);
    this.onConnectionClose = this.onConnectionClose.bind(this);
    this.onConnectionError = this.onConnectionError.bind(this);
    this.onConnectionMessage = this.onConnectionMessage.bind(this);
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    h = new Humanizer(this.props.locale);
    webSocket = new WebSocket(`wss://dvgw.veloxity.net/vlx-dp/vlx-tv/apps/activity/${token}/`);
    webSocket.onopen = this.onConnectionOpen;
    webSocket.onclose = this.onConnectionClose;
    webSocket.onerror = this.onConnectionError;
    webSocket.onmessage = this.onConnectionMessage;
  }

  componentWillUnmount() {
    if (webSocket) webSocket.close();
  }

  onConnectionOpen() {
    if (this.refAppActivity) {
      this.setState({
        status: 'success',
        statusMessage: (
          <span>
            {this.props.intl.formatMessage({ id: 'tv.onconnected' })} <i className="fa fa-circle" />
          </span>
        ),
      });
    }
    this.keepAlive();
  }

  onConnectionClose() {
    if (this.refAppActivity) {
      this.setState({ status: 'error', statusMessage: this.props.intl.formatMessage({ id: 'tv.disconnected' }) });
    }
    if (timeCounter) timeCounter.stop();
    if (timerId) clearTimeout(timerId);
  }

  onConnectionError() {
    if (this.refAppActivity) {
      this.setState({ status: 'error', statusMessage: this.props.intl.formatMessage({ id: 'tv.onerror' }) });
    }
  }

  onConnectionMessage(event) {
    if (event.data === 'Y') {
      return;
    }
    const json = JSON.parse(event.data);
    if (json.offset) {
      if (this.refAppActivity) {
        this.setState({ startDeviceCount: json.offset });
      }

      counter = $('.clock').FlipClock(this.state.startDeviceCount, {
        clockFace: 'Counter',
      });
      timeCounter = $('.counter').FlipClock(0);
    } else {
      this.incrementCounter(json.devices.length);
      if (this.refAppActivity) {
        this.setState({ devices: this.state.devices.concat(json) });
      }
    }
  }

  keepAlive() {
    if (webSocket.readyState === webSocket.OPEN) {
      webSocket.send('X');
    }
    timerId = setTimeout(() => this.keepAlive(), 10000);
  }

  incrementCounter(times) {
    for (let i = 0; i < times; i += 1) {
      counter.increment();
    }
  }

  render() {
    const { intl } = this.props;
    const data = [];

    for (let i = 0; i < this.state.devices.length; i += 1) {
      /*eslint-disable*/
      this.state.devices[i].devices.map(item => {
        /*eslint-enable*/
        // if (item.lt === 0 && item.ln === 0) {
        //  return;
        // }
        let popup = null;
        if (item.a === 'enter') {
          popup = `<div>
          <ul class="list-group">
            <li class="list-group-item" style=${item.a === 'enter'
              ? 'color:white;background-color:green'
              : 'color:white;background-color:red'}>
              ${item.a.toUpperCase()}
            </li>
            <li class="list-group-item">${intl.formatMessage({
              id: 'tv.connection',
            })} ${item.c}</li>
            <li class="list-group-item">${intl.formatMessage({
              id: 'tv.uid',
            })} ${item.uid}</li>
            <li class="list-group-item">${intl.formatMessage({
              id: 'tv.deviceid',
            })}: ${item.d}</li>
          </ul>
        </div>`;
        } else {
          popup = `<div>
                  <ul class="list-group">
                    <li class="list-group-item" style=${item.a === 'enter'
                      ? 'color:white;background-color:green'
                      : 'color:white;background-color:red'}>
                      ${item.a.toUpperCase()}
                    </li>
                    <li class="list-group-item">${intl.formatMessage({
                      id: 'tv.duration',
                    })} ${h.humanize(item.sd, { round: true })}</li>
                    <li class="list-group-item">${intl.formatMessage({
                      id: 'tv.connection',
                    })} ${item.c}</li>
                    <li class="list-group-item">${intl.formatMessage({
                      id: 'tv.uid',
                    })} ${item.uid}</li>
                    <li class="list-group-item">${intl.formatMessage({
                      id: 'tv.deviceid',
                    })}: ${item.d}</li>
                  </ul>
                </div>`;
        }
        data.push({
          lat: item.lt,
          lng: item.ln,
          popup,
          options: {
            icon:
              item.a === 'enter'
                ? L.icon({
                  iconUrl: '/img/marker_android.svg',
                  iconSize: [40, 40],
                  iconAnchor: [20, 40],
                })
                : customMarker,
          },
        });
      });
    }
    return (
      <Col lg={9} md={12} sm={12} xs={12} className="mb-lg">
        <Notification status={this.state.status} message={this.state.statusMessage} />
        <span className="new-register-label">
          <FormattedMessage id="tv.activitycount" />
        </span>
        <span className="device-counter-position clock" />
        <Map
          className="markercluster-map"
          center={[18.184465, 20.296939]}
          zoom={2}
          maxZoom={18}
          style={{ minHeight: '815px' }}>
          <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
          <MarkerClusterGroup
            ref={(c) => {
              this.refAppActivity = c;
            }}
            markers={data}
            wrapperOptions={{ enableDefaultStyle: true }} />
        </Map>
        <span className="time-counter-position counter" />
      </Col>
    );
  }
}

function mapStateToProps(state) {
  return {
    locale: state.lang.locale,
  };
}

AppActivityMap.propTypes = {
  intl: PropTypes.object.isRequired,
  locale: PropTypes.string.isRequired,
};

export default injectIntl(connect(mapStateToProps, null)(AppActivityMap));
