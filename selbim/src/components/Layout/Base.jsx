import React from 'react';
import PropTypes from 'prop-types';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { injectIntl } from 'react-intl';
import { Helmet } from 'react-helmet';
import Header from './Header';
import Sidebar from './Sidebar';
import Footer from './Footer';
import { getRoutes } from '../../Constants';

const Base = ({ children, intl }) => (
  <div className="wrapper">
    <div className="application">
      <Helmet defaultTitle="Selbim" titleTemplate="Selbim - %s">
        <meta charSet="utf-8" />
        <title>{getRoutes(children.props.location.pathname, intl)}</title>
      </Helmet>
    </div>

    <Header />
    <Sidebar />
    <CSSTransitionGroup
      component="section"
      transitionName="rag-fadeIn"
      transitionEnterTimeout={500}
      transitionLeaveTimeout={500}>
      {React.cloneElement(children, {
        key: children.props.location.pathname,
      })}
    </CSSTransitionGroup>
    <Footer />
  </div>
);

Base.propTypes = {
  children: PropTypes.element.isRequired,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Base);
