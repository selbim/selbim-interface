import React from 'react';
import PropTypes from 'prop-types';

const ContentWrapper = ({ unwrap, children }) => (
  <div className="content-wrapper">
    {unwrap ? <div className="unwrap">children</div> : children}
  </div>
);

ContentWrapper.defaultProps = {
  unwrap: false,
};

ContentWrapper.propTypes = {
  children: PropTypes.array.isRequired,
  unwrap: PropTypes.bool,
};

export default ContentWrapper;
