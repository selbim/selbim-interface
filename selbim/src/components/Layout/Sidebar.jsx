import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import includes from 'lodash/includes';
import { Link, withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { PERMISSIONS } from '../../Constants';

class Sidebar extends React.Component {
  navigator(route) {
    this.props.history.push(route);
  }

  render() {
    const { permissions } = this.props;
    return (
      <aside className="aside">
        {/* START Sidebar (left) */}
        <div className="aside-inner">
          <nav className="sidebar">
            {/* START sidebar nav */}
            <ul className="nav">
              {/* Iterates over all sidebar items */}
              <li className="nav-heading ">
                <span>
                  <FormattedMessage id="sidebar.items.title" />
                </span>
              </li>

              <li>
                <Link to="/dashboard">
                  <em className="icon-speedometer" />
                  <span>
                    <FormattedMessage id="sidebar.items.dashboard" />
                  </span>
                </Link>
              </li>

              <li>
                <Link to="/apps">
                  <em className="icon-screen-smartphone" />
                  <span>
                    <FormattedMessage id="sidebar.items.myapps" />
                  </span>
                </Link>
              </li>

              {includes(permissions, PERMISSIONS.WATCH_REGISTRATION_TV)
                ? <li>
                  <Link to="/tv">
                    <em className="icon-screen-desktop" />
                    <span>
                      <FormattedMessage id="sidebar.items.tv" />
                    </span>
                  </Link>
                </li>
                : null}

              {includes(permissions, PERMISSIONS.READ_INSIGHT)
                ? <li>
                  <Link to="/insights">
                    <em className="icon-graph" />
                    <span>
                      <FormattedMessage id="sidebar.items.insights" />
                    </span>
                  </Link>
                </li>
                : null}
            </ul>
            {/* END sidebar nav */}

            {/* START documentation nav */}
            <ul className="nav">
              <li className="nav-heading ">
                <span>
                  <FormattedMessage id="sidebar.items.integration" />
                </span>
              </li>

              <li>
                <Link to="/documentation">
                  <em className="icon-doc" />
                  <span>
                    <FormattedMessage id="sidebar.items.documentation" />
                  </span>
                </Link>
              </li>

              <li>
                <Link to="/getsdk">
                  <em className="icon-arrow-down-circle" />
                  <span>
                    <FormattedMessage id="sidebar.items.getsdk" />
                  </span>
                </Link>
              </li>
              {/* END documentation nav */}
            </ul>
          </nav>
        </div>
        {/* END Sidebar (left) */}
      </aside>
    );
  }
}

Sidebar.propTypes = {
  permissions: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    permissions: state.user.permissions,
  };
}

export default withRouter(connect(mapStateToProps)(Sidebar));
