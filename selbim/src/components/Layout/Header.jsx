import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import withStorage from '../../hoc/withStorage';
import initScreenfull from '../Common/fullscreen';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.initSidebar();
  }

  componentDidMount() {
    initScreenfull();
  }

  initSidebar() {
    if (!this.props.isSidebarOpen) {
      document.body.classList.toggle('aside-collapsed');
    }
  }

  render() {
    const { isSidebarOpen, setStorage, email } = this.props;
    const ddAlertTitle = (
      <span>
        <em className="icon-settings" />
      </span>
    );
    return (
      <header className="topnavbar-wrapper">
        {/* START Top Navbar */}
        <nav className="navbar topnavbar">
          {/* START navbar header */}
          <div className="navbar-header">
            <Link to={'/'} className="navbar-brand">
              <div className="brand-logo">
                SELBIM
              </div>
              <div className="brand-logo-collapsed">
               SELBIM
              </div>
            </Link>
          </div>
          {/* END navbar header */}
          {/* START Nav wrapper */}
          <div className="nav-wrapper">
            {/* START Left navbar */}
            <ul className="nav navbar-nav">
              <li>
                <a
                  href="#"
                  onClick={() => {
                    setStorage('isSidebarOpen', !isSidebarOpen);
                    document.body.classList.toggle('aside-collapsed');
                  }}
                  className="hidden-xs">
                  <em className="fa fa-navicon" />
                </a>
                <a
                  href="#"
                  className="visible-xs sidebar-toogle"
                  onClick={() => {
                    setStorage('isSidebarOpen', !isSidebarOpen);
                    document.body.classList.add('aside-collapsed');
                    document.body.classList.toggle('aside-toggled');
                  }}>
                  <em className="fa fa-navicon" />
                </a>
              </li>
            </ul>
            {/* END Left navbar */}
            {/* START Right Navbar */}
            <ul className="nav navbar-nav navbar-right">
              <li>
                <p className="navbar-text">{email}</p>
              </li>
              {/* Fullscreen (only desktops) */}
              <li className="visible-lg">
                <a href="#" data-toggle-fullscreen="">
                  <em className="fa fa-expand" />
                </a>
              </li>
              {/* START Alert menu */}
              <NavDropdown noCaret eventKey={3} title={ddAlertTitle} id="basic-nav-dropdown">
                <LinkContainer to="/settings">
                  <MenuItem className="animated flipInX" eventKey={3.1}>
                    <FormattedMessage id="header.menu.settings" />
                  </MenuItem>
                </LinkContainer>
                <MenuItem divider />
                <LinkContainer to="/logout">
                  <MenuItem className="animated flipInX" eventKey={3.2}>
                    <FormattedMessage id="header.menu.logout" />
                  </MenuItem>
                </LinkContainer>
              </NavDropdown>
              {/* END Alert menu */}
            </ul>
            {/* END Right Navbar */}
          </div>
          {/* END Nav wrapper */}
        </nav>
        {/* END Top Navbar */}
      </header>
    );
  }
}

function mapStateToProps(state) {
  return {
    email: state.user.email,
  };
}

Header.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
  setStorage: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, null)(withStorage('isSidebarOpen', true)(Header));
