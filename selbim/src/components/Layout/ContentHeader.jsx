import React from 'react';
import PropTypes from 'prop-types';
import LocaleToggle from '../LocaleToggle/LocaleToggle';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';

const ContentHeader = ({ title, titleSmall }) => (
  <div className="content-heading">
    <div className="pull-right">
      <LocaleToggle />
    </div>
    {title}
    {titleSmall ? <small>{titleSmall}</small> : <small />}
    <Breadcrumbs />
  </div>
);

ContentHeader.defaultProps = {
  titleSmall: '',
};

ContentHeader.propTypes = {
  title: PropTypes.string.isRequired,
  titleSmall: PropTypes.string,
};

export default ContentHeader;
