import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Helmet } from 'react-helmet';
import { getRoutes } from '../../Constants';

const BasePage = ({ children, intl }) => (
  <div className="wrapper login-background-animation center-middle" data-overlay="6">
    <div className="application">
      <Helmet defaultTitle="Selbim" titleTemplate="Selbim - %s">
        <meta charSet="utf-8" />
        <title>{getRoutes(children.props.location.pathname, intl)}</title>
      </Helmet>
    </div>
    <div className="z-index-1">{children}</div>
  </div>
);

BasePage.propTypes = {
  children: PropTypes.element.isRequired,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(BasePage);
