import React from 'react';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

const Footer = () => (
  <footer className="app-footer">
    <Row>
      <Col lg={5} md={5} sm={5} xs={12}>
        <div className="footer-custom-center">
        Selbim
          &nbsp; v{process.env.VERSION}
          &nbsp; © 2018 &nbsp;
        </div>
      </Col>
      <Col lg={7} md={7} sm={7} xs={12}>
        <div className="footer-custom-center">
          <Link to="/privacy">
            <FormattedMessage id="footer.items.privacy" />
          </Link>
          <span>&nbsp; | &nbsp;</span>
          <Link to="/terms">
            <FormattedMessage id="footer.items.terms" />
          </Link>
        </div>
      </Col>
    </Row>
  </footer>
);

export default Footer;
