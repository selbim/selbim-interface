import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import Grid from 'react-bootstrap/lib/Grid';
import Col from 'react-bootstrap/lib/Col';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';
import DoughnutChartWidget from '../Widgets/DoughnutChartWidget';
import PieChartWidget from '../Widgets/PieChartWidget';
import TopTrendingAppsWidget from '../Widgets/TopTrendingAppsWidget';

const heavyTraffic = {
  labels: ['X', 'Y', 'Z', 'N'],
  datasets: [
    {
      data: [11500, 3300, 1600, 485],
      backgroundColor: ['#92d8ff', '#577993', '#d5f1ff', '#4b96d1'],
      hoverBackgroundColor: ['#0068f4', '#0068f4', '#0068f4', '#577993'],
    },
  ],
};

const visitedBranches = {
  labels: ['İstanbul', 'Ankara', 'İzmir', 'Antalya'],
  datasets: [
    {
      data: [299400, 195500, 130300, 102500],
      backgroundColor: ['#008476', '#bae3df', '#2bafa4', '#77fff4'],
      hoverBackgroundColor: ['#0068f4', '#0068f4', '#0068f4', '#577993'],
    },
  ],
};

const usingPhysical = {
  labels: ['İstanbul', 'Ankara', 'İzmir', 'Antalya'],
  datasets: [
    {
      data: [9, 5, 47, 12],
      backgroundColor: ['#69DDFF', '#96CDFF', '#D8E1FF', '#DBBADD'],
      hoverBackgroundColor: ['#0068f4', '#0068f4', '#0068f4', '#0068f4'],
    },
  ],
};

const heavyTrafficTable = {
  headers: ['Branch', 'Count'],
  data: [{ data: ['Z', '11.5k'] }, { data: ['Y', '3.3k'] }, { data: ['X', '1.6k'] }, { data: ['N', '485'] }],
};

const visitedTable = {
  headers: ['Branch', 'Count'],
  data: [{ data: ['Z', '11.5k'] }, { data: ['Y', '3.3k'] }, { data: ['X', '1.6k'] }, { data: ['N', '485'] }],
};

const BranchActivity = ({ intl }) =>
  (<ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'insights.branchactivity' })} />
    <Grid fluid>
      <Col lg={6}>
        <DoughnutChartWidget
          title={intl.formatMessage({ id: 'insights.branchactivity.heavytraffic' })}
          data={heavyTraffic}
          height={250}
          options={{
            maintainAspectRatio: false,
          }} />
      </Col>
      <Col lg={6}>
        <TopTrendingAppsWidget
          title={intl.formatMessage({ id: 'insights.branchactivity.heavytraffic' })}
          data={heavyTrafficTable} />
      </Col>
      <Col lg={6}>
        <DoughnutChartWidget
          title={intl.formatMessage({ id: 'insights.branchactivity.mostvisited' })}
          data={visitedBranches}
          height={250}
          options={{
            maintainAspectRatio: false,
          }} />
      </Col>
      <Col lg={6}>
        <TopTrendingAppsWidget
          title={intl.formatMessage({ id: 'insights.branchactivity.mostvisited' })}
          data={visitedTable} />
      </Col>
      <Col lg={6}>
        <PieChartWidget
          title={intl.formatMessage({ id: 'insights.branchactivity.physicalbranch' })}
          data={usingPhysical}
          height={263}
          options={{
            maintainAspectRatio: false,
          }} />
      </Col>
      <Col lg={6} />
    </Grid>
  </ContentWrapper>);

BranchActivity.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(BranchActivity);
