import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';
import VerticalBarChart from '../Widgets/VerticalBarChart';
import HorizontalStackBarChart from '../Widgets/HorizontalStackBarChart';
import MultipleLineChart from '../Widgets/MultipleLineChart';
import TopTrendingAppsWidget from '../Widgets/TopTrendingAppsWidget';

const hourlyData = {
  labels: [
    '2017/07/20 19',
    '2017/07/20 20',
    '2017/07/20 21',
    '2017/07/20 22',
    '2017/07/20 23',
    '2017/07/21 01',
    '2017/07/21 02',
    '2017/07/21 03',
    '2017/07/21 04',
    '2017/07/21 05',
    '2017/07/21 06',
    '2017/07/21 07',
    '2017/07/21 08',
    '2017/07/21 09',
    '2017/07/21 10',
    '2017/07/21 11',
    '2017/07/21 12',
    '2017/07/21 13',
    '2017/07/21 14',
    '2017/07/21 15',
    '2017/07/21 16',
    '2017/07/21 17',
  ],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: '#7266ba',
      borderColor: '#7266ba',
      borderWidth: 1,
      hoverBackgroundColor: '#545454',
      hoverBorderColor: '#545454',
      data: [
        10000,
        9000,
        9000,
        12000,
        14000,
        13000,
        9000,
        5000,
        3000,
        2200,
        1500,
        1000,
        300,
        500,
        750,
        1400,
        2600,
        6300,
        8000,
        12300,
        14500,
        15000,
        15956,
        14700,
        13800,
        5000,
      ],
    },
  ],
};

const VerticalBarChartData = {
  labels: ['Finance', 'Game', 'Social', 'Dating', 'Sports', 'Shopping', 'Travel', 'Music', 'Health & Fitness'],
  datasets: [
    {
      label: 'Other Competitor',
      backgroundColor: '#9dc3e6',
      data: [15, 7, 9, 8, 7, 12, 15, 14, 11],
    },
    {
      label: 'Blinnk',
      backgroundColor: '#be38c9',
      data: [7, 11, 9, 15, 10, 13, 9, 17, 7],
    },
  ],
};

const appInstallTrendsData = {
  labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week6', 'Week 7', 'Week 8'],
  datasets: [
    {
      label: 'Blinnk Users',
      fill: false,
      lineTension: 0.1,
      backgroundColor: '#be38c9',
      borderColor: '#be38c9',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: '#be38c9',
      pointBackgroundColor: '#be38c9',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: '#be38c9',
      pointHoverBorderColor: '#be38c9',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [12, 11, 10, 12, 9, 15, 16, 15],
    },
    {
      label: "Competitor's Users",
      fill: false,
      lineTension: 0.1,
      backgroundColor: '#9dc3e6',
      borderColor: '#9dc3e6',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: '#9dc3e6',
      pointBackgroundColor: '#9dc3e6',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: '#9dc3e6',
      pointHoverBorderColor: '#9dc3e6',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [23, 20, 16, 21, 23, 27, 19, 28],
    },
  ],
};

const locationDistData = {
  labels: ["Competitor's Users", 'Blinnk Users'],
  datasets: [
    {
      label: 'Home',
      data: [45, 35],
      backgroundColor: '#4472c5',
      hoverBackgroundColor: '#4472c5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Work',
      data: [5, 10],
      backgroundColor: '#ed7e30',
      hoverBackgroundColor: '#ed7e30',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Lunch',
      data: [10, 15],
      backgroundColor: '#a5a5a5',
      hoverBackgroundColor: '#a5a5a5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Shopping Mall',
      data: [5, 8],
      backgroundColor: '#ffc100',
      hoverBackgroundColor: '#ffc100',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Leisure',
      data: [35, 32],
      backgroundColor: '#5996cf',
      hoverBackgroundColor: '#5996cf',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
  ],
};
const networkDistData = {
  labels: ["Competitor's Users", 'Blinnk Users'],
  datasets: [
    {
      label: 'Wi-Fi',
      data: [55, 60],
      backgroundColor: '#4472c5',
      hoverBackgroundColor: '#4472c5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: '3G',
      data: [15, 20],
      backgroundColor: '#a5a5a5',
      hoverBackgroundColor: '#a5a5a5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'LTE',
      data: [30, 20],
      backgroundColor: '#5c9bd5',
      hoverBackgroundColor: '#5c9bd5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
  ],
};

const topTrendsData = {
  headers: ['Categories', '#1', '#2'],
  data: [{ data: ['Finance', 'x', 'y'] }, { data: ['Finance', 'a', 'b'] }, { data: ['Finance', 'k', 'l'] }],
};

const AppActivity = ({ intl }) => (
  <ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'insights.appactivity' })} />
    <Grid fluid>
      <Col lg={4} className="pl0">
        <div className="panel widget">
          <div className="panel-body bg-info text-center">
            <div className="text-lg m0">205,449</div>
            <p>
              <FormattedMessage id="insights.totalusagecount" />
            </p>
            <p>
              <FormattedMessage id="insights.hourly" />
            </p>
          </div>
        </div>
        <div className="panel widget">
          <div className="panel-body bg-primary text-center">
            <div className="text-lg m0">8,217</div>
            <p>
              <FormattedMessage id="insights.averageusagecount" />
            </p>
            <p>
              <FormattedMessage id="insights.hourly" />
            </p>
          </div>
        </div>
      </Col>
      <Col lg={8} className="pr0">
        <VerticalBarChart
          title={intl.formatMessage({ id: 'insights.appactivity.hourlydistribution' })}
          data={hourlyData}
          height={263}
          options={{
            maintainAspectRatio: false,
            legend: {
              display: false,
            },
          }} />
      </Col>
      <VerticalBarChart
        title={intl.formatMessage({ id: 'insights.appactivity.appinstalldistribution' })}
        data={VerticalBarChartData}
        options={{
          maintainAspectRatio: false,
          responsive: true,
          legend: {
            position: 'bottom',
          },
        }} />
      <Row>
        <Col lg={6}>
          <MultipleLineChart
            title={intl.formatMessage({ id: 'insights.appactivity.appinstalltrends' })}
            data={appInstallTrendsData}
            height={200}
            options={{
              maintainAspectRatio: false,
              responsive: true,
              legend: {
                position: 'bottom',
              },
            }} />
        </Col>
        <Col lg={6}>
          <TopTrendingAppsWidget
            title={intl.formatMessage({ id: 'insights.appactivity.topinstallapps' })}
            data={topTrendsData} />
        </Col>
      </Row>
      <Row>
        <Col lg={6} className="pl0">
          <HorizontalStackBarChart
            title={intl.formatMessage({ id: 'insights.appactivity.segmentlocationdistribution' })}
            data={locationDistData}
            options={{
              maintainAspectRatio: false,
              responsive: true,
              legend: {
                position: 'bottom',
              },
              scales: {
                xAxes: [
                  {
                    stacked: true,
                  },
                ],
                yAxes: [
                  {
                    stacked: true,
                  },
                ],
              },
            }} />
        </Col>
        <Col lg={6} className="pr0">
          <HorizontalStackBarChart
            title={intl.formatMessage({ id: 'insights.appactivity.segmentnetworkdistribution' })}
            data={networkDistData}
            options={{
              maintainAspectRatio: false,
              responsive: true,
              legend: {
                position: 'bottom',
              },
              scales: {
                xAxes: [
                  {
                    stacked: true,
                  },
                ],
                yAxes: [
                  {
                    stacked: true,
                  },
                ],
              },
            }} />
        </Col>
      </Row>
    </Grid>
  </ContentWrapper>
);

AppActivity.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(AppActivity);
