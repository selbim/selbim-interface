import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';
import TrendingAppsWidget from '../Widgets/TrendingAppsWidget';
import TopTrendingAppsWidget from '../Widgets/TopTrendingAppsWidget';
import HorizontalStackBarChart from '../Widgets/HorizontalStackBarChart';

const appCategoryData = {
  labels: ['Finance', 'Game', 'Social', 'Dating', 'Sports', 'Shopping', 'Travel', 'Music', 'Health & Fitness'],
  datasets: [
    {
      pointRadius: [7, 13, 9, 10, 14, 8, 10, 14, 5],
      pointHoverRadius: 15,
      backgroundColor: '#8884d8',
      strokeColor: 'rgba(220,220,220,1)',
      fill: false,
      data: [200, 100, 600, 200, 400, 800, 1000, 400, 400],
      showLine: false,
    },
  ],
};

const appCategoryDataOtherCompany = {
  labels: ['Finance', 'Game', 'Social', 'Dating', 'Sports', 'Shopping', 'Travel', 'Music', 'Health & Fitness'],
  datasets: [
    {
      pointRadius: [7, 13, 9, 5, 14, 13, 10, 14, 5],
      pointHoverRadius: 15,
      backgroundColor: '#8884d8',
      strokeColor: 'rgba(220,220,220,1)',
      fill: false,
      data: [100, 200, 450, 1000, 400, 800, 200, 50, 400],
      showLine: false,
    },
  ],
};

const topTrendsData = {
  headers: ['Categories', '#1', '#2', '#3'],
  data: [{ data: ['Finance', 'x', 'y', 'z'] }, { data: ['Game', 'a', 'b', 'c'] }, { data: ['Social', 'k', 'l', 'm'] }],
};

const topTrendsDataOtherCompany = {
  headers: ['Categories', '#1', '#2', '#3'],
  data: [
    { data: ['Health & Fitness', 'd', 'e', 'f'] },
    { data: ['Sports', 'h', 'i', 'j'] },
    { data: ['Social', 'k', 'l', 'm'] },
  ],
};

const locationDistData = {
  labels: ["Competitor's Users", 'Blinnk Users'],
  datasets: [
    {
      label: 'Home',
      data: [45, 35],
      backgroundColor: '#4472c5',
      hoverBackgroundColor: '#4472c5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Work',
      data: [5, 10],
      backgroundColor: '#ed7e30',
      hoverBackgroundColor: '#ed7e30',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Lunch',
      data: [10, 15],
      backgroundColor: '#a5a5a5',
      hoverBackgroundColor: '#a5a5a5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Shopping Mall',
      data: [5, 8],
      backgroundColor: '#ffc100',
      hoverBackgroundColor: '#ffc100',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'Leisure',
      data: [35, 32],
      backgroundColor: '#5996cf',
      hoverBackgroundColor: '#5996cf',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
  ],
};

const networkDistData = {
  labels: ["Competitor's Users", 'Blinnk Users'],
  datasets: [
    {
      label: 'Wi-Fi',
      data: [55, 60],
      backgroundColor: '#4472c5',
      hoverBackgroundColor: '#4472c5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: '3G',
      data: [15, 20],
      backgroundColor: '#a5a5a5',
      hoverBackgroundColor: '#a5a5a5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
    {
      label: 'LTE',
      data: [30, 20],
      backgroundColor: '#5c9bd5',
      hoverBackgroundColor: '#5c9bd5',
      hoverBorderWidth: 2,
      hoverBorderColor: 'lightgrey',
    },
  ],
};

const Trends = ({ intl }) => (
  <ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'insights.trends' })} />
    <Row>
      <Col lg={6} sm={6} xs={12}>
        <TrendingAppsWidget
          title={intl.formatMessage({ id: 'insights.trends.title.trendingappcategory.blinnk' })}
          data={appCategoryData}
          options={{
            legend: {
              display: false,
            },
            scales: {
              yAxes: [
                {
                  ticks: {
                    max: 1600,
                  },
                },
              ],
            },
          }} />
      </Col>
      <Col lg={6} sm={6} xs={12}>
        <TrendingAppsWidget
          title={intl.formatMessage({ id: 'insights.trends.title.trendingappcategory.othercompany' })}
          data={appCategoryDataOtherCompany}
          options={{
            legend: {
              display: false,
            },
            scales: {
              yAxes: [
                {
                  ticks: {
                    max: 1600,
                  },
                },
              ],
            },
          }} />
      </Col>
      <Col lg={6} sm={6} xs={12}>
        <TopTrendingAppsWidget
          title={intl.formatMessage({ id: 'insights.trends.title.toptrendingapps' })}
          data={topTrendsData} />
      </Col>
      <Col lg={6} sm={6} xs={12}>
        <TopTrendingAppsWidget
          title={intl.formatMessage({ id: 'insights.trends.title.toptrendingapps' })}
          data={topTrendsDataOtherCompany} />
      </Col>
      <Col lg={6}>
        <HorizontalStackBarChart
          title={intl.formatMessage({ id: 'insights.trends.segmentlocationdistribution' })}
          data={locationDistData}
          options={{
            maintainAspectRatio: false,
            responsive: true,
            legend: {
              position: 'bottom',
            },
            scales: {
              xAxes: [
                {
                  stacked: true,
                },
              ],
              yAxes: [
                {
                  stacked: true,
                },
              ],
            },
          }} />
      </Col>
      <Col lg={6} className="pr0">
        <HorizontalStackBarChart
          title={intl.formatMessage({ id: 'insights.trends.segmentnetworkdistribution' })}
          data={networkDistData}
          options={{
            maintainAspectRatio: false,
            responsive: true,
            legend: {
              position: 'bottom',
            },
            scales: {
              xAxes: [
                {
                  stacked: true,
                },
              ],
              yAxes: [
                {
                  stacked: true,
                },
              ],
            },
          }} />
      </Col>
    </Row>
  </ContentWrapper>
);

Trends.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Trends);
