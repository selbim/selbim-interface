import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';
import PieChartWidget from '../Widgets/PieChartWidget';

const usingPhysical = {
  labels: ['Backpacker', 'Car Owner', 'Music Lover', 'Social Media Addict'],
  datasets: [
    {
      data: [9, 5, 47, 12],
      backgroundColor: ['#69DDFF', '#96CDFF', '#D8E1FF', '#DBBADD'],
      hoverBackgroundColor: ['#0068f4', '#0068f4', '#0068f4', '#0068f4'],
    },
  ],
};

const Competitors = ({ intl }) =>
  (<ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'insights.competitors' })} />
    <Row>
      <Col lg={6}>
        <PieChartWidget
          title={intl.formatMessage({ id: 'insights.competitors.branchactivity' })}
          data={usingPhysical}
          height={263}
          options={{
            maintainAspectRatio: false,
            legend: {
              position: 'right',
            },
          }} />
      </Col>
    </Row>
  </ContentWrapper>);

Competitors.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Competitors);
