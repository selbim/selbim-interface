import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Map from 'react-leaflet/lib/Map';
import TileLayer from 'react-leaflet/lib/TileLayer';
import FeatureGroup from 'react-leaflet/lib/FeatureGroup';
import { EditControl } from 'react-leaflet-draw';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';

const position = [41.0365536, 29.0971969];
const Maps = ({ intl }) =>
  (<ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'insights.maps' })} />
    <Row>
      <Col lg={12} md={12} sm={12} xs={12} className="panel b">
        <div className="panel-heading">
          <div className="panel-title text-center">
            <FormattedMessage id="insights.map.title" />
          </div>
        </div>
        <div className="panel-body">
          <Map center={position} zoom={10}>
            <TileLayer
              attribution="<a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a>"
              url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
            <FeatureGroup>
              <EditControl
                position="topleft"
                draw={{
                  polyline: false,
                  polygon: false,
                  rectangle: false,
                  circle: true,
                  marker: false,
                }} />
            </FeatureGroup>
          </Map>
        </div>
      </Col>
    </Row>
  </ContentWrapper>);

Maps.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Maps);
