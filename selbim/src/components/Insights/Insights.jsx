import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';

const Insights = ({ intl }) =>
  (<ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'insights.title' })} />
    <Grid fluid>
      <Row>
        <Col lg={2} md={6} sm={12} xs={12}>
          <div className="panel b full-width" style={{ height: '160px' }}>
            <div className="panel-body text-center">
              <Link to="/insights/maps" className="link-unstyled text-primary">
                <em className="fa fa-5x fa-map-o mb-lg mt" />
                <br />
                <span className="h4"><FormattedMessage id="insights.maps" /></span>
              </Link>
            </div>
          </div>
        </Col>
        <Col lg={2} md={6} sm={12} xs={12}>
          <div className="panel b full-width" style={{ height: '160px' }}>
            <div className="panel-body text-center">
              <Link to="/insights/competitors" className="link-unstyled text-info">
                <em className="fa fa-5x fa-bullseye mb-lg mt" />
                <br />
                <span className="h4"><FormattedMessage id="insights.competitors" /></span>
              </Link>
            </div>
          </div>
        </Col>
        <Col lg={2} md={6} sm={12} xs={12}>
          <div className="panel b full-width" style={{ height: '160px' }}>
            <div className="panel-body text-center">
              <Link to="/insights/trends" className="link-unstyled text-purple">
                <em className="fa fa-5x fa-line-chart mb-lg mt" />
                <br />
                <span className="h4"><FormattedMessage id="insights.trends" /></span>
              </Link>
            </div>
          </div>
        </Col>
        <Col lg={2} md={6} sm={12} xs={12}>
          <div className="panel b full-width" style={{ height: '160px' }}>
            <div className="panel-body text-center">
              <Link to={'/insights/branch-activity'} className="link-unstyled text-warning">
                <em className="fa fa-5x fa-building-o mb-lg mt" />
                <br />
                <span className="h4"><FormattedMessage id="insights.branchactivity" /></span>
              </Link>
            </div>
          </div>
        </Col>
        <Col lg={2} md={6} sm={12} xs={12}>
          <div className="panel b full-width" style={{ height: '160px' }}>
            <div className="panel-body text-center">
              <Link to="/insights/app-activity" className="link-unstyled text-danger">
                <em className="fa fa-5x fa-mobile mb-lg mt" />
                <br />
                <span className="h4"><FormattedMessage id="insights.appactivity" /></span>
              </Link>
            </div>
          </div>
        </Col>
      </Row>
    </Grid>
  </ContentWrapper>);

Insights.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Insights);
