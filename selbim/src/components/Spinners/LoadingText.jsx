import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

const LoadingText = ({ loading, error, data, button }) => {
  let text;
  if (loading) {
    text = <div>{button.sending} <em className="fa fa-fw fa-spinner fa-spin" /></div>;
  } else if (error) {
    text = <div>{button.error} <em className="fa fa-fw fa-exclamation-circle" /></div>;
  } else if (!isEmpty(data)) {
    text = <div>{button.sent} <em className={button.sentIcon} /></div>;
  } else {
    text = <div>{button.default} <em className={button.defaultIcon} /></div>;
  }
  return <div>{text}</div>;
};

LoadingText.defaultProps = {
  data: {},
  button: {
    sending: 'Sending',
    error: 'Error Occured',
    sent: 'Succesfully Sent!',
    default: 'Send',
    defaultIcon: 'fa fa-fw fa-envelope',
    sentIcon: 'fa fa-fw fa-check',
  },
};
LoadingText.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  data: PropTypes.object,
  button: PropTypes.object,
};

export default LoadingText;
