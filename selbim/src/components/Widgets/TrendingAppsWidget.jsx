import React from 'react';
import Col from 'react-bootstrap/lib/Col';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';

const TrendingAppsWidget = ({ title, data, options }) =>
  (<Col lg={12} md={12} sm={12} xs={12} className="panel b">
    <div className="panel-heading">
      <div className="panel-title text-center">
        {title}
      </div>
    </div>
    <div className="panel-body">
      <Line data={data} options={options} height={200} />
    </div>
  </Col>);

TrendingAppsWidget.defaultProps = {
  options: {
    maintainAspectRatio: true,
    responsive: true,
    legend: {
      display: false,
    },
    tooltips: {
      callbacks: {
        label(tooltipItem) {
          return tooltipItem.yLabel;
        },
      },
    },
  },
};

TrendingAppsWidget.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  options: PropTypes.object,
};

export default TrendingAppsWidget;
