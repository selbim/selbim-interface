import React from 'react';
import PropTypes from 'prop-types';
import Col from 'react-bootstrap/lib/Col';
import { Line } from 'react-chartjs-2';

const MultipleLineChart = ({ title, data, options, width, height }) => (
  <Col lg={12} md={12} sm={12} xs={12} className="panel b">
    <div className="panel-heading">{title ? <div className="panel-title text-center">{title}</div> : null}</div>
    <div className="panel-body">
      <Line data={data} options={options} width={width} height={height} />
    </div>
  </Col>
);

MultipleLineChart.defaultProps = {
  options: {
    maintainAspectRatio: false,
    responsive: true,
    legend: {
      display: false,
    },
    tooltips: {
      callbacks: {
        label(tooltipItem) {
          return tooltipItem.yLabel;
        },
      },
    },
  },
};

MultipleLineChart.defaultProps = {
  width: 100,
  height: 225,
};
MultipleLineChart.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  options: PropTypes.object,
  width: PropTypes.number,
  height: PropTypes.number,
};

export default MultipleLineChart;
