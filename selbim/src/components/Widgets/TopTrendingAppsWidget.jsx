import React from 'react';
import Col from 'react-bootstrap/lib/Col';
import PropTypes from 'prop-types';
import map from 'lodash/map';

const TopTrendingAppsWidget = ({ title, data }) =>
  (<Col lg={12} md={12} sm={12} xs={12} className="panel b">
    <div className="panel-heading">
      <div className="panel-title text-center">
        <div style={{ color: 'gray' }}>
          {title}
        </div>
      </div>
    </div>
    <div className="panel-body">
      <table className="full-width">
        <tbody>
          <tr style={{ height: '50px' }}>
            {map(data.headers, value =>
              (<th key={Math.random()} className="top-trending-table-th">
                {value}
              </th>),
            )}
          </tr>
          {map(data.data, value =>
            (<tr key={Math.random()} className="mt-lg" style={{ height: '50px' }}>
              {map(value.data, row =>
                (<td key={Math.random()} className="text-center">
                  {row}
                </td>),
              )}
            </tr>),
          )}
        </tbody>
      </table>
    </div>
  </Col>);

TopTrendingAppsWidget.defaultProps = {
  title: '',
};

TopTrendingAppsWidget.propTypes = {
  title: PropTypes.string,
  data: PropTypes.object.isRequired,
};

export default TopTrendingAppsWidget;
