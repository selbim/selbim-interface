import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import map from 'lodash/map';
import groupBy from 'lodash/groupBy';
import Table from 'react-bootstrap/lib/Table';
import Col from 'react-bootstrap/lib/Col';
import reduce from 'lodash/reduce';
import { injectIntl, FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { Doughnut } from 'react-chartjs-2';
import NumberFormatter from '../Formatters/NumberFormatter';
import LoadingSpinner from '../Spinners/LoadingSpinner';

const flipDoughnat = function (e, uniqueKey) {
  e.preventDefault();
  const card = document.getElementsByClassName(uniqueKey);
  card[0].classList.toggle('flipped');
};

function groupByVersion(array) {
  return groupBy(
    map(map(array, value => ({ key: value.key.split('-')[1], value: value.value })), value => ({
      key: value.key.split('.')[0],
      value: value.value,
    })),
    value => Math.floor(value.key),
  );
}

function getTotal(array) {
  return reduce(array, (acc, val) => acc + val.value, 0);
}

function getFilteredData(array) {
  return map(array, value => ({
    label: value[0].key,
    value: reduce(value, (acc, val) => acc + val.value, 0),
  })).filter((value) => {
    if (getTotal(array) > 5000) {
      if (value.value > 500) {
        return value;
      }
      return null;
    }
    return value;
  });
}

function getValues(array) {
  return map(array, value => value.value);
}

function getVersions(array) {
  return map(array, value => value.label);
}

const FlipChartWidgetInner = ({ pieData, tableData, isLoading, type, intl }) => {
  const cardFlip = Math.random();
  return (
    <div className="panel widget">
      {isLoading ? <LoadingSpinner /> : <div />}
      <div className={`${isLoading ? 'blur' : ''}`}>
        <div className="panel-heading">
          <a
            onClick={e => flipDoughnat(e, `${cardFlip}`)}
            data-toggle="tooltip"
            title={intl.formatMessage({ id: 'myapps.flipchartwidget.clicktoseedetails' })}
            data-spinner="standard"
            className="pull-right">
            <em className="fa fa-exchange" />
          </a>
          <div className="text-center">
            <h3>
              <NumberFormatter number={getTotal(tableData)} /> <FormattedMessage id="myapps.flipchartwidget.devices" />
            </h3>
            <p>
              <strong>
                {type} <FormattedMessage id="myapps.flipchartwidget.osdistribution" />
              </strong>
            </p>
          </div>
        </div>
        <div className="panel-body overflow-hidden">
          <div>
            <div className="flip-container svg-center">
              {isEmpty(tableData)
                ? <div id="card" className={`${cardFlip}  text-center no-available-data`}>
                  <figure className="front">
                    <div>
                      <FormattedMessage id="myapps.flipchartwidget.nodata" />
                    </div>{' '}
                  </figure>
                  <figure className="back">
                    <FormattedMessage id="myapps.flipchartwidget.nodata" />
                  </figure>
                </div>
                : <div id="card" className={`${cardFlip}`}>
                  <figure className="front">
                    <Doughnut graphType="Doughnut" text="Android" data={pieData} width={50} height={50} />
                  </figure>
                  <figure className="back">
                    {
                      <Table responsive className="table-striped table-responsive flip-table">
                        <thead>
                          <tr>
                            <th>
                              <FormattedMessage id="myapps.flipchartwidget.table.versions" />
                            </th>
                            <th>
                              <FormattedMessage id="myapps.flipchartwidget.table.count" />
                            </th>
                          </tr>
                        </thead>
                        <tbody style={{ width: '100%' }}>
                          {map(tableData.sort((a, b) => b.value - a.value), value =>
                              (<tr key={Math.random()}>
                                <td>
                                  {value.key}
                                </td>
                                <td>
                                  <NumberFormatter number={value.value} />
                                </td>
                              </tr>),
                            )}
                        </tbody>
                      </Table>
                      }
                  </figure>
                </div>}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
FlipChartWidgetInner.defaultProps = {
  isLoading: false,
  type: '',
};

FlipChartWidgetInner.propTypes = {
  type: PropTypes.string,
  tableData: PropTypes.array.isRequired,
  pieData: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  intl: PropTypes.object.isRequired,
};

class FlipChartWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = { alert: null };
  }

  render() {
    const { deviceStat, deviceStatLoading, deviceStatError, hidePlatform, intl } = this.props;

    const errorWidget = (
      <Col lg={6} md={6} xs={12}>
        <div className="panel widget">
          <div className="panel-heading">
            <div className="pull-right">
              <a>
                <em className="fa fa-exchange" />
              </a>
            </div>
          </div>
          <div className="panel-body">
            <div className="flip-container svg-center text-center">
              <p className="text-md mb-lg mt-lg">
                <FormattedMessage id="myapps.segmentationwidget.error" />
              </p>
              <p>
                <FormattedMessage id="myapps.segmentationwidget.refreshcomponent" />
              </p>
              <div className="mt-lg mb-lg">
                <a onClick={this.onRefreshClicked}>
                  <em className="fa fa-refresh fa-3x icon-blue" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </Col>
    );

    if (deviceStatError) {
      return (
        <div>
          {errorWidget}
          {errorWidget}
        </div>
      );
    }

    if (isEmpty(deviceStat) || deviceStatLoading) {
      const androidData = {
        labels: ['Red', 'Blue', 'Yellow'],
        datasets: [
          {
            data: [300, 50, 100],
            backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
            hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
          },
        ],
      };

      const iosData = {
        labels: ['Red', 'Blue', 'Yellow'],
        datasets: [
          {
            data: [300, 50, 100],
            backgroundColor: ['#9fe3f0', '#428bca', '#d5f1ff'],
            hoverBackgroundColor: ['#0068f4', '#0068f4', '#0068f4'],
          },
        ],
      };

      const tableData = [{ key: '5.0', value: 5 }];

      return (
        <div>
          <Col lg={6} md={6} xs={12}>
            <FlipChartWidgetInner pieData={androidData} tableData={tableData} isLoading intl={intl} />
          </Col>
          <Col lg={6} md={6} xs={12}>
            <FlipChartWidgetInner pieData={iosData} tableData={tableData} isLoading intl={intl} />
          </Col>
        </div>
      );
    }
    const androidData = getFilteredData(groupByVersion(deviceStat.statistics.android));
    const iosData = getFilteredData(groupByVersion(deviceStat.statistics.ios));
    const androidVersionsKeys = getVersions(androidData);
    const androidVersionsValues = getValues(androidData);
    const iosVersionsKeys = getVersions(iosData);
    const iosVersionsValues = getValues(iosData);

    const pieDataAndroid = {
      labels: androidVersionsKeys,
      datasets: [
        {
          data: androidVersionsValues,
          backgroundColor: ['#B2DFDB', '#26A69A', '#00796B', '#81C784', '#43A047', '#1B5E20', '#1DE9B6', '#64DD17'],
          hoverBackgroundColor: [
            '#469A10',
            '#469A10',
            '#469A10',
            '#469A10',
            '#469A10',
            '#469A10',
            '#469A10',
            '#469A10',
          ],
        },
      ],
    };

    const pieDataIOS = {
      labels: iosVersionsKeys,
      datasets: [
        {
          data: iosVersionsValues,
          backgroundColor: ['#87d2ff', '#d5f1ff', '#428bca', '#9fe3f0', '#e3f7ff', '#245292'],
          hoverBackgroundColor: ['#0068f4', '#0068f4', '#0068f4', '#0068f4', '#0068f4', '#0068f4'],
        },
      ],
    };
    return (
      <div>
        {(hidePlatform && this.props.properties.platform === 'ANDROID') || !hidePlatform
          ? <Col lg={6} md={6} xs={12}>
            <FlipChartWidgetInner
              pieData={pieDataAndroid}
              tableData={deviceStat.statistics.android}
              type="Android"
              intl={intl} />
          </Col>
          : ''}

        {(hidePlatform && this.props.properties.platform === 'IOS') || !hidePlatform
          ? <Col lg={6} md={6} xs={12}>
            <FlipChartWidgetInner pieData={pieDataIOS} tableData={deviceStat.statistics.ios} type="IOS" intl={intl} />
          </Col>
          : ''}
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {
  };
}

FlipChartWidget.defaultProps = {
  deviceStat: {},
  properties: {},
  hidePlatform: false,
};

FlipChartWidget.propTypes = {
  properties: PropTypes.object,
  deviceStat: PropTypes.object,
  deviceStatLoading: PropTypes.bool.isRequired,
  deviceStatError: PropTypes.bool.isRequired,
  hidePlatform: PropTypes.bool,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(connect(mapStateToProps)(FlipChartWidget));
