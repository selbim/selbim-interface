import React from 'react';
import PropTypes from 'prop-types';
import Col from 'react-bootstrap/lib/Col';
import { HorizontalBar } from 'react-chartjs-2';

const HorizontalStackBarChart = ({ title, data, options }) =>
  (<Col lg={12} md={12} sm={12} xs={12} className="panel b">
    <div className="panel-heading">
      <div className="panel-title text-center">
        {title}
      </div>
    </div>
    <div className="panel-body">
      <HorizontalBar data={data} options={options} height={265} />
    </div>
  </Col>);

HorizontalStackBarChart.defaultProps = {
  options: {
    maintainAspectRatio: false,
    responsive: true,
    legend: {
      display: false,
    },
    tooltips: {
      callbacks: {
        label(tooltipItem) {
          return tooltipItem.yLabel;
        },
      },
    },
  },
};

HorizontalStackBarChart.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  options: PropTypes.object,
};

export default HorizontalStackBarChart;
