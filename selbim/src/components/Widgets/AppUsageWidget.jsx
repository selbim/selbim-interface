import React from 'react';
import PropTypes from 'prop-types';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import isEmpty from 'lodash/isEmpty';
import NumberFormatter from '../Formatters/NumberFormatter';
import LoadingSpinner from '../Spinners/LoadingSpinner';

const InnerAppUsageWidget = ({ params, isLoading }) =>
  (<div className="panel widget">
    {isLoading ? <LoadingSpinner /> : <div />}
    <div className={`panel-body ${isLoading ? 'blur' : ''}`}>
      <Row className="row-table">
        <Col md={6} className="text-center">
          <img src={params.iconUrl} alt="icon url" className="thumb96" />
        </Col>
        <Col md={6}>
          <h3 className="mt0"><a href={params.url}>{params.name}</a></h3>
          <ul className="list-unstyled">
            <li className="mb-sm text-sm text-break">
              <em className="fa fa-archive fa-fw" />{params.uid}
            </li>
          </ul>
        </Col>
      </Row>
    </div>
    <div className="panel-body bg-inverse">
      <Row className="row-table text-center">
        <Col xs={3} md={3} lg={3}>
          <p className="m0 h3">{Math.round(params.averageDuration)}</p>
          <p className="m0 text-muted"><FormattedMessage id="myapps.appusagewidget.averageduration" /></p>
        </Col>
        <Col xs={3} md={3} lg={3}>
          <p className="m0 h3">
            <NumberFormatter number={params.appUsingDeviceCount} />
          </p>
          <p className="m0 text-muted text-break"><FormattedMessage id="myapps.appusagewidget.appactivitycount" /></p>
        </Col>
        <Col xs={3} md={3} lg={3}>
          <p className="m0 h3">
            <NumberFormatter number={params.activeDeviceCount} />
          </p>
          <p className="m0 text-muted text-break"><FormattedMessage id="myapps.appusagewidget.appdevicecount" /></p>
        </Col>
        <Col xs={3} md={3} lg={3}>
          <p className="m0 h3">
            {Math.ceil(params.appUsingDeviceCount * 100 / params.activeDeviceCount)} %
          </p>
          <p className="m0 text-muted text-break">
            <FormattedMessage id="myapps.appusagewidget.dailyusagepercentage" />
          </p>
        </Col>
      </Row>
    </div>
  </div>);

InnerAppUsageWidget.defaultProps = {
  params: {},
  isLoading: false,
};

InnerAppUsageWidget.propTypes = {
  params: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
};

class AppUsageWidget extends React.Component {
  render() {
    const { properties, averageUsage } = this.props;
    if (!properties || isEmpty(properties)) {
      return <div />;
    }
    if (!averageUsage || isEmpty(averageUsage)) {
      return <div />;
    }
    return (
      <Col lg={6} md={6} xs={12}>
        <div className="panel b">
          <div className="panel-heading bg-gray-lighter text-bold">
            <FormattedMessage id="myapps.appusagewidget.applicationusage" />
          </div>
          <div className="panel-body">
            <InnerAppUsageWidget
              key={Math.random()}
              params={{
                iconUrl: averageUsage[0].application.iconUrl,
                url: averageUsage[0].application.url,
                name: averageUsage[0].application.label,
                uid: averageUsage[0].application.uid,
                averageDuration: averageUsage[0].averageDuration,
                appUsingDeviceCount: averageUsage[0].appUsingDeviceCount,
                activeDeviceCount: averageUsage[0].activeDeviceCount,
              }} />
          </div>
        </div>
      </Col>
    );
  }
}

AppUsageWidget.defaultProps = {
  averageUsage: [],
};

AppUsageWidget.propTypes = {
  averageUsage: PropTypes.array,
  properties: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    averageUsage: state.myapps.selectedApplication.averageUsage.statistics,
  };
}

export default injectIntl(connect(mapStateToProps)(AppUsageWidget));
