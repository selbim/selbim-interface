import React from 'react';
import PropTypes from 'prop-types';
import NumericLable from './NumericLable';

const numberFormat = {
  justification: 'L',
  currency: false,
  percentage: false,
  precision: 1,
  commafy: true,
  shortFormat: true,
  title: true,
};

const NumberFormatter = ({ number }) => (
  <NumericLable params={numberFormat}>
    {`${number}`}
  </NumericLable>
);

NumberFormatter.propTypes = {
  number: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default NumberFormatter;
