export function format(num) {
  const n = num.toString();
  const p = n.indexOf('.');
  return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, ($0, i) => (p < 0 || i < p ? `${$0},` : $0));
}
