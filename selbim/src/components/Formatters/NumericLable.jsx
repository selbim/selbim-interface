import React from 'react';
import PropTypes from 'prop-types';

const NumericLabel = (props) => {
  // inspired by http://stackoverflow.com/a/9462382
  function nFormatter(num, minValue) {
    if (!num || !+num || typeof +num !== 'number') {
      return {
        number: num,
      };
    }

    num = +num;

    minValue = minValue || 0;
    const si = [
      { value: 1e18, symbol: 'E' },
      { value: 1e15, symbol: 'P' },
      { value: 1e12, symbol: 'T' },
      { value: 1e9, symbol: 'G' },
      { value: 1e6, symbol: 'M' },
      { value: 1e3, symbol: 'k' },
    ];

    let i = 0;
    if (typeof num === 'number' && num >= minValue) {
      for (i = 0; i < si.length; i += 1) {
        if (num >= si[i].value) {
          return {
            number: num / si[i].value,
            letter: si[i].symbol,
          };
        }
      }
    }
    return {
      number: num,
    };
  }

  let locales;
  let number;
  let mystyle;
  let option;
  const css = '';
  if (props.params) {
    locales = props.params.locales;
    if (props.params.wholenumber === 'floor') {
      number = Math.floor(props.children);
    } else if (props.params.wholenumber === 'ceil') {
      number = Math.ceil(props.children);
    } else {
      number = props.children;
    }

    let styles = 'right';
    if (props.params.justification === 'L') {
      styles = 'left';
    } else if (props.params.justification === 'C') {
      styles = 'center';
    } else {
      styles = 'right';
    }
    mystyle = {
      textAlign: styles,
    };

    let currencyIndicator;
    if (props.params.currencyIndicator) {
      currencyIndicator = props.params.currencyIndicator;
    } else {
      currencyIndicator = 'USD';
    }

    if (props.params.percentage) {
      option = {
        style: 'percent',
        maximumFractionDigits: props.params.precision,
        useGrouping: props.params.commafy,
      };
    } else if (props.params.currency) {
      option = {
        style: 'currency',
        currency: currencyIndicator,
        maximumFractionDigits: props.params.precision,
        useGrouping: props.params.commafy,
      };
    } else {
      option = {
        style: 'decimal',
        maximumFractionDigits: props.params.precision,
        useGrouping: props.params.commafy,
      };
    }

    if (props.params.cssClass) {
      props.params.cssClass.map(clas => css === `${css}${clas} `);
    }
  } else {
    number = props.children;
    locales = 'en-US';
    mystyle = {
      textAlign: 'left',
    };
    option = {};
  }

  let shortenNumber = number;
  let numberLetter = '';

  if (props.params && props.params.shortFormat) {
    const sn = nFormatter(number, props.params.shortFormatMinValue || 0);
    shortenNumber = sn.number;
    numberLetter = sn.letter || '';

    if (props.params.shortFormatMinValue && +number >= props.params.shortFormatMinValue) {
      option.maximumFractionDigits = props.params.shortFormatPrecision || props.params.precision || 0;
    }
  }

  let theFormattedNumber = shortenNumber;

  if (typeof shortenNumber === 'number') {
    theFormattedNumber = Intl.NumberFormat(locales, option).format(+shortenNumber);
  }

  if (numberLetter) {
    if (props.params && props.params.percentage) {
      theFormattedNumber = theFormattedNumber.replace('%', `${numberLetter}%`);
    } else {
      theFormattedNumber += numberLetter;
    }
  }

  let title = false;
  if (props.params && props.params.title) {
    props.params.title === true ? (title = number) : (title = props.params.title); //eslint-disable-line
  }

  if (mystyle.textAlign && (mystyle.textAlign === 'right' || mystyle.textAlign === 'center')) {
    // div
    if (title) {
      // with title
      return (
        <div className={css} style={mystyle} title={title}>
          {theFormattedNumber}
        </div>
      );
    }
    // without title
    return (
      <div className={css} style={mystyle}>
        {theFormattedNumber}
      </div>
    );
  }
  // span
  if (title) {
    // with title
    return (
      <span className={css} style={mystyle} title={title}>
        {theFormattedNumber}
      </span>
    );
  }
  // without title
  return (
    <span className={css} style={mystyle}>
      {theFormattedNumber}
    </span>
  );
};

NumericLabel.propTypes = {
  params: PropTypes.object.isRequired,
  children: PropTypes.string.isRequired,
};

export default NumericLabel;
