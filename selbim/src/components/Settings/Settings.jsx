import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Tab from 'react-bootstrap/lib/Tab';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import map from 'lodash/map';
import SweetAlert from 'react-bootstrap-sweetalert';
import isSubmitting from 'redux-form/lib/isSubmitting';
import { change, untouch } from 'redux-form/lib/actions';
import { injectIntl, FormattedMessage } from 'react-intl';
import ContentHeader from '../Layout/ContentHeader';
import ContentWrapper from '../Layout/ContentWrapper';
import { changePassword, invalidateChangePassword } from '../../actions/authActions';
import ChangePasswordForm from '../Forms/ChangePasswordForm';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = { alert: null };
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  componentWillMount() {
    this.props.invalidateChangePassword();
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.submitting) {
      this.props.change('changePasswordForm', 'newPassword', '');
      this.props.change('changePasswordForm', 'newPasswordConfirm', '');
      this.props.untouch('changePasswordForm', 'newPassword', '');
    }
  }

  handleFormSubmit(value) {
    return this.props.changePassword(value);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
    return null;
  }

  render() {
    const { intl } = this.props;
    return (
      <ContentWrapper>
        <ContentHeader title="User Settings" />
        <Tab.Container className="container-md" id="settings-tab" defaultActiveKey="tabpanel1">
          <Row>
            <Col md={3}>
              <div className="panel b">
                <div className="panel-heading bg-gray-lighter text-bold">
                  <FormattedMessage id="settings.sidebar.title" />
                </div>
                <Nav>
                  <NavItem eventKey="tabpanel1"><FormattedMessage id="settings.sidebar.account" /></NavItem>
                  <NavItem eventKey="tabpanel2"><FormattedMessage id="settings.sidebar.applications" /></NavItem>
                </Nav>
              </div>
            </Col>
            <Col md={9}>
              <Tab.Content animation className="p0 b0">
                <Tab.Pane eventKey="tabpanel1">
                  <div>
                    <div className="panel b">
                      <div className="panel-heading bg-gray-lighter text-bold">
                        <FormattedMessage id="settings.sidebar.account.title" values={{ email: this.props.email }} />
                      </div>
                      <div className="panel-body">
                        <ChangePasswordForm onSubmit={this.handleFormSubmit} />
                        {this.renderAlert()}
                      </div>
                    </div>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="tabpanel2">
                  <div className="panel b">
                    <div className="panel-heading bg-gray-lighter text-bold">
                      <FormattedMessage id="settings.sidebar.applications.title" />
                    </div>
                    <div className="panel-body">
                      {this.state.alert}
                      <p>
                        <span>
                          <FormattedMessage
                            id="settings.sidebar.applications.text"
                            values={{ appCount: Object.keys(this.props.myApps).length }} />
                        </span>
                      </p>
                      <ul className="list-group">
                        {map(this.props.myApps, myApp =>
                          (<li key={Math.random()} className="list-group-item clearfix">
                            <div className="pull-left mr">
                              <img src={myApp.application.iconUrl} alt="App" className="img-responsive thumb48" />
                            </div>
                            <div className="pull-right" style={{ marginTop: '8px' }}>
                              <button
                                onClick={() =>
                                  this.setState({
                                    alert: (
                                      <SweetAlert
                                        danger
                                        showCancel
                                        confirmBtnText={intl.formatMessage({
                                          id: 'settings.sidebar.applications.delete',
                                        })}
                                        cancelBtnText={intl.formatMessage({ id: 'alert.negative' })}
                                        confirmBtnBsStyle="danger"
                                        cancelBtnBsStyle="default"
                                        title={
                                          <FormattedMessage
                                            id="settings.sidebar.applications.alert.text"
                                            values={{ app: `${myApp.application.label}` }} />
                                        }
                                        onConfirm={() => {
                                          this.setState({ alert: null });
                                        }}
                                        onCancel={() => this.setState({ alert: null })} />
                                    ),
                                  })}
                                type="button"
                                className="btn btn-danger">
                                <strong><FormattedMessage id="settings.sidebar.applications.delete" /></strong>
                              </button>
                            </div>
                            <p className="text-bold mb0">{myApp.application.label}</p>
                            <small>{myApp.application.uid}</small>
                          </li>),
                        )}
                      </ul>
                    </div>
                  </div>
                </Tab.Pane>
              </Tab.Content>
            </Col>
            {this.props.changePasswordSuccess
              ? <SweetAlert
                success
                title={intl.formatMessage({ id: 'settings.sidebar.account.password.alert.success' })}
                confirmBtnText={intl.formatMessage({ id: 'alert.positive' })}
                onConfirm={() => {
                  this.props.invalidateChangePassword();
                }} />
              : ''}
          </Row>
        </Tab.Container>
      </ContentWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.changePasswordErrorMessage,
    changePasswordSuccess: state.auth.changePasswordSuccess,
    submitting: isSubmitting('changePasswordForm')(state),
    email: state.user.email,
    myApps: state.myapps.appList,
    deleteApplicationSuccess: state.myapps.deleteApplicationSuccess,
  };
}

Settings.defaultProps = {
  myApps: {},
  errorMessage: '',
  changePasswordSuccess: false,
};

Settings.propTypes = {
  myApps: PropTypes.object,
  changePassword: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
  changePasswordSuccess: PropTypes.bool,
  invalidateChangePassword: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
  untouch: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(
  connect(mapStateToProps, {
    changePassword,
    invalidateChangePassword,
    change,
    untouch,
  })(Settings),
);
