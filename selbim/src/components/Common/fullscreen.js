// FULLSCREEN
// -----------------------------------
import $ from 'jquery';
import screenfull from 'screenfull';

function toggleFSIcon($element) {
  if (screenfull.isFullscreen) {
    $element.children('em').removeClass('fa-expand').addClass('fa-compress');
  } else {
    $element.children('em').removeClass('fa-compress').addClass('fa-expand');
  }
}

function initScreenfull() {
  if (typeof screenfull === 'undefined') return;

  const $doc = $(document);
  const $fsToggler = $(this);

  // Not supported under IE
  const ua = window.navigator.userAgent;
  if (ua.indexOf('MSIE ') > 0 || !!ua.match(/Trident.*rv\u003a11\./)) {
    $fsToggler.addClass('hide');
  }

  if (!$fsToggler.is(':visible')) {
    return;
  }

  $fsToggler.on('click', (e) => {
    e.preventDefault();

    if (screenfull.enabled) {
      screenfull.toggle();

      toggleFSIcon($fsToggler);
    }
  });

  if (screenfull.raw && screenfull.raw.fullscreenchange) {
    $doc.on(screenfull.raw.fullscreenchange, () => {
      toggleFSIcon($fsToggler);
    });
  }
}

export default () => {
  $('[data-toggle-fullscreen]').each(initScreenfull);
};
