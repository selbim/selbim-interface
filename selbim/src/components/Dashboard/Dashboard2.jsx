import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import Row from 'react-bootstrap/lib/Row';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';

const Dashboard = ({ intl }) =>
  (<ContentWrapper>
    <ContentHeader title={intl.formatMessage({ id: 'dashboard.title' })} />
    <Row>
      dashboard 2
    </Row>
    <Row className="ml-sm mr-sm" />
  </ContentWrapper>);

Dashboard.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Dashboard);
