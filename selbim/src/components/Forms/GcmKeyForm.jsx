import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import { connect } from 'react-redux';
import LoadingText from '../Spinners/LoadingText';

const GCMKeyForm = (props) => {
  const { handleSubmit, submitting, pnDetails, pnDetailsLoading, pnDetailsError, intl } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Row className="mb-lg">
        <Col lg={2} md={2} xs={12} className="text-muted text-break">
          <h5><FormattedMessage id="myapps.gcmkeyform.apikey" /></h5>
        </Col>
        <Col lg={10} md={10} xs={12}>
          <Field className="form-control" name="gcmServerKey" type="text" component="input" />
        </Col>
      </Row>
      <Row>
        <Col lg={2} md={2} xs={12} className="text-muted text-break">
          <h5><FormattedMessage id="myapps.gcmkeyform.senderid" /></h5>
        </Col>
        <Col lg={10} md={10} xs={12}>
          <Field className="form-control" name="gcmSenderId" type="text" component="input" />
        </Col>
      </Row>
      <Row>
        <Col lg={12} md={12} xs={12}>
          <button type="submit" className="btn btn-primary mt-lg pull-right" disabled={submitting}>
            <LoadingText
              loading={pnDetailsLoading}
              error={pnDetailsError}
              data={pnDetails}
              button={{
                sending: intl.formatMessage({ id: 'myapps.appinfoform.send.updating' }),
                error: intl.formatMessage({ id: 'myapps.appinfoform.send.error' }),
                sent: intl.formatMessage({ id: 'myapps.gcmkeyform.send.updatedkeys' }),
                default: intl.formatMessage({ id: 'myapps.gcmkeyform.send.default' }),
                defaultIcon: ' ',
                sentIcon: 'fa fa-fw fa-check',
              }} />
          </button>
        </Col>
      </Row>
    </form>
  );
};

GCMKeyForm.defaultProps = {
  pnDetails: {},
  pnDetailsLoading: false,
  pnDetailsError: false,
};

GCMKeyForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  pnDetails: PropTypes.object,
  pnDetailsLoading: PropTypes.bool,
  pnDetailsError: PropTypes.bool,
  intl: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    pnDetails: state.myapps.selectedApplication.pnDetails,
    pnDetailsLoading: state.myapps.selectedApplication.pnDetailsLoading,
    pnDetailsError: state.myapps.selectedApplication.pnDetailsError,
  };
}

const gcmKeyForm = reduxForm({
  form: 'gcmKeyForm',
})(GCMKeyForm);

export default injectIntl(connect(mapStateToProps)(gcmKeyForm));
