import React from 'react';
import PropTypes from 'prop-types';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';

const validate = (formProps) => {
  const errors = {};
  if (!formProps.password) {
    errors.password = 'Please enter a password';
  }
  return errors;
};

const RenderFieldPassword = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <div className=" has-feedback">
      <input className="form-control" {...input} placeholder={label} type={type} />
      <span className="fa fa-lock form-control-feedback text-muted" />
    </div>
    <span className="alert-danger">{touched && error ? error : '\r'}</span>
  </div>
);

const RecoverCompleteForm = (props) => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit}>
      <p className="text-center">
        Enter your new password
      </p>
      <div className="form-group has-feedback">
        <label htmlFor="resetInputEmail1" className="text-muted">Password</label>
        <Field
          name="password"
          type="password"
          className="form-control"
          component={RenderFieldPassword}
          label="password"
          placeholder="Email" />
      </div>
      <button type="submit" className="btn btn-danger btn-block">Reset</button>
    </form>
  );
};

RenderFieldPassword.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

RecoverCompleteForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'recoverComplete',
  validate,
})(RecoverCompleteForm);
