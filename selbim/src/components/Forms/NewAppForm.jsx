import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';
import Button from 'react-bootstrap/lib/Button';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import TooltipHover from '../Helpers/TooltipHover';

const NewAppForm = ({ handleSubmit, languageOptions, pristine, submitting, intl }) =>
  (<form onSubmit={handleSubmit}>
    <Row className="mb-lg">
      <Col lg={2} md={2} xs={12} className="text-muted text-break">
        <TooltipHover tooltipText="myapps.application.new.tooltip.name">
          <h5>
            {intl.formatMessage({ id: 'myapps.application.new.name' })}{' '}
            <em className="fa fa-question-circle" />
          </h5>
        </TooltipHover>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <Field
          className="form-control"
          name="appName"
          type="text"
          component="input"
          placeholder={intl.formatMessage({ id: 'myapps.application.new.placeholder.name' })} />
      </Col>
    </Row>
    <Row className="mb-lg">
      <Col lg={2} md={2} xs={12} className="text-muted text-break">
        <TooltipHover tooltipText="myapps.application.new.tooltip.packagename">
          <h5>
            {intl.formatMessage({ id: 'myapps.application.new.packagename' })}{' '}
            <em className="fa fa-question-circle" />
          </h5>
        </TooltipHover>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <Field
          className="form-control"
          name="packageName"
          type="text"
          component="input"
          placeholder={intl.formatMessage({ id: 'myapps.application.new.placeholder.packagename' })} />
      </Col>
    </Row>
    <Row>
      <Col lg={2} md={2} xs={12} className="text-muted text-break">
        <TooltipHover tooltipText="myapps.application.new.tooltip.type">
          <h5>
            {intl.formatMessage({ id: 'myapps.application.new.type' })}{' '}
            <em className="fa fa-question-circle" />
          </h5>
        </TooltipHover>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <Field name="applicationType" component="select" className="form-control mb-lg">
          <option value="PRIVATE">{intl.formatMessage({ id: 'myapps.application.new.PRIVATE' })}</option>
          <option value="PUBLIC">{intl.formatMessage({ id: 'myapps.application.new.PUBLIC' })}</option>
        </Field>
      </Col>
    </Row>
    <Row>
      <Col lg={2} md={2} xs={12} className="text-muted text-break">
        <h5>
          {intl.formatMessage({ id: 'myapps.application.new.language' })}
        </h5>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <Field name="defaultLanguage" component="select" className="form-control mb-lg">
          {languageOptions.map(value => <option key={Math.random()}>{value}</option>)}
        </Field>
      </Col>
    </Row>
    <Row>
      <Col lg={2} md={2} xs={12} className="text-muted text-break">
        <h5>
          {intl.formatMessage({ id: 'myapps.application.new.platform' })}
        </h5>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <Row>
          <Col lg={6} md={12} xs={12}>
            <label className="bg-success-dark text-center pv-lg" style={{ width: '100%' }}>
              <Field name="ostype" component="input" type="radio" value="ANDROID" />
              {' '}
              <em className="fa fa-android fa-3x" />
            </label>
          </Col>
          <Col lg={6} md={12} xs={12}>
            <label className="bg-inverse text-center text-white pv-lg" style={{ width: '100%' }}>
              <Field name="ostype" component="input" type="radio" value="IOS" />
              {' '}
              <em className="fa fa-apple fa-3x" />
            </label>
          </Col>
        </Row>
      </Col>
    </Row>
    <Row>
      <Col lg={12} md={12} xs={12}>
        <Button type="submit" className="btn btn-primary mt-lg pull-right" disabled={pristine || submitting}>
          {intl.formatMessage({ id: 'myapps.application.new.addapplication' })}
        </Button>
      </Col>
    </Row>
  </form>);

NewAppForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  languageOptions: PropTypes.array.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(
  reduxForm({
    form: 'newAppForm',
  })(NewAppForm),
);
