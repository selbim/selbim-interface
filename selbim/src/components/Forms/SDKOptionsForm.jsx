import React from 'react';
import PropTypes from 'prop-types';
import reduxForm from 'redux-form/lib/reduxForm';
import { injectIntl, FormattedMessage } from 'react-intl';
import Field from 'redux-form/lib/Field';
import InputGroup from 'react-bootstrap/lib/InputGroup';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover from 'react-bootstrap/lib/Popover';
import Button from 'react-bootstrap/lib/Button';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import CopyToClipboard from 'react-copy-to-clipboard';

const copied = (
  <Popover id="popover-positioned-scrolling-top" title="">
    <strong><FormattedMessage id="myapps.sdkoptionsform.copied" /></strong>
  </Popover>
);
const SDKOptionsForm = ({ handleSubmit, initialValues }) =>
  (<form onSubmit={handleSubmit}>
    <Row className="mb-lg">
      <Col lg={2} md={2} xs={12} className="text-muted">
        <h5><FormattedMessage id="myapps.sdkoptionsform.priority" /></h5>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <InputGroup>
          <Field className="form-control" name="instancePriority" type="text" component="input" disabled />
          <CopyToClipboard text={`${initialValues.instancePriority}`}>
            <InputGroup.Button>
              <OverlayTrigger rootClose trigger="click" placement="left" overlay={copied}>
                <Button className="btn-primary">
                  <em className="fa fa-copy" />
                </Button>
              </OverlayTrigger>
            </InputGroup.Button>
          </CopyToClipboard>
        </InputGroup>
      </Col>
    </Row>
    <Row className="mb-lg">
      <Col lg={2} md={2} xs={12} className="text-muted">
        <h5><FormattedMessage id="myapps.sdkoptionsform.endpoint" /></h5>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <InputGroup>
          <Field className="form-control" name="endPoint" type="text" component="input" disabled />
          <CopyToClipboard text={`${initialValues.endPoint}`}>
            <InputGroup.Button>
              <OverlayTrigger rootClose trigger="click" placement="left" overlay={copied}>
                <Button className="btn-primary">
                  <em className="fa fa-copy" />
                </Button>
              </OverlayTrigger>
            </InputGroup.Button>
          </CopyToClipboard>
        </InputGroup>
      </Col>
    </Row>
    <Row className="mb-lg">
      <Col lg={2} md={2} xs={12} className="text-muted">
        <h5><FormattedMessage id="myapps.sdkoptionsform.license" /></h5>
      </Col>
      <Col lg={10} md={10} xs={12}>
        <InputGroup>
          <Field className="form-control" name="licenseKey" type="text" component="input" disabled />
          <CopyToClipboard text={`${initialValues.licenseKey}`}>
            <InputGroup.Button>
              <OverlayTrigger rootClose trigger="click" placement="left" overlay={copied}>
                <Button className="btn-primary">
                  <em className="fa fa-copy" />
                </Button>
              </OverlayTrigger>
            </InputGroup.Button>
          </CopyToClipboard>
        </InputGroup>
      </Col>
    </Row>
  </form>);

SDKOptionsForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
};

export default injectIntl(
  reduxForm({
    form: 'sdkOptionsForm',
  })(SDKOptionsForm),
);
