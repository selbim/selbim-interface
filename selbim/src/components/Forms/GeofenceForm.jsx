import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import reduxForm from 'redux-form/lib/reduxForm';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import Field from 'redux-form/lib/Field';
import some from 'lodash/some';

const validate = (formProps) => {
  const errors = {};
  if (!formProps.latitude) {
    errors.latitude = <FormattedMessage id="engage.event.geofencemodal.latitudeEmpty" />;
  } else if (!/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/i.test(formProps.latitude)) {
    errors.latitude = <FormattedMessage id="engage.event.geofencemodal.latitudeInvalid" />;
  }
  if (!formProps.longitude) {
    errors.longitude = <FormattedMessage id="engage.event.geofencemodal.longitudeEmpty" />;
  } else if (
    !/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/i.test(
      formProps.longitude,
    )
  ) {
    errors.longitude = <FormattedMessage id="engage.event.geofencemodal.longitudeInvalid" />;
  }
  if (!formProps.radius) {
    errors.radius = <FormattedMessage id="engage.event.geofencemodal.radiusEmpty" />;
  } else if (isNaN(Number(formProps.radius))) {
    errors.radius = <FormattedMessage id="engage.event.geofencemodal.radiusInvalid" />;
  } else if (Number(formProps.radius) > 1000) {
    errors.radius = <FormattedMessage id="engage.event.geofencemodal.radiusMaxWarning" />;
  }
  if (!formProps.name) {
    errors.name = <FormattedMessage id="engage.event.geofencemodal.identifierEmpty" />;
  } else if (formProps.name.length > 16) {
    errors.name = <FormattedMessage id="engage.event.geofencemodal.identifierMaxWarning" />;
  }

  return errors;
};

const renderField = ({ input, label, placeholder, type, inputRef, meta: { touched, error, warning } }) =>
  (<div>
    {label}
    <input {...input} ref={inputRef} className="form-control" placeholder={placeholder} type={type} />
    {touched &&
      ((error && <span className="alert-danger pull-left">{error}</span>) ||
        (warning && <span className="alert-warning pull-left">{warning}</span>))}
  </div>);

renderField.defaultProps = { inputRef() {} };

renderField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  inputRef: PropTypes.func,
};

const validateCampaignName = matchWith => (value) => {
  if (some(matchWith, { circle: { id: value } })) {
    return <FormattedMessage id="engage.event.geofencemodal.identifierDuplicateError" />;
  }
  return undefined;
};

class GeofenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.focusIdentifier = this.focusIdentifier.bind(this);
  }

  componentDidMount() {
    this.inputElement.focus();
  }

  focusIdentifier() {
    this.inputElement.focus();
  }

  render() {
    const { handleSubmit, geoData, intl } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <Row>
          <Col sm={12} lg={12} md={12} xs={12} className="mt-lg">
            <Col sm={12} lg={3} md={12} xs={12}>
              <Field
                name="name"
                type="text"
                inputRef={(el) => {
                  this.inputElement = el;
                  return this.inputElement;
                }}
                component={renderField}
                label={intl.formatMessage({ id: 'engage.event.geofencemodal.identifier' })}
                placeholder={intl.formatMessage({ id: 'engage.event.geofencemodal.placename' })}
                validate={validateCampaignName(geoData)}
                matchWith={geoData} />
            </Col>
            <Col sm={12} lg={3} md={12} xs={12}>
              <Field
                name="latitude"
                type="text"
                component={renderField}
                label={intl.formatMessage({ id: 'engage.event.geofencemodal.latitude' })}
                placeholder="41.038966" />
            </Col>
            <Col sm={12} lg={3} md={12} xs={12}>
              <Field
                name="longitude"
                type="text"
                component={renderField}
                label={intl.formatMessage({ id: 'engage.event.geofencemodal.longitude' })}
                placeholder="29.133682" />
            </Col>
            <Col sm={12} lg={2} md={12} xs={12}>
              <Field
                name="radius"
                type="text"
                component={renderField}
                label={intl.formatMessage({ id: 'engage.event.geofencemodal.radius' })}
                placeholder="100 (meters)" />
            </Col>
            <Col sm={12} lg={1} md={12} xs={12}>
              <div><br /></div>
              <button onClick={this.focusIdentifier} className="btn btn-success btn-block">
                +
              </button>
            </Col>
          </Col>
        </Row>
      </form>
    );
  }
}

GeofenceForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  geoData: PropTypes.array.isRequired,
  intl: PropTypes.object.isRequired,
};

export default reduxForm({
  form: 'geofenceForm',
  touchOnBlur: false,
  validate,
})(GeofenceForm);
