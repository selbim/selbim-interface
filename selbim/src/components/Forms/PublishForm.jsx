import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';
import Button from 'react-bootstrap/lib/Button';
import LocaleUtils from 'react-day-picker/moment';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import moment from 'moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';

const MONTHS = {
  tr: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
  en: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
};
const WEEKDAYS_LONG = {
  tr: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
  en: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
};
const WEEKDAYS_SHORT = {
  tr: ['Pz', 'Pzt', 'Sl', 'Çrş', 'Prş', 'Cm', 'Ct'],
  en: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
};

const validate = (formProps) => {
  const errors = {};
  if (!formProps.campaignName) {
    errors.campaignName = <FormattedMessage id="engage.publish.error.campaigname" />;
  }
  if (!formProps.totalQuota) {
    errors.totalQuota = <FormattedMessage id="engage.publish.error.totalquota" />;
  } else if (formProps.totalQuota < 1 || formProps.totalQuota > 1000000) {
    errors.totalQuota = <FormattedMessage id="engage.publish.error.totalquotavalue" />;
  }
  return errors;
};

const renderField = ({ input, label, type, meta: { touched, error, warning } }) =>
  (<div>
    <input {...input} placeholder={label} type={type} className="form-control pull-left" />
    {touched &&
      ((error &&
        <span className="alert-danger pull-left">
          {error}
        </span>) ||
        (warning &&
          <span className="alert-warning pull-left">
            {warning}
          </span>))}
  </div>);

const renderDatePicker = ({ initialDate, input, dayPickerProps, meta: { touched, error, warning } }) =>
  (<div>
    <DayPickerInput
      className="form-control pull-left"
      {...input}
      onDayChange={day => input.onChange(day)}
      value={
        input.value
          ? moment.unix(moment(input.value, 'YYYY-MM-DD') / 1000).format('YYYY-MM-DD')
          : (moment.unix(initialDate / 1000).format('YYYY-MM-DD'), input.onChange(initialDate))
      }
      dayPickerProps={dayPickerProps} />
    {touched &&
      ((error &&
        <span className="alert-danger pull-left">
          {error}
        </span>) ||
        (warning &&
          <span className="alert-warning pull-left">
            {warning}
          </span>))}
  </div>);

class PublishForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { startDate: moment(), endDate: moment().add(1, 'years') };
  }

  render() {
    const { handleSubmit, intl, submitting, locale } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <Row className="align-center mt-lg">
          <Col lg={6} md={6} sm={12} xs={12}>
            <h4 className="pull-right">
              <FormattedMessage id="engage.publish.title.campaign.name" />
            </h4>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12}>
            <Field
              name="campaignName"
              type="text"
              component={renderField}
              label={intl.formatMessage({ id: 'engage.publish.title.name' })} />
          </Col>
        </Row>
        <Row className="align-center mt-lg">
          <Col lg={6} md={6} sm={12} xs={12}>
            <h4 className="pull-right">
              <FormattedMessage id="engage.publish.title.campaign.description" />
            </h4>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12}>
            <Field
              className="form-control pull-left"
              name="campaignDescription"
              type="text"
              component="input"
              placeholder={intl.formatMessage({ id: 'engage.publish.title.description' })} />
          </Col>
        </Row>
        <Row className="align-center mt-lg">
          <Col lg={6} md={6} sm={12} xs={12}>
            <h4 className="pull-right">
              <FormattedMessage id="engage.publish.title.dailyquota" />
            </h4>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12}>
            <Field
              className="form-control pull-left"
              name="dailyQuota"
              type="text"
              component="input"
              placeholder={intl.formatMessage({ id: 'engage.publish.title.dailyquota.limit' })} />
          </Col>
        </Row>
        <Row className="align-center mt-lg">
          <Col lg={6} md={6} sm={12} xs={12}>
            <h4 className="pull-right">
              <FormattedMessage id="engage.publish.title.totalquota" />
            </h4>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12}>
            <Field
              className="form-control pull-left"
              name="totalQuota"
              type="text"
              component={renderField}
              label={intl.formatMessage({ id: 'engage.publish.title.total.quota.limit.limit' })} />
          </Col>
        </Row>
        <Row className="align-center mt-lg">
          <Col lg={6} md={6} sm={12} xs={12}>
            <h4 className="pull-right">
              <FormattedMessage id="engage.publish.title.startdate" />
            </h4>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12}>
            <Field
              className="form-control pull-left"
              name="startDate"
              initialDate={this.state.startDate}
              component={renderDatePicker}
              dayPickerProps={{
                disabledDays: { before: new Date() },
                localeUtils: LocaleUtils,
                locale,
                months: MONTHS[locale],
                weekdaysLong: WEEKDAYS_LONG[locale],
                weekdaysShort: WEEKDAYS_SHORT[locale],
                firstDayOfWeek: 1,
              }} />
          </Col>
        </Row>
        <Row className="align-center mt-lg">
          <Col lg={6} md={6} sm={12} xs={12}>
            <h4 className="pull-right">
              <FormattedMessage id="engage.publish.title.enddate" />
            </h4>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12}>
            <Field
              className="form-control pull-left"
              name="endDate"
              component={renderDatePicker}
              initialDate={this.state.endDate}
              dayPickerProps={{
                disabledDays: { before: this.state.startDate._d },
                localeUtils: LocaleUtils,
                locale,
                months: MONTHS[locale],
                weekdaysLong: WEEKDAYS_LONG[locale],
                weekdaysShort: WEEKDAYS_SHORT[locale],
                firstDayOfWeek: 1,
              }} />
          </Col>
        </Row>
        <Row className="align-center mt-lg">
          <Col lg={6} md={6} sm={12} xs={12} />
          <Col lg={6} md={6} sm={12} xs={12} className="text-md-right">
            <Button type="submit" className="btn btn-success mt-lg" disabled={submitting}>
              {intl.formatMessage({ id: 'engage.publish.campaign.publish.button' })}
            </Button>
          </Col>
        </Row>
      </form>
    );
  }
}
renderField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

renderDatePicker.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  dayPickerProps: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  initialDate: PropTypes.object.isRequired,
};

PublishForm.defaultProps = {
  locale: 'en',
};

PublishForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
  submitting: PropTypes.bool.isRequired,
  locale: PropTypes.string,
};

function mapStateToProps(state) {
  return {
    locale: state.lang.locale,
  };
}

const form = reduxForm({
  form: 'publishForm',
  mapStateToProps,
  validate,
})(PublishForm);

export default injectIntl(connect(mapStateToProps, null)(form));
