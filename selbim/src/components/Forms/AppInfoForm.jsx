import React from 'react';
import PropTypes from 'prop-types';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import LoadingText from '../Spinners/LoadingText';
import TooltipHover from '../Helpers/TooltipHover';

const AppInfoForm = (props) => {
  const { handleSubmit, submitting, languageOptions, licenseKey, licenseKeyLoading, licenseKeyError, intl } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Row>
        <Col lg={2} md={2} xs={12} className="text-muted">
          <h5><FormattedMessage id="myapps.appinfoform.name" /></h5>
        </Col>
        <Col lg={10} md={10} xs={12}>
          <Field className="form-control mb-lg" name="applicationName" type="text" component="input" />
        </Col>
      </Row>
      <Row>
        <Col lg={2} md={2} xs={12} className="text-muted">
          <h5><FormattedMessage id="myapps.appinfoform.package" /></h5>
        </Col>
        <Col lg={10} md={10} xs={12}>
          <Field className="form-control mb-lg" name="packageName" type="text" component="input" disabled />
        </Col>
      </Row>
      <Row>
        <Col lg={2} md={2} xs={12} className="text-muted">
          <TooltipHover tooltipText="myapps.application.new.tooltip.type">
            <h5>
              <FormattedMessage id="myapps.appinfoform.type" />{' '}
              <em className="fa fa-question-circle" />
            </h5>
          </TooltipHover>
        </Col>
        <Col lg={10} md={10} xs={12}>
          <Field name="applicationType" component="select" className="form-control mb-lg">
            <option>{intl.formatMessage({ id: 'myapps.application.new.PRIVATE' })}</option>
            <option>{intl.formatMessage({ id: 'myapps.application.new.PUBLIC' })}</option>
          </Field>
        </Col>
      </Row>
      <Row>
        <Col lg={2} md={2} xs={12} className="text-muted">
          <h5><FormattedMessage id="myapps.appinfoform.defaultLanguage" /></h5>
        </Col>
        <Col lg={10} md={10} xs={12}>
          <Field className="form-control" name="defaultLanguage" component="select" placeholder="select">
            {languageOptions.map(value => <option key={Math.random()} value={value}>{value}</option>)}
          </Field>
        </Col>
      </Row>
      <Row className="mt-lg">
        <Col lg={2} md={2} xs={12} className="text-muted">
          <h5><FormattedMessage id="myapps.appinfoform.platform" /></h5>
        </Col>
        <Col lg={10} md={10} xs={12}>
          <Field className="form-control mb-lg" name="sdk" type="text" component="input" disabled />
        </Col>
      </Row>
      <Row>
        <Col lg={12} md={12} xs={12} className="mt-lg">
          <div className="pull-right">
            <button type="submit" className="btn btn-primary" disabled={submitting}>
              <LoadingText
                loading={licenseKeyLoading}
                error={licenseKeyError}
                data={licenseKey}
                button={{
                  sending: intl.formatMessage({ id: 'myapps.appinfoform.send.updating' }),
                  error: intl.formatMessage({ id: 'myapps.appinfoform.send.error' }),
                  sent: intl.formatMessage({ id: 'myapps.appinfoform.send.updated' }),
                  default: intl.formatMessage({ id: 'myapps.appinfoform.send.default' }),
                  defaultIcon: ' ',
                  sentIcon: 'fa fa-fw fa-check',
                }} />
            </button>
          </div>
        </Col>
      </Row>
    </form>
  );
};

AppInfoForm.defaultProps = {
  licenseKey: {},
  licenseKeyLoading: false,
  licenseKeyError: false,
};

AppInfoForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  languageOptions: PropTypes.array.isRequired,
  licenseKey: PropTypes.object,
  licenseKeyLoading: PropTypes.bool,
  licenseKeyError: PropTypes.bool,
  intl: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    licenseKey: state.myapps.selectedApplication.licenseKey,
    licenseKeyLoading: state.myapps.selectedApplication.licenseKeyLoading,
    licenseKeyError: state.myapps.selectedApplication.licenseKeyError,
  };
}

const appInfoForm = reduxForm({
  form: 'appInfoForm',
})(AppInfoForm);

export default injectIntl(connect(mapStateToProps)(appInfoForm));
