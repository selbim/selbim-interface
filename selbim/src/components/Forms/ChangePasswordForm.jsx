import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';

const validate = (formProps) => {
  const errors = {};

  if (!formProps.newPassword) {
    errors.newPassword = <FormattedMessage id="settings.sidebar.account.passwordEmpty" />;
  }

  if (formProps.newPassword !== formProps.newPasswordConfirm) {
    errors.newPasswordConfirm = <FormattedMessage id="settings.sidebar.account.passwordMatch" />;
  }
  return errors;
};

const RenderFieldPassword = ({ input, label, type, meta: { touched, error } }) =>
  (<div>
    <div className=" has-feedback">
      <label>{label}</label>
      <input className="form-control" {...input} type={type} />
      <span className="fa fa-lock form-control-feedback text-muted" />
    </div>
    <span className="alert-danger">{touched && error ? error : '\r'}</span>
  </div>);

const ChangePasswordForm = (props) => {
  const { handleSubmit, pristine, submitting, intl } = props;
  return (
    <form onSubmit={handleSubmit} className="mb-lg">
      <Field
        name="newPassword"
        label={intl.formatMessage({ id: 'settings.sidebar.account.password' })}
        type="password"
        component={RenderFieldPassword} />
      <Field
        name="newPasswordConfirm"
        label={intl.formatMessage({ id: 'settings.sidebar.account.passwordConfirm' })}
        type="password"
        component={RenderFieldPassword} />
      <button type="submit" className="btn btn-primary btn-primary mt-lg" disabled={pristine || submitting}>
        {submitting
          ? intl.formatMessage({ id: 'settings.sidebar.account.passwordUpdating' })
          : intl.formatMessage({ id: 'settings.sidebar.account.passwordUpdate' })}
      </button>
    </form>
  );
};

RenderFieldPassword.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

ChangePasswordForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  intl: PropTypes.object.isRequired,
};

const form = reduxForm({
  form: 'changePasswordForm',
  validate,
})(ChangePasswordForm);

export default injectIntl(form);
