import React from 'react';
import PropTypes from 'prop-types';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';

const RenderFieldMail = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <div className=" has-feedback">
      <input className="form-control" {...input} placeholder={label} type={type} />
      <span className="fa fa-envelope form-control-feedback text-muted" />
    </div>
    <span className="alert-danger">{touched && error ? error : '\r'}</span>
  </div>
);

const ResendForm = (props) => {
  const { handleSubmit, buttonColor } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Row>
        <Col lg={12} md={12} xs={12}>
          <Field
            className="form-control"
            name="email"
            type="email"
            component={RenderFieldMail}
            label="Enter your email"
            placeholder="Enter your email" />
        </Col>
      </Row>
      <Row>
        <Col lg={12} md={12} xs={12}>
          <div>
            <button type="submit" className={`btn btn-block mt-lg ${buttonColor}`}>
              Send
            </button>
          </div>
        </Col>
      </Row>
    </form>
  );
};

RenderFieldMail.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

ResendForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  buttonColor: PropTypes.string.isRequired,
};

export default reduxForm({
  form: 'ResendForm',
})(ResendForm);
