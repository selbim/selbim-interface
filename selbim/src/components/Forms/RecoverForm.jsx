import React from 'react';
import PropTypes from 'prop-types';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';

const validate = (formProps) => {
  const errors = {};
  if (!formProps.email) {
    errors.email = 'Please enter an email';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formProps.email)) {
    errors.email = 'Invalid email address';
  }

  return errors;
};

const RenderFieldMail = ({ input, placeholder, type, meta: { touched, error } }) => (
  <div>
    <div className=" has-feedback">
      <input className="form-control" {...input} placeholder={placeholder} type={type} />
      <span className="fa fa-envelope form-control-feedback text-muted" />
    </div>
    <span className="alert-danger">{touched && error ? error : '\r'}</span>
  </div>
);

const RecoverForm = (props) => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit}>
      <p className="text-center">
        Fill with your mail to receive instructions on how to reset your password.
      </p>
      <div className="form-group has-feedback">
        <label htmlFor="resetInputEmail1" className="text-muted">Email address</label>
        <Field name="email" className="form-control" component={RenderFieldMail} type="text" placeholder="Email" />
      </div>
      <button type="submit" className="btn btn-danger btn-block">Reset</button>
    </form>
  );
};

RenderFieldMail.propTypes = {
  input: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

RecoverForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'recover',
  validate,
})(RecoverForm);
