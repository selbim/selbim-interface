import React from 'react';
import { injectIntl } from 'react-intl';
import marked from 'marked';
import Row from 'react-bootstrap/lib/Row';
import ContentHeader from '../Layout/ContentHeader';
import ContentWrapper from '../Layout/ContentWrapper';
import ios280 from './ios_sdk_download-2.8.0.md';
import android280 from './android_sdk_download-2.8.0.md';

class GetSdk extends React.Component {
  constructor(props) {
    super(props);
    this.state = { markdown: '' };
    marked.setOptions({
      renderer: new marked.Renderer(),
      gfm: true,
      tables: true,
      breaks: false,
      pedantic: false,
      sanitize: false,
      smartLists: true,
      smartypants: false,
    });
    this.selectDoc = this.selectDoc.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentWillMount() {
    this.selectDoc(android280);
  }

  selectDoc(docName) {
    fetch(docName)
      .then(response => response.text())
      .then((text) => {
        this.setState({
          markdown: marked(text),
        });
      });
  }

  handleSelect(eventKey) {
    this.setState({ key: eventKey });
    switch (eventKey) {
      case 'android':
        this.selectDoc(android280);
        break;
      case 'ios':
        this.selectDoc(ios280);
        break;
      default:
        this.selectDoc(android280);
        break;
    }
  }

  render() {
    /*eslint-disable*/
    const { markdown } = this.state;
    return (
      <ContentWrapper>
        <ContentHeader title={this.props.intl.formatMessage({ id: 'sidebar.items.getsdk' })} />
        <Row>
        contents here
        </Row>
      </ContentWrapper>
    );
    /*eslint-enable*/
  }
}

GetSdk.propTypes = {
  // properties: PropTypes.object.isRequired,
};
export default injectIntl(GetSdk);
