> Pick one of the options below to download Veloxity iOS SDK.

#### 1. Minimum IOS version 8.0 and above

* If the minimum IOS version of the project above 8.0, you are free to use dynamic framework including
the resource files. You need to add VeloxitySDK.framework to the embedded framework section of
Build Phases.

[Veloxity SDK Framework Download (armv7 arm64)](https://sta01.veloxity.net/vlx_dv/frameworks/2.8.0/dynamic/VeloxitySDK.framework.zip)

[Veloxity SDK Framework Download For Emulators (armv7 i386 x86_64 arm64)](https://sta01.veloxity.net/vlx_dv/frameworks/2.8.0/dynamic/VeloxitySDK.framework.fat.zip)

<hr />

#### 2. Minimum IOS version under 8.0

* If the project is set to a value under IOS 8.0 (such as 6.0, 7.0), App Store will reject the application with
dynamic framework since it’s introduced in IOS 8.0. There’s an App Store policy to control the wrong
usage.

[Veloxity SDK Framework  Download (armv7 arm64)](https://sta01.veloxity.net/vlx_dv/frameworks/2.8.0/static/VeloxitySDK.framework.zip)

[Veloxity SDK Framework  Download For Emulators (armv7 i386 x86_64 arm64)](https://sta01.veloxity.net/vlx_dv/frameworks/2.8.0/static/VeloxitySDK.framework.fat.zip)

> For more detailed information visit [documentation](./documentation) page.
