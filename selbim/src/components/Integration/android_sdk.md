<p align="left" style="font-size: 25px">
	<img src="/img/markdown/android_logo.png" width="150"><br /><br />
	<strong>
		VELOXITY ANDROID SDK<br />
		Version 2.8.0
        </strong>
</p>

## **Abstract**

This document outlines conceptual overviews, programming tasks, and integration guide with respect to Veloxity Android SDK.

**EMAIL**: [developer@blinnk.com](mailto:developer@blinnk.com)

**DATE**: 09/11/2017

Distribution is subject to copyright laws.

## **Contents**

- 1 <a href="#introduction-to-veloxity-sdk">Introduction to Veloxity SDK</a>
- 2 <a href="#how-veloxity-android-sdk-operate">How Veloxity Android SDK Operate?</a>
- 3 <a href="#minimum-system-requirements">Minimum System Requirements</a>
- 4 <a href="#integration-steps">Integration Steps</a>
	- 4.1 <a href="#add-veloxity-sdk-to-your-project">Add Veloxity SDK to your project</a>
		 - 4.1.1 <a href="#local-aar-file">Local AAR file</a>
		 - 4.1.2 <a href="#remote-maven-repository">Remote Maven repository</a>
	- 4.2 <a href="#veloxity-service-label-definition">Veloxity Service Label Definition</a>
	- 4.3 <a href="#permission-id-definition">Permission Id Definition</a>
	- 4.4 <a href="#start-veloxity-service">Start Veloxity Service</a>
		 - 4.4.1 <a href="#with-data-usage-dialog">With Data Usage Dialog</a>
		 - 4.4.2 <a href="#without-data-usage-dialog">Without Data Usage Dialog</a>
	- 4.5 <a href="#opt-in-opt-out">Opt-in / Opt-out</a>
	- 4.6 <a href="#unique-device-id">Unique Device ID</a>
	- 4.7 <a href="#send-custom-data">Send Custom Data</a>
	- 4.8 <a href="#application-class">Application Class</a>
	- 4.9 <a href="#send-msisdn-info">Send Msisdn Info</a>
	- 4.10 <a href="#collect-session-analytics">Collect Session Analytics</a>
- 5 <a href="#synthetic-transactions">Synthetic Transactions</a>
	- 5.1 <a href="#overview">Overview</a>
	- 5.2 <a href="#functions">Functions</a>
		- 5.2.1 <a href="#initialization">Initialization</a>
		- 5.2.2 <a href="#wi-fi-speed-test">Wi-Fi Speed Test</a>
		- 5.2.3 <a href="#download-speed-test">Download Speed Test</a>
		- 5.2.4 <a href="#upload-speed-test">Upload Speed Test</a>
		- 5.2.5 <a href="#ping-(gateway)">Ping (Gateway)</a>
		- 5.2.6 <a href="#ping-(isp)">Ping (ISP)</a>
		- 5.2.7 <a href="#ping-(host)">Ping (Host)</a>
		- 5.2.8 <a href="#delay-test">Delay Test</a>
		- 5.2.9 <a href="#loaded-ping">Loaded Ping</a>
		- 5.2.10 <a href="#network-trace">Network Trace</a>
- 6 <a href="#push-notification">Push Notification</a>
	- 6.1 <a href="#libraries">Libraries</a>
	- 6.2 <a href="#manifest-file">Manifest File</a>
	- 6.3 <a href="#register-for-push-notifications">Register for Push Notifications</a>
	- 6.4 <a href="#lack-of-google-play-services">Lack of Google Play Services</a>
	- 6.5 <a href="#if-application-has-already-gcm-service">If Application has already GCM Service</a>
- 7 <a href="#transaction-management">Transaction Management</a>
- 8 <a href="#using-veloxity-sdk-with-proguard">Using Veloxity SDK with Proguard</a>
- 9 <a href="#privacy-policy">Privacy Policy</a>
- 10 <a href="#sample-application">Sample Application</a>

## **Change Log**

| Version       | Date          | Exp.                                                                |
|:--------------|:--------------|:--------------------------------------------------------------------|
| 2.4.0         | 07/28/2015    | Added section G 						      |
| 2.4.1         | 08/22/2015    | Updated the section D-5 & F-3 			              |
| 2.4.2         | 09/02/2015    | Updated the section D-3 & Added section H. 		              |
| 2.4.3         | 09/16/2015    | Edited the section D-5, D-6, F-2 & H			              |
| 2.4.4 	| 12/04/2015    | Edited Section D-5.2, E-2.2, E-2.3, E-2.4, E-2.5, E-2.6, E-2.7 & H  |
| 2.5.0 	| 01/13/2015    | Edited Section B, C, D.1, D.2, D.3, D.4, D5, F, F.1,F.2, F.3, F.4, H|
| 2.5.4         | 02/03/2016    | All sections are edited and formatted.	                      |
| 2.5.5         | 02/15/2016    | Added section 6.5, 9 & updated 3, 4.4, 8 		              |
| 2.5.6 / 2.5.7 | 03/17/2016    | Updated 4.4						              |
| 2.6.0         | 04/25/2016    | Updated 4.4, added 4.7, updated 9, added 10. 		              |
| 2.6.2/2.6.3   | 08/31/2016    | Added 4.8, 4.9                                                      |
| 2.7.0/2.7.5   | 5/11/2017	| Updated 5.2.3, 5.2.4 					              |
| 2.7.6         | 6/15/2017	| Updated 4.4                                                         |
| 2.7.7         | 6/21/2017	| Updated 4.4                                                         |
| 2.8.0         | 9/11/2017	| Updated 4.1, Added 4.10                                            |

<p align="left" style="font-size: 40px">
<br />
	<strong>
	VELOXITY ANDROID SDK<br />
		</strong>
</p>

<div id='introduction-to-veloxity-sdk'><a name='Introduction to Veloxity Sdk'></a></div>

# **1 Introduction to Veloxity SDK**

Veloxity Android SDK is used to collect network diagnostics data from mobile devices in which the SDK is attached to one of the installed apps. The collected data serves as a means to analyze the user’s Wi-Fi & cellular network connection details to identify issues affecting network speed. Data usage and location data are also collected for improved troubleshooting and reporting purposes. Collected data is stored in the Cloud environment which is specified by the application developer. All the features and variables can be manipulated from cloud online. It is optimized in all aspects that battery usage is less than 1%.

<p align="center"><img id="introductionSchema" src="/img/markdown/introduction_schema.png" width="550"></p>

<div id='how-veloxity-android-sdk-operate'><a name='How Veloxity Android Sdk Operate'></a></div>

# **2 How Veloxity Android SDK Operate?**

Veloxity library works as a background service. When **Veloxity.initialize()** method is called, the service starts in the background. Once registered as a background service Veloxity agent starts automatically on each device-restart.

If more than one registered Veloxity SDK instances(applications) are present in the device only through multiple apps, one instance runs at a given time. SDK instances communicate with each other via broadcast messages. If the running instance crashes or the relevant app is uninstalled from the device, a different instance shall start immediately.

Broadcast messages work only in their internal operator network. No application can listen these messages. Only the applications signed with the same key will be able to communicate with the others. The priority parameter in Veloxity Options determines the selection of the active instance.

<div id='minimum-system-requirements'><a name='Minimum System Requirements'></a></div>

# **3 Minimum System Requirements**

Veloxity SDK supports:
-	SDK: Android API Level 14 and above
-	OS: Android 4.0+
-	Emulator Target Google APIs 14+

SDK can be integrated to any kind of application which has the minimum API level below 14. SDK just won’t initialize for these levels (1-13).

<div id='integration-steps'><a name='Integration Steps'></a></div>

# **4 Integration Steps**

<div id='add-veloxity-sdk-to-your-project'><a name='Add Veloxity SDK to Your Project'></a></div>

### **4.1 Add Veloxity SDK to your project**

<div id='local-aar-file'><a name='Local AAR file'></a></div>

#### **4.1.1 Local AAR file**

 - copy **veloxitysdk-version_number.aar** to the libs folder of your project.
 -  Define local libs folder path to the **build.gradle**:

``` javascript
repositories {
	flatDir {
	    dirs 'libs'
	}
}
```
 - Add these libraries as dependency to the **build.gradle** file of your application:

``` javascript
dependencies {
	compile 'com.google.android.gms:play-services-gcm:11.0.4'
	compile 'com.google.android.gms:play-services-location:11.0.4'
	compile 'com.google.android.gms:play-services-awareness:11.0.4'
	compile(name: 'veloxitysdk-version_number', ext: 'aar')
}
```

Note: There’s no need to add anything to the AndroidManifest.xml file. Permissions and definitions will be merged after adding SDK AAR file.

<div id='remote-maven-repository'><a name='Remote Maven Repository'></a></div>

#### **4.1.2 Remote Maven repository**

 - Define remote Maven URL to the **build.gradle**:

``` javascript
repositories {
	maven{
		credentials{
			username "CUSTOMER_USERNAME"
			password "CUSTOMER_PASSWORD"
		}
		url 'https://maven.veloxity.net/nexus/content/repositories/releases/'
	}
}
```
 - Add the library as dependency to the  **build.gradle** file of your application:

``` javascript
dependencies {
	compile 'net.veloxity.sdk:veloxitysdk:<version_number>'
}
```
Note: There’s no need to add anything to the **AndroidManifest.xml** or  **build.gradle.** Other dependencies **play-services-gcm**, **play-services-location** and **play-services-awareness** will be downloaded automatically by Gradle system. Permissions and definitions will be merged after adding library as dependency.

<div id='veloxity-service-label-definition'><a name='Veloxity Service Label Definition'></a></div>

## **4.2 Veloxity Service Label Definition**

Veloxity SDK can automatically use your application name what you want to see as the process name of your application on the running process list of the Android settings menu.  Please check if there’s any variable defined in the **strings.xml** correctly:
``` xml
<string name="app_name">Write a Service Label Name</string>
```
Note: If you don’t define anything in here, SDK will use the label name as “**Data Service**” only for default strings.xml file (values/strings.xml) and won’t consider looking at other translations like us, fr, de, tr.

<p align="center">
	<img id="runningApp" src="/img/markdown/running_app.png" width="600" />
</p>

<div id='permission-id-definition'><a name='Permission Id Definition'></a></div>

## **4.3 Permission Id Definition**

If more than one registered Veloxity SDK instances are present in the device only through multiple apps, one instance runs at a given time. SDK instances communicate with each other via broadcast messages. In order to provide better and secure communication, please define a permission id in your **build.gradle** file:

```java
android {
	defaultConfig {
		applicationId "net.veloxity.application"
		manifestPlaceholders = [permissionId: "PERMISSION_ID"]
	}
}
```

Note: PERMISSION_ID must be the same for all application published by the operator. Please get the correct PERMISSION_ID from [**developer@blinnk.com**](mailto:developer@blinnk.com)

<div id='start-veloxity-service'><a name='Start Veloxity Service'></a></div>

## **4.4 Start Veloxity Service**

In order to initialize and start the Veloxity agent, the **initialize()** function needs to be called within the **onCreate()** method of your Main Launcher class. LICENSE_KEY, APP_CODE(@deprecated), APP_PRIORITY, and ENDPOINT_URL parameters should be given to you. (Please contact us at, [**developer@blinnk.com**](mailto:developer@blinnk.com) for getting this information)

APP_CODE(@deprecated) and LICENSE_KEY separates the application from the others. Veloxity will provide different LICENSE_KEY(s) in order to use your applications. LICENSE_KEY is generated only for one application and directly dependent to the package name of the application.

<p align="center" style="font-size: 15px">
<br /><strong>
	Starting from 2.6.0 version, Veloxity SDK can identify the application just only with the LICENSE_KEY. APP_CODE is deprecated and no need to use/set the APP_CODE.
		</strong><br /></p>

APP_PRIORITY parameter should be in range [0-10]. It provides a priority to running SDK if you publish multiple applications including the Veloxity SDKs to Play Store. The one has the bigger priority will be the running SDK to collect data from the phone, all others will be in silent mode unless active one is uninstalled. If you use the same priority for different applications, SDK started first (start time) will be the active one.

GCM_SENDER_ID is used by Android app to register a physical device with GCM to be able to receive notifications from GCM from particular 3rd party server. All the push notification system is ready to use in the SDK. Only thing you should do is to pass this information via **VeloxityOptions** object to the SDK.

First of all, define a **VeloxityOptions** object to enter the parameters depending on the regulation of your country:

<div id='with-data-usage-dialog'><a name='With Data Usage Dialog'></a></div>

### **4.4.1 With Data Usage Dialog**
``` java
try {
	VeloxityOptions veloxityOptions = new VeloxityOptions.Builder(MainActivity.this)
																	.setPriority(APP_PRIORITY)
																	.setLicenseKey(LICENSE_KEY)
																	.setWebServiceEndpoint("https://YOUR_ENDPOINT_URL")
																	.setGCMSenderID(GCM_SENDER_ID)
																	.setDialogTitle(DATA_USAGE_TITLE)
																	.setDialogMessage(DATA_USAGE_MESSAGE)
																	.setListener(new DataUsageListener() {
																		@Override
																		public void onDataUsageResult(boolean isServiceStarted) {
																		  // TODO
																		}
																		@Override
																		public void onCompleted() {
																		  // TODO
																		}
																	})
																	.build();
	Veloxity.initialize(veloxityOptions);
}
catch (LicenseException | GCMSenderIdException e) {
	e.printStackTrace();
}
```
DATA_USAGE_TITLE, DATA_USAGE_MESSAGE parameters and **DataUsageListener()** method make you control the data usage page and get the callbacks accordingly.

<p align="center">
<img id="dataUsage" src="/img/markdown/data_usage.png" width="600">
</p>

Data Usage Page is automatically shown on the screen before initializing the SDK. Based on the end user response, SDK decides what to do next. If user agrees the terms and conditions, it begins to collect data and give a callback to the application developer via **DataUsageListener()** method.

You can change the Title and the Message content of the Alert Dialog by DATA_USAGE_TITLE and DATA_USAGE_MESSAGE parameters.

Optionally, you can use these two methods for changing the content of positive and negative buttons on the Data Usage page: (**default: Accept/Decline**)
``` java
.setDialogPositive(POSITIVE_BUTTON_STRING)
.setDialogNegative(NEGATIVE_BUTTON_STRING)
```
There’s also another optional parameter to change typeface of the dialog content (including dialog title, message, positive and negative button):
``` java
.setDialogTypeFace(FONT_PATH_IN_ASSET_FOLDER)
```
If the font is in a folder in **assets** folder, you should write the string as “**fonts/font_name.ttf**”, otherwise only file name is pretty enough to set it out. (“**font_name.ttf**”)

Style of Data Usage dialog can be modified easily for providing consistency between your activities. Only thing you should do is adding these fields to sytles.xml file and editing based on your requirements:
``` xml
<style name="vlx_dialog" parent="@android:style/YOUR_STYLE">
	//  YOUR STYLE ITEMS..
	<items …/>
</style>
```
Please don’t forget to add “**privacy policy**” link to the **DATA USAGE DIALOG** to make sure your app is compliant with the **User Data policy** and all other policies listed in the **Developer Program Policies**. Please check the **Privacy Policy** section for further information.

<div id='without-data-usage-dialog'><a name='Without Data Usage Dialog'></a></div>

### **4.4.2 Without Data Usage Dialog**

If the regulation of your country doesn’t push you to show a warning to the end user, you can remove the dialog related stuffs from the **VeloxityOptions** object. Hence service will start instantly after you call the initialize method of the SDK.
``` java
try {
	VeloxityOptions veloxityOptions = new VeloxityOptions.Builder(MainActivity.this)
																			.setPriority(APP_PRIORITY)
																			.setLicenseKey(LICENSE_KEY)
																			.setWebServiceEndpoint("https://YOUR_ENDPOINT_URL")
																			.setGCMSenderID(GCM_SENDER_ID)
																			.build();
	Veloxity.initialize(veloxityOptions);
}
catch (LicenseException | GCMSenderIdException e) {
	e.printStackTrace();
}
```
After Android Marshmallow, SDK automatically shows the required permission dialogs to the end user for READ_PHONE_STATE and ACCESS_COARSE_LOCATION unless you disable and decide to manage them by yourself. To succeed it, you need to set this method to **VeloxityOptions** object:
``` java
.setNeverAskedPermission(true)
```


<div id='opt-in-opt-out'><a name='Opt-in / Opt-out'></a></div>

## **4.5 Opt-in / Opt-out**

Veloxity SDK allows you to opt-in/opt-out the data collection service.
You can disable the service by calling this method in anywhere:
``` java
Veloxity.optOut(getApplicationContext());
```
After calling the method, Veloxity Service will be completely stopped and terminate the data collection session.
On the other hand, you are able to re-enable the service by calling the initialization method of SDK similarly:
``` java
try {
	VeloxityOptions veloxityOptions = new VeloxityOptions.Builder(MainActivity.this)
																	.setPriority(APP_PRIORITY)
																	.setLicenseKey(LICENSE_KEY)
																	.setWebServiceEndpoint("https://YOUR_ENDPOINT_URL")
																	.setGCMSenderID(GCM_SENDER_ID)
																	.setDialogTitle(DATA_USAGE_TITLE)
																	.setDialogMessage(DATA_USAGE_MESSAGE)
																	.setListener(new DataUsageListener() {
																	  @Override
																	  public void onDataUsageResult(boolean isServiceStarted) {
																	     // TODO
																	  }
																	  @Override
																	  public void onCompleted() {
																	     // TODO
																	  }
																	})
																	.build();
	Veloxity.initialize(veloxityOptions);
}
catch (LicenseException | GCMSenderIdException e) {
	e.printStackTrace();
}
```
If you don't want to show the data usage dialog, remove the given part below
``` java
.setDialogTitle(DATA_USAGE_TITLE)
.setDialogMessage(DATA_USAGE_MESSAGE)
.setListener(new DataUsageListener() {
	@Override
	public void onDataUsageResult(boolean isServiceStarted){
		// TODO
	}
	@Override
	public void onCompleted(){
		// TODO
	}
}).
```
If you want to learn the status of Veloxity Service, if it is really running or not, you should call the **isServiceRunning()** method:
``` java
Veloxity.isServiceRunning(getApplicationContext());
```
It returns true or false as boolean.

<div id='unique-device-id'><a name='Unique Device ID'></a></div>

## **4.6 Unique Device ID**

Veloxity Android SDK provides a public method for getting a unique device ID. You can call this method statically:
~~~ java
Veloxity.getDeviceId(getApplicationContext());
~~~
<div id='send-custom-data'><a name='Send Custom Data'></a></div>

## **4.7 Send Custom Data**

SDK provides a public method to pass custom data to the cloud side. It can be related with any kind of information application already has. (age, gender, credits, etc.)
``` java
Veloxity.sendCustomData(context, JSONObject);
```
Application must call this method with a context and a JSON object. Then SDK handles the rest and sends the custom JSON content to the cloud side.

<div id='application-class'><a name='Application Class'></a></div>

## **4.8 Application Class**

If your application uses the **Application** class, you must add this control to beginning of **onCreate()** method in order to prevent running twice for the Veloxity process(**:pricing**):
``` java
public class VeloxityApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		if (Veloxity.isInVeloxityProcess(this))
		return;
	}
}
```
<div id='send-msisdn-info'><a name='Send Msisdn Info'></a></div>

## **4.9 Send Msisdn Info**

SDK provides a public method for passing msisdn information of the end user.  After SDK activation, application can pass the (String) information to the SDK via this method:
``` java
Veloxity.sendCustomData(context, msisdnString);
```
<div id='collect-session-analytics'><a name='Collect Session Analytics'></a></div>

## **4.10 Collect Session Analytics**

In order to bind and listen the lifecycle callbacks Android provides, you should call **Veloxity.registerLifeCycleCallbacks(this)** method in the application class. Hence SDK will listen the enter/exit conditions and collect session duration as well:

``` java
public class VeloxityApplication extends Application {

   @Override
   public void onCreate() {
      super.onCreate();
      if (Veloxity.isInVeloxityProcess(this)) {
         return;
      }
      Veloxity.registerLifeCycleCallbacks(this);
   }

}
```
Note: This method should be called after **Veloxity.isInVeloxityProcess(this)** method if it's used.

<div id='synthetic-transactions'><a name='Synthetic Transactions'></a></div>

# **5 Synthetic Transactions**

<div id='overview'><a name='Overview'></a></div>

## **5.1 Overview**

Synthetic Transaction Execution is a feature that carries out some network related evaluation operations automatically. Before Veloxity’s Synthetic Transaction feature, these kinds of evaluations would be run by the user manually several times.

The synthetic transaction functions available through Veloxity Android SDK are listed below.

<div id='functions'><a name='Functions'></a></div>

## **5.2 Functions**

<div id='initialization'><a name='Initialization'></a></div>

### **5.2.1 Initialization**

Before using any method of synthetic transaction tests, initialize method has to be called. Calling this method once is enough to carry out the evaluations. This function downloads the pre-defined values and prepares the environment.

**NetworkSpeedManager.initNetworkTest()** carries out the initialization requirement.
``` java
NetworkSpeedManager speedManager = new NetworkSpeedManager();
speedManager.initNetworkTest(getActivity(), new SettingsDownloadListener() {
	 @Override
	 public void onSettingsDownloaded() {
			// to do
	 }
});
```
<div id='wi-fi-speed-test'><a name='Wi-Fi Speed Test'></a></div>

### **5.2.2 Wi-Fi Speed Test**

Used to evaluate the LAN speed by using UDP datagram packages.
``` java
speedManager.startWifiTest(getActivity(), new WifiTestResultListener() {
    @Override
    public void onError() {
        //to do
    }
    @Override
    public void onWifiTestResult(double[] xArr, double[] yArr, double mbps, int status, String name) {
        //to do
    }, new PartialTestResultListener() {
        @Override
        public void onPartialTestResult(double mbps, int index) {
            //to do
        }
    }
}, assetPath, sampleCount);
```
- **onPartialTestResult()** will inform the application every second and contains these fields:
	- **mbps**: Average LAN speed in the last 1 second.
	- **index**: Index of the current test. Will also be used as second value.
- **onWifiTestResult()** will be triggered as soon as the Wi-Fi Test is fully completed.
	- **xArr[]**: Includes indexes of seconds like 1, 2, 3, ...., 15
	- **Arr[]**: Includes speeds per second as mbps. x[5] = mbps in the 5th second.
	- **mbps**: Average LAN speed
	- **status**: Indicates the result of the Wi-Fi test. 0: BAD, 1: AVERAGE, 2: GOOD
	- **name**: Result text of the Wi-Fi test result (BAD, AVERAGE or GOOD)
- **assetPath** will be the path of the file in assets folder of the android project. If it’s in a sub directory, you should define like this: "folder/filename.zip", otherwise "filename.zip" will be enough. If you set it as “null”, SDK will generate a dummy file to upload.
- **sampleCount** will determine the test duration. If you set it as 20, test will finish after 20 seconds.

<div id='download-speed-test'><a name='Download Speed Test'></a></div>

### **5.2.3 Download Speed Test**

Used to evaluate the user’s download speed from a given source.
``` java
speedManager.startDownloadTest(getActivity(), new SpeedTestResultListener() {
	@Override
	public void onSpeedTestResult(ArrayList<Double> arrMbps, double mbps, int status, String name) {
	    //to do
	}
	@Override
	public void onError() {
	    //to do
	}
	@Override
	public void uploadDataSuccess() {
	    //to do
	}
	@Override
	public void uploadDataFailure() {
	    //to do
	}, new PartialTestResultListener() {
	    @Override
	    public void onPartialTestResult(double mbps, int index) {
	        //to do
	    }
	}, downloadUrl, durationInMs, threadCount);
```
- **onSpeedTestResult()** will be triggered as soon as the download test is finished.
	- **arrMbps**: Keeps download speed values per second.
	- **mbps**: Average download speed
	- **status**: Indicates the result of the download-speed test.
		- 0: BAD
		- 1: AVERAGE
		- 2: GOOD
	- **name**: Result text of the download-speed test result (BAD, AVERAGE or GOOD)
- **onError()** is fired when any error occurs during the test.
- **uploadDataSuccess()** will be triggered after the speed test results is sent to the cloud successfully.
- **uploadDataFailure()** will be triggered after the speed test results is failed during the upload session.
- **onPartialTestResult()** , see function 5.2.2
- **downloadUrl** is the complete URL of the file like "http://domain.com/file.zip".
- **threadCount** will determine the count of asynchronous web calls. Default value is 5.

<div id='upload-speed-test'><a name='Upload Speed Test'></a></div>

### **5.2.4 Upload Speed Test**

Used to evaluate the user’s data upload speed by uploading a certain amount of sample data to a defined end point.
``` java
speedManager.startUploadTest(getActivity(), new SpeedTestResultListener() {
	@Override
	public void onSpeedTestResult(ArrayList<Double> arrMbps, double mbps, int status, String name) {
	    //to do
	}
	@Override
	public void onError(){
	    //to do
	}
	@Override
	public void uploadDataSuccess(){
	    //to do
	}
	@Override
	public void uploadDataFailure () {
	    //to do
	}, new PartialTestResultListener() {
	    @Override
	    public void onPartialTestResult(double mbps, int index) {
	        //to do
	    }
	}, uploadUrl, assetPath, durationInMs, threadCount);
```

-	**onSpeedTestResult()** , see function 5.2.3
-	**onError()** , see function 5.2.3
-	**uploadDataSuccess()** , see function 5.2.3
-	**uploadDataFailure()** , see function 5.2.3
-	**onPartialTestResult()** , see function 5.2.2
-	**uploadUrl** is the complete URL handler to upload the file like http://domain.com/upload
-	**assetPath** , see function 5.2.2
-	**threadCount** will determine the count of asynchronous web calls. Default value is 5.

<div id='ping-(gateway)'><a name='Ping (Gateway)'></a></div>

### **5.2.5 Ping (Gateway)**

Used to carry out ping test to the connected gateway or Wi-Fi hotspot. The function finds the gateway IP address internally.
``` java
speedManager.pingGateway(getActivitiy(), new PingTestResultListener() {
	@Override
	public void onPingResult(String gatewayIp,
							String host,
							String isp,
							PingResult delayGateway,
							PingResult delayHost,
							PingResult delayIsp) {
	  // to do
	}

	@Override
	public void onError() {
	  // to do
	}

	@Override
	public void uploadDataSuccess() {
	  // to do
	}

	@Override
	public void uploadDataFailure() {
	  // to do
	}
}, pingCount);
```
-	  **onPingResult()** will be triggered as soon as ping test is finished.
	- **gatewayIP**: IP address of the gateway
	- **host**: This value is null specific to this function.
	- **isp**: This value is null specific to this function.
	- **delayGateway**: Test result container object.
	- **delayHost**: This value is null specific to this function.
	- **delayIsp**: This value is null specific to this function.

<div id='ping-(isp)'><a name='Ping (ISP)'></a></div>

### **5.2.6 Ping (ISP)**

Used to carry out ping test to the gateway in the ISP side. The function finds the ISP gateway’s IP address internally.
``` java
speedManager.pingISP(getActivitiy(), new PingTestResultListener() {
	@Override
	public void onPingResult(String gatewayIp,
							String host,
							String isp,
							PingResult delayGateway,
							PingResult delayHost,
							PingResult delayIsp) {
	  // to do
	}

	@Override
	public void onError() {
	  // to do
	}

	@Override
	public void uploadDataSuccess() {
	  // to do
	}

	@Override
	public void uploadDataFailure() {
	  // to do
	}
}, pingCount);
```
-	**onPingResult()** will be triggered as soon as ping test is finished.
	- **gatewayIP**: IP address of the gateway
	- **host**: This value is null specific to this function.
	- **isp**: The IP address of the ISP’s gateway.
	- **delayGateway**: This value is null specific to this function.
	- **delayHost**: This value is null specific to this function.
	- **delayIsp**: The result container object.

<div id='ping-(host)'><a name='Ping (Host)'></a></div>

### **5.2.7 Ping (Host)**

Used to carry out ping test to a given host entry (IP or host).
``` java
speedManager.pingHost(getActivity(), new PingTestResultListener() {
	@Override
	public void onPingResult(String gatewayIp,
							String host,
							String isp,
							PingResult delayGateway,
							PingResult delayHost,
							PingResult delayIsp) {
	    // to do
	}

	@Override
	public void onError() {
	    // to do
	}

	@Override
	public void uploadDataSuccess() {
	    // to do
	}

	@Override
	public void uploadDataFailure() {
	    // to do
	}
}, pingCount);
```
-	**onPingResult()** will be triggered as soon as ping test is finished.
	- **gatewayIP**: This value is null specific to this function
	- **host**: The host IP address of the pinged end point
	- **isp**: This value is null specific to this function.
	- **delayGateway**: This value is null specific to this function.
	- **delayHost**: The result container object.
	- **delayIsp**: This value is null specific to this function.

<div id='delay-test'><a name='Delay Test'></a></div>

### **5.2.8 Delay Test**

Used to evaluate the user’s delay to a given host entry by each endpoint (gateway, ISP and host). This function calls pingGateway, pingISP, and pingHost functions internally.
``` java
speedManager.startDelayTest(getActivity(), new PingTestResultListener() {
	@Override
	public void onPingResult(String gatewayIp,
							String host,
							String isp,
							PingResult delayGateway,
							PingResult delayHost,
							PingResult delayIspt) {
	   //to do
	}
},
new PingTestPartResultListener(){
	@Override
	public void onPingPartTestResult(double delay, int index, PING_TYPE type){
	   //to do
	}
}, new PingIpListener(){
@Override
public void onPingIpResult(String gateway, String isp, String broadband) {
   //to do
}, pingUrl);
```
- **onPingPartTestResult()** is called once for each step and 3 times in total.
	- **delay**: The delay value in milliseconds.
	- **index**: The index value during evaluation.
	- **PING_TYPE**: The type of the ping operation. It may have these values:
		- **Gateway**
		- **ISP**
		- **Host**
- **onPingIpResult()** is called once for each step and 3 times in total. This function is called when the IP address of the endpoint is resolved.
	- **gateway**: The IP address of the LAN gateway.
	- **isp**: This value is null specific to this function.
	- **broadband**: The IP address of the host entry.
- **onPingResult()** will be triggered as soon as the test is fully completed.
	- **gatewayIP**: IP address of the gateway
	- **host**: The host IP address of the pinged end point
	- **isp**: IP address of the gateway on the ISP side.
	- **delayGateway**: The result container object for the ping test against the LAN gateway. (or router)
	- **delayHost**: The result container object for the ping test against the host entry.
	- **delayIsp**: The result container object for the ping test against the ISP’s gateway.

<div id='loaded-ping'><a name='Loaded Ping'></a></div>

### **5.2.9 Loaded Ping**

Similar to the “delay test” function. The difference is that parallel thread count and ping package sizes can be adjusted. By this mean, maximum load of ping packages is attempted to send to gateway, ISP gateway, and remote host.
``` java
LoadedPingManager.startLoadedPing(context, pingCountPerThread, packetSize, threadCount, hostUrl, new PingListener() {
	@Override
	public void onPingResult(double delay, PING_TYPE type) {
		if (type == PING_TYPE.GATEWAY) {
			 // to do
		}
		else if (type == PING_TYPE.ISP) {
			 // to do
		}
		else if (type == PING_TYPE.HOST) {
			 // to do
		}
}
});
```
**pingCountPerThread**: How many pings are to be sent by a single thread.
**packetSize**: Packet size that will be used in per ping as bytes.
**threadCount**: How many simultaneous threads are to be run for the evaluation.
**hostUrl**: Remote host or IP address to ping
**onPingResult(delay, type)**: Triggered after each thread completes its ping process. Delay is the average ping result in milliseconds. Type can be **GATEWAY, ISP** or **HOST**.

<div id='network-trace'><a name='Network Trace'></a></div>

### **5.2.10 Network Trace**

Used to gather information about other devices that are connected to the same network. Firstly, SDK detects IP range by using subnet mask, then pings those IP addresses one by one and detects devices that are alive. **IP Address, MAC address** and **device name** are returned to the developer.
``` java
NetworkTracer.getConnectedDevices(context, new TraceListener() {
	@Override
	public void onTraceResult(ArrayList<TraceEntity> entities) {
		// to do
	}

	@Override
	public void onPartialTraceResult(final TraceEntity entity) {
		// to do
	}
});
```
**TraceListener**: Informs the app when a networked device is discovered. Also triggers a method when all devices are detected and returns device info as array.

**onTraceResult(ArrayList<TraceEntity> entities)**: Triggered when all devices in the local network are detected. Contains an array list of device info.

**onPartialTraceResult(final TraceEntity entity)**: Triggered whenever a single device is detected. Contains device info (TraceEntity) that encapsulates **IP Address, MAC Address,** and **Device Name** fields.

Device name field may not be detected for all devices. This value is dependent on the device.

<div id='push-notification'><a name='Push Notification'></a></div>

# **6 Push Notification**

Veloxity Android SDK is capable of sending push notification messages to the client. **GCM (Google Cloud Messaging)** server is used to provide Push Notification support.

Follow these steps to make sure the P.N. feature will be up and running.

<div id='libraries'><a name='Libraries'></a></div>

## **6.1 Libraries**

Since gcm.jar is deprecated as of May 28, 2015, the new GCM library must be used to get push notifications from Google Cloud.  Google Play Services Library needs to be added as a dependency to the **build.gradle** file of your application **if you don’t use remote maven repository:**
``` javascript
dependencies {
	compile 'com.google.android.gms:play-services-gcm:11.0.4'
}
```

Gradle system automatically downloads and prepares the necessary files in order to get push notification messages properly. If you see any kind of conflicts or missing classes in your project, you should check whether this library is downloaded successfully or not.

Note: You can skip this part if you use remote maven repository in the **build.gradle**. Gradle automatically adds this dependency to your project.

<div id='manifest-file'><a name='Manifest File'></a></div>

## **6.2 Manifest File**

This addition is required for the functionality of the P.N. feature.

Android system has capability to show a notification message on the bar with the provided small and large icon. Large icon will be downloaded and shown automatically after getting push notification from the cloud. But there’s no option for the small icon.
<p align="center"><img id="manifestFile" src="/img/markdown/manifest_file.png" width="550"></p>

You must define a small icon or icons in **drawable** folder of the application:
```
drawable/ic_push_notification.png
drawable-hdpi/ic_push_notification.png
drawable-mdpi/ic_push_notification.png
drawable-xdpi/ic_push_notification.png
drawable-xxdpi/ic_push_notification.png
```
After adding **ic_push_notification.png** to the application, you’re going to see the small icon on each PN message sent by Veloxity Cloud:

<p align="center"><img id="pnMessageByVeloxity" src="/img/markdown/pn_message.png" width="550"></p>

<div id='register-for-push-notifications'><a name='Register for Push Notifications'></a></div>

## **6.3 Register for Push Notifications**

As a last step you must set GCM_SENDER_ID of your android project (can be taken by using Google APIs Console) to **VeloxityOptions** object in order to **initialize()** method of the SDK.

SDK will update your GCM token regularly in each start of your application.

<div id='lack-of-google-play-services'><a name='Lack of Google Play Services'></a></div>

## **6.4 Lack of Google Play Services**

Veloxity SDK automatically check the device if end-user has Google Play Services on its phone. If the device doesn’t have a compatible Google Play Services APK, Veloxity SDK will show an error on notification bar to inform and direct the user to Play Store to download the latest load. This feature is provided by Veloxity SDK. There’s nothing the developer needs to do in here.

<div id='if-application-has-already-gcm-service'><a name='If Application has already GCM Service'></a></div>

## **6.5 If Application has already GCM Service**

**Please ignore this section if you don’t have any other Google Cloud Messaging (Push Notification) service implemented in your application.**

If your application actively uses actively another GCM service, you don’t need to change anything. Veloxity SDK listens the broadcasts and filters out the related push notification messages. If it’s its own message, it’s going to abort the broadcast as well. Therefore, an application developer should only take care of his own implementation.

GCM allows to use different sender IDs for the same application:

https://developers.google.com/cloud-messaging/concept-options#receiving-messages-from-multi-senders

Same or different sender IDs can be used for the application and the SDK.

<div id='transaction-management'><a name='Transaction Management'></a></div>

# **7 Transaction Management**

The SDK enables the ability to execute transactions that are triggered by SDK events. When an SDK event is sent to the cloud, it may receive back a push notification. The received push notification may in return trigger a transaction executed by the hosting app. In order to return the result of a specific transaction, the method below should be called:
``` java
try {
	VeloxityTransaction.notify(this, TRANSACTION_ID, RESULT_TYPE);
}
catch (TransactionException e) {
	e.printStackTrace();
}
```
Every push notification passes along a unique ID that identifies the transaction and each ID generated should be matched with a transaction result. The above call should be made every single time a push notification is received by the hosting app, regardless of whether the transaction is executed or not.

**TRANSACTION_ID** is the string representation of executed transaction.

**RESULT_TYPE** is the enum type representation of transaction result. You should import “net.veloxity.sdk.ResultType” class to use these types: **ResultType.SUCCESS, ResultType.FAILURE**

<div id='using-veloxity-sdk-with-proguard'><a name='Using Veloxity SDK with Proguard'></a></div>

# **8 Using Veloxity SDK with Proguard**

ProGuard is a tool that shrinks, optimizes and obfuscates your code and is included when you create a new Android project in Android Studio. The Android Plugin for Gradle automatically appends ProGuard configuration files in AAR package and appends that package to your ProGuard configuration.

In order to use ProGuard with Veloxity SDK, you don’t need to do anything. The configuration will be merged during project build **automatically**.

If you have any trouble with automatic build, you can add these lines to your proguard configuration file manually:
```
-keep class net.veloxity.** { *; }
-keep interface net.veloxity.** { *; }
-dontwarn net.veloxity.**
```
<div id='privacy-policy'><a name='Privacy Policy'></a></div>

# **9 Privacy Policy**

You must describe how SDK collects and uses the personal information and the options available to the end user via **Privacy Policy**. In order to make it clear for the customers, please share a privacy policy link on the **PLAY STORE PAGE** and the **DATA USAGE DIALOG**.

<p align="center"><img id="privacyPolicy" src="/img/markdown/privacy_policy.png" width="550"></p>

You can prepare a new policy for your application based on the Veloxity Privacy Policy:
(http://www.veloxity.net/veloxity-sdk-privacy-policy/)

<div id='sample-application'><a name='Sample Application'></a></div>

# **10 Sample Application**

If you want to check the basic implementations of the SDK, you can investigate these repositories (and pull them out if you need) from Bitbucket:

**Android Sample Application:**

-**master branch (basic implementation with standart permission dialog):**

https://bitbucket.org/veloxity-inc/veloxity-sdk-android-sample/

-**withGCM branch (for the users who has already using a GCM service):**

https://bitbucket.org/veloxity-inc/veloxity-sdk-android-sample/branch/withGCM
