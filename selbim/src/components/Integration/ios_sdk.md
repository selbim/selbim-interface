<p align="left" style="font-size: 25px">
	<img src="/img/markdown/apple_logo.png" width="150"><br /><br />
	<strong>
		VELOXITY iOS SDK<br />
		 Version 2.8.0
	</strong>
</p>

## **Abstract**

This document outlines conceptual overviews, programming tasks, and integration guide with respect to Veloxity iOS SDK.

**EMAIL**: [developer@blinnk.com](mailto:developer@blinnk.com)

**DATE**: 09/11/2017

Distribution is subject to copyright laws.

## **Contents**

- 1 <a href="#introduction-to-veloxity-sdk">Introduction to Veloxity SDK</a>
- 2 <a href="#how-veloxity-ios-sdk-operate">How Veloxity iOS SDK Operate?</a>
- 3 <a href="#minimum-system-requirements">Minimum System Requirements</a>
- 4 <a href="#integration-steps">Integration Steps</a>
	- 4.1 <a href="#add-veloxity-sdk-to-your-project">Add Veloxity SDK to your project</a>
		 - 4.1.1 <a href="#add-sdk-as-framework">Add SDK as framework</a>
		 - 4.1.2 <a href="#install-sdk-via-cocoaPods">Install SDK via CocoaPods</a>
    - 4.2  <a href="#veloxity-sdk-configurations">Veloxity SDK Configurations</a>
    - 4.3  <a href="#import-header-file">Import Header file</a>
    - 4.4  <a href="#start-veloxity-service">Start Veloxity Service</a>
     	- 4.4.1 <a href="#with-data-usage-dialog">With Data Usage Dialog</a>
     	- 4.4.2 <a href="#without-data-usage-dialog">Without Data Usage Dialog</a>
    - 4.5 <a href="#opt-in-opt-out">Opt-in / Opt-out</a>
    - 4.6 <a href="#passing-user-identifier-(msisdn-email)">Passing User Identifier (MSISDN/EMAIL)</a>
    - 4.7 <a href="#sending-custom-data-to-cloud">Sending Custom Data to Cloud</a>
    - 4.8 <a href="#unique-device-id">Unique Device ID</a>
    - 4.9 <a href="#collect-session-analytics">Collect Session Analytics</a>
- 5 <a href="#push-notification">Push Notification</a>
	- 5.1 <a href="#register-to-apn-service">Register to APN Service</a>
	- 5.2 <a href="#pass-device-token-to-sdk">Pass Device Token to SDK</a>
	- 5.3 <a href="#pass-remote-notifications-to-sdk">Pass Remote Notifications to SDK</a>
- 6 <a href="#synthetic-transactions">Synthetic Transactions</a>
 	- 6.1 <a href="#overview">Overview</a>
 	- 6.2 <a href="#functions">Functions</a>
 		- 6.2.1 <a href="#initialization">Initialization</a>
 		- 6.2.2 <a href="#wi-fi-speed-test">Wi-Fi Speed Test</a>
 		- 6.2.3 <a href="#download-speed-test">Download Speed Test</a>
 		- 6.2.4 <a href="#upload-speed-test">Upload Speed Test</a>
 		- 6.2.5 <a href="#ping">Ping</a>
 		- 6.2.6 <a href="#delay-test">Delay Test</a>
 		- 6.2.7 <a href="#traceroute-test">Traceroute Test</a>
 		- 6.2.8 <a href="#network-trace-test">Network Trace Test</a>
 		- 6.2.9 <a href="#resource-usage-test">Resource Usage Test</a>
- 7 <a href="#sample-application">Sample Application</a>

## **Change Log**
| Version  |    Date   |             Exp.                      |
|:---------|:----------|:--------------------------------------|
|   2.6.0  | 06/22/2016| All sections are edited and formatted.|
|   2.6.3  | 08/31/2016| Updated 4.2                           |
|   2.6.3  | 09/27/2016| Updated 5.3                           |
|   2.7.0  | 10/18/2016| Updated 4.2,5.3                       |
|2.7.2-2.7.4 | 02/27/2017| Updated 4.8 and updated 5.3          |
|2.7.5-2.7.7 | 06/21/2017| Added 4.1.1.2                        |
|   2.8.0  | 09/11/2017| Added 4.9                             |

# **Veloxity iOS SDK**

<div id='introduction-to-veloxity-sdk'><a name='Introduction to Veloxity SDK'></a></div>

## **1 Introduction to Veloxity SDK**

Veloxity iOS SDK is used to collect network diagnostics data from mobile devices in which the SDK is attached to one of the installed apps. The collected data serves as a means to analyze the user’s Wi-Fi & cellular network connection details to identify issues affecting network speed. Data usage and location data are also collected for improved troubleshooting and reporting purposes. Collected data is stored in the Cloud environment which is specified by the application developer. All the features and variables can be manipulated from cloud online. It is optimized in all aspects that battery usage is less than 1%.

<p align="center"><img id="introductionScehma" src="/img/markdown/introduction_schema.png" width="550"></p>

<div id='how-veloxity-ios-sdk-operate'><a name='How Veloxity iOS Sdk Operate'></a></div>

## **2 How Veloxity iOS SDK operate?**

Veloxity library works as a background service. When **[[Veloxity sharedInstance] start]** method is called, the service starts in the background. Once registered as a background service Veloxity agent starts automatically on each device-restart.

If more than one registered Veloxity SDK through multiple apps are present in the device only one instance runs at a given time. If the running instance crashes or relevant app is uninstalled from the device a different instance shall start immediately.

The priority of the license key determines the selection of instance that shall kick in.

<div id='minimum-system-requirements'><a name='Minimum System Requirements'></a></div>

## **3 Minimum System Requirements**

Veloxity SDK supports and works on iOS 8.0 and above.

If the application is under IOS 8.0, you can use the static library option. Veloxity SDK will only initialize for the devices bigger than IOS 8.0 and will be passive for the rest of them.

<div id='integration-steps'><a name='Integration Steps'></a></div>

# **4	Integration Steps **

<div id='add-veloxity-sdk-to-your-project'><a name='Add Veloxity SDK to Your Project'></a></div>

## **4.1	Add Veloxity SDK to your project **

<div id='add-sdk-as-framework'><a name='Add SDK as framework'></a></div>

### **4.1.1 Add SDK as framework**

•	Get up-to-date version of framework from Veloxity Team. (Please contact us from [**developer@blinnk.com**](mailto:developer@blinnk.com) for getting **VeloxitySDK.framework**)
•	Add these frameworks into the project (your linker settings):
```
SystemConfiguration.framework
CFNetwork.framework
UIKit.framework
Foundation.framework
CoreTelephony.framework
CoreLocation.framework
Security.framework
```
•	Add these libraries into the project (your linker settings):
```
libsqlite3.dylib
libz.dylib
```
<div id='minimum-ios-version-under-8.0'><a name='Minimum IOS version under 8.0'></a></div>

#### **4.1.1.1	Minimum IOS version under 8.0**

If the project is set to a value under IOS 8.0 (such as 6.0, 7.0), App Store will reject the application with dynamic framework since it’s introduced in IOS 8.0. There’s an App Store policy to control the wrong usage.

After adding the necessary frameworks & libraries (see 4.1.1) to the linker settings, you should add the static framework file instead of the embedded(dynamic) framework.

**DON’T ADD THE STATIC LIBRARY TO EMBEDDED FRAMEWORK SECTION. IT WILL REJECT YOU WHILE UPLOADING APPLICATION TO THE APP STORE.**

(See the picture below for better understanding)

<p align="center"><img id="buildPhases" src="/img/markdown/build_phases1.png" width="850"></p>

On the other hand, you have to write **–ObjC** to the other linker flags in order to prevent run-time crashes.

<p align="center"><img id="buildSettings" src="/img/markdown/build_settings.png" width="850"></p>

<div id='minimum-ios-version-8.0-and-above'><a name='Minimum IOS version 8.0 and above'></a></div>

#### **4.1.1.2	Minimum IOS version 8.0 and above**

If the minimum IOS version of the project above 8.0, you are free to use dynamic framework including the resource files. You need to add **VeloxitySDK.framework** to the embedded framework section of General and Build Phases (Normally they sync each other)

<p align="center"><img id="buildPhases" src="/img/markdown/build_phases2.png" width="850"></p>

<p align="center"><img id="buildPhases" src="/img/markdown/build_phases3.png" width="850"></p>

<div id='install-sdk-via-cocoaPods'><a name='Install SDK via CocoaPods'></a></div>

### **4.1.2	Install SDK via CocoaPods**

•	**Download CocoaPods**

CocoaPods is a dependency manager for Objective-C, which automates and simplifies the process of using 3rd-party libraries in your projects.

CocoaPods is distributed as a ruby gem, and is installed by running the following commands in Terminal.app:
``` shell
$ sudo gem install cocoapods
$ pod setup
```
DEPENDING ON YOUR RUBY INSTALLATION, YOU MAY NOT HAVE TO RUN AS SUDO TO INSTALL THE COCOAPODS GEM.

•	**Create a pod file**

Project dependencies to be managed by CocoaPods are specified in a file called **Podfile**. Create this file in the same directory as your Xcode project (**.xcodeproj**) file:
``` shell
$ touch Podfile
$ vim Podfile
```
Copy and paste the following lines into the target/end section of your Podfile:
``` shell
pod 'VeloxitySDK', :git => 'https://bitbucket.org/vlx_cloud_agent/veloxity-ios-cocoapods.git'
```
YOU SHOULDN’T USE TEXTEDIT TO EDIT THE POD FILE BECAUSE TEXTEDIT LIKES TO REPLACE STANDARD QUOTES WITH MORE GRAPHICALLY APPEALING QUOTES. THIS CAN CAUSE COCOAPODS TO GET CONFUSED AND DISPLAY ERRORS, SO IT’S BEST TO JUST USE VIM OR ANOTHER PROGRAMMING TEXT EDITOR.

•	**Install dependencies**

Now you can install the dependencies in your project:
``` shell
$ pod install
```
From now on, be sure to always open the generated Xcode workspace (**.xcworkspace**) instead of the project file when building your project:
``` shell
$ open <YourProjectName>.xcworkspace
```
<div id='veloxity-sdk-configurations'><a name='Veloxity SDK Configurations'></a></div>

## **4.2	Veloxity SDK Configurations**

•	**Enable Automatic Reference Counting**
Arc must be enabled for initialization of the SDK.

•	**Enable the Capabilities of the Application**

Push Notifications and Remote Notifications must be enabled for actively working SDK. Also you must add a keychain group as **net.veloxity.PERMISSION_ID** to the Keychain Sharing section. (Please contact us at, [**developer@blinnk.com**](mailto:developer@blinnk.com) for getting the PERMISSION_ID value) (see the picture below)

<p align="center"><img id="capabilities" src="/img/markdown/capabilities.png" width="850"></p>

•	**Add mandatory location update string to the plist of your application**
 (**NSLocationAlwaysUsageDescription**, see the picture below)

<p align="center"><img id="plist" src="/img/markdown/plist.png" width="850"></p>

<div id='import-header-file'><a name='Import Header file'></a></div>

## **4.3	Import Header file**

In your **[app_name]-Prefix.pch** project file (generally found in the Supporting Filesfolder), include the VeloxitySDK header inside the **#ifdef __OBJC__ #endif** block:
``` Objective-C
#import <VeloxitySDK/VeloxitySDK.h>
```
<div id='start-veloxity-service'><a name='Start Veloxity Service'></a></div>

## **4.4	Start Veloxity Service**

In order to initialize and start the Veloxity agent, the **[[Veloxity sharedInstance] start]** function needs to be called within the **application:didFinishLaunchingWithOptions** method of your **AppDelegate.m** file.

LICENSE_KEY and WEB_SERVICE_URL parameters should be given to you. (Please contact us at, [**developer@blinnk.com**](mailto:developer@blinnk.com) for getting this information)

LICENSE_KEY separates the application from the others. Veloxity will provide different LICENSE_KEY(s) in order to use your applications. LICENSE_KEY is generated only for one application and directly dependent to the package name of the application.

First of all, in your **AppDelegate.m** file, start Veloxity SDK depending on the regulation of your country:

<div id='with-data-usage-dialog'><a name='With Data Usage Dialog'></a></div>

### **4.4.1	With Data Usage Dialog**

In your **AppDelegate.m** file, add this call as first line of **application:didFinishLaunchingWithOptions:**
``` Objective-C
[[Veloxity sharedInstance] setLicenseKey:@"LICENSE_KEY"];
[[Veloxity sharedInstance] setWebServiceUrl:@"WEB_SERVICE_URL"];
[[Veloxity sharedInstance]
setAuthorizationMenu:NSLocalizedString(@"AUTH_TITLE", nil)
withMessage:NSLocalizedString(@"AUTH_MSG", nil)
andAcceptTitle:NSLocalizedString(@"AUTH_OK", nil)
andDenyTitle:NSLocalizedString(@"AUTH_CANCEL", nil)];

[[Veloxity sharedInstance] setDelegate:self];
[[Veloxity sharedInstance] start];
```
AUTH_TITLE, AUTH_MSG, AUTH_OK and AUTH_CANCEL parameters and delegate method make you control the authorization page and get the callbacks accordingly.

<p align="center"><img id="messagePermisson" src="/img/markdown/message_permission.png" width="850"></p>

Data Usage Page is automatically shown on the screen before initializing the SDK. Based on the end user response, SDK decides what to do next. If user agrees the terms and conditions, it begins to collect data and give a callback to the application developer via **VeloxityDelegate** method. To reach the delegate methods, you should **VeloxityDelegate** protocol in your **AppDelegate.m** file:
``` Objective-C
@interface AppDelegate ()<VeloxityDelegate>
```
Then use these delegates depending on your application workflow:
``` Objective-C
#pragma mark Veloxity Delegate Methods
-(void) vlxAuthorizationDidSucceed {	// TODO	}
-(void) vlxAuthorizationDidFailed {	// TODO	}
```
You can change the Title and the Message content of the Data Usage Dialog by AUTH_TITLE and AUTH_MSG parameters.
Optionally, you can use the AUTH_OK and AUTH_CANCEL strings for changing the content of positive and negative buttons on the Data Usage dialog.

<div id='without-data-usage-dialog'><a name='Without Data Usage Dialog'></a></div>

### **4.4.2	Without Data Usage Dialog**

If the regulation of your country doesn’t push you to show a warning to the end user, you can remove the dialog related stuffs from the start operation. Hence service will start instantly after you call the initialize method of the SDK.
``` Objective-C
[[Veloxity sharedInstance] setLicenseKey:@"LICENSE_KEY"];
[[Veloxity sharedInstance] setWebServiceUrl:@"WEB_SERVICE_URL"];
[[Veloxity sharedInstance] start];
```
iOS will ask permissions for Notification and Location as usual but not data usage dialog.

<div id='opt-in-opt-out'><a name='Opt-in / Opt-out'></a></div>

## **4.5	Opt-in / Opt-out**

Veloxity SDK allows you to opt-in/opt-out the data collection service.
You can disable the service by calling this method in anywhere:
``` Objective-C
[[Veloxity sharedInstance] optOut];
```
After calling the method, Veloxity Service will be completely stopped and terminate the data collection session.
On the other hand, you are able to re-enable the service by calling the initialization method of SDK similarly:
``` Objective-C
[[Veloxity sharedInstance] setLicenseKey:@"LICENSE_KEY"];
[[Veloxity sharedInstance] setWebServiceUrl:@"WEB_SERVICE_URL"];
[[Veloxity sharedInstance]
setAuthorizationMenu:NSLocalizedString(@"AUTH_TITLE",  nil)
withMessage:NSLocalizedString(@"AUTH_MSG", nil)
andAcceptTitle:NSLocalizedString(@"AUTH_OK", nil)
andDenyTitle:NSLocalizedString(@"AUTH_CANCEL", nil)];
[[Veloxity sharedInstance] setDelegate:self];
[[Veloxity sharedInstance] optIn];
```
(If you don't want to show the data usage dialog, remove the given part below.)
``` Objective-C
[[Veloxity sharedInstance]
setAuthorizationMenu:NSLocalizedString(@"AUTH_TITLE", nil)
withMessage:NSLocalizedString(@"AUTH_MSG", nil)
andAcceptTitle:NSLocalizedString(@"AUTH_OK", nil)
andDenyTitle:NSLocalizedString(@"AUTH_CANCEL", nil)];
[[Veloxity sharedInstance] setDelegate:self];
```
If you want to learn the status of Veloxity Service, if it is really running or not, you should call the **serviceStatus** method:
``` Objective-C
[[Veloxity sharedInstance] serviceStatus];
```
It returns BOOL types (YES,NO).

<div id='passing-user-identifier-(msisdn-email)'><a name='Opt-in / Opt-out'></a></div>

## **4.6	Passing User Identifier (MSISDN/EMAIL)**

Veloxity iOS SDK provides a public method for passing msisdn or email information of the end user.  After SDK activation, application can pass the (NSString) identifier to the SDK via this method:
``` Objective-C
[[Veloxity sharedInstance] setUserIdentifier:MSISDN_TEXT];
```
<div id='sending-custom-data-to-cloud'><a name='Sending Custom Data to Cloud'></a></div>

## **4.7	Sending Custom Data to Cloud**

Veloxity iOS SDK provides a public method for sending specific data to the cloud.  After SDK activation, application can send the (NSDictionary) JSON content to the SDK via this method:
``` Objective-C
[[Veloxity sharedInstance] sendCustomData:JSON_CONTENT];
```
<div id='unique-device-id'><a name='Unique Device ID'></a></div>

## **4.8	Unique Device ID**

Veloxity iOS SDK provides a public method for getting a unique device ID. You can call this method directly from the **Veloxity.h** file:
``` Objective-C
[[Veloxity sharedInstance] getDeviceId];
```

<div id='collect-session-analytics'><a name='Collect Session Analytics'></a></div>

## **4.9	Collect Session Analytics**

Veloxity iOS SDK automatically binds the lifecycle of the application, listens enter/exit conditions and collects session duration information as well.

<div id='push-notification'><a name='Push Notification'></a></div>

# **5	Push Notification**

Veloxity iOS SDK is capable of sending push notification messages to the client. **APN (Apple Push Notification Service)** server is used to provide Push Notification support.

Follow these steps to make sure the P.N. feature will be up and running.

<div id='register-to-apn-service'><a name='Register to APN Service'></a></div>

## **5.1	Register to APN Service**

In the AppDelegate.m file, you need to register to the APN service by using this code block:
``` Objective-C
UIUserNotificationSettings *settings = [UIUserNotificationSettings
settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |
UIUserNotificationTypeSound) categories:nil];

[[UIApplication sharedApplication] registerUserNotificationSettings:settings];
```
Then add delegate to complete the registration:
``` Objective-C
-(void)application:(UIApplication*)application
didRegisterUserNotificationSettings:(UIUserNotificationSettings*)notificationSettings {
	[application registerForRemoteNotifications];
}
```
<div id='pass-device-token-to-sdk'><a name='Pass Device Token to SDK'></a></div>

## **5.2	Pass Device Token to SDK**

After getting Device Token from the **didRegisterForRemoteNotificationsWithDeviceToken**, you need to send this token to the SDK:
``` Objective-C
[[Veloxity sharedInstance] registerDeviceToken:deviceToken];
```
<div id='pass-remote-notifications-to-sdk'><a name='Pass Remote Notifications to SDK'></a></div>

## **5.3	Pass Remote Notifications to SDK**

Application should pass the incoming push notification message to the Veloxity SDK for the receiver methods:
``` Objective-C
didReceiveRemoteNotification:(NSDictionary*)userInfo
didReceiveRemoteNotification:(NSDictionary*)userInfo fetchCompletionHandler:(nonnull
void(^)(UIBackgroundFetchResult))completionHandler
```
If application or other SDKs use its own PN system, it should handle the incoming messages and only direct Veloxity related push notifications to the SDK:
``` Objective-C
if ([userInfo objectForKey:@"vlx"]){
	[[Veloxity sharedInstance] startBackgroundTransactionWithUserInfo:userInfo];
completionHandler(UIBackgroundFetchResultNewData); // completionHandler
	return;
}
```
Please ensure that **application:didFinishLaunchingWithOptions:(NSDictionary*)launchOptions** method doesn’t intercept the incoming message coming from the push notification.

<div id='synthetic-transactions'><a name='Synthetic Transactions'></a></div>

# **6	Synthetic Transactions**

<div id='overview'><a name='Overview'></a></div>

## **6.1	Overview**

Synthetic Transaction Execution is a feature that carries out some network related evaluation operations automatically. Before Veloxity’s Synthetic Transaction feature, these kinds of evaluations would be run by the user manually several times.

The synthetic transaction functions available through Veloxity iOS SDK are listed below.

<div id='functions'><a name='Functions'></a></div>

## **6.2	Functions**

All synthetic transaction methods are bundled in **vlxPublic** class which can be reached via **VeloxitySDK.h** header.

<div id='initialization'><a name='Initialization'></a></div>

### **6.2.1	Initialization**

Before using any methods, **VeloxitySDK.h** header file should be imported and the class which will be using public functions should be set as the delegate of **vlxPublic** class:
``` Objective-C
[[vlxPublic sharedInstance] setDelegate:self];
```
and based on your needs, define the **vlxPublicDelegate** protocol.
``` Objective-C
@interface CLASSNAME() <vlxPublicDelegate>
```
<div id='wi-fi-speed-test'><a name='Wi-Fi Speed Test'></a></div>

### **6.2.2	Wi-Fi Speed Test**

Used to evaluate the LAN speed by using UDP datagram packages:
``` Objective-C
[[vlxPublic sharedInstance] startWiFiSpeedTestWithCount:<sampleCount>];
```

**``<sampleCount>``** number of samples

if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishWiFiSpeedTestWithSampleArray:(NSMutableDictionary*)_sampleDict
andIsSuccess:(BOOL)isSuccess withMessage:(NSString *)_message
```
-	**_sampleDict**: contains test samples in bytes
-	**isSuccess** : indicates the success status of the test
-	**_message** : status message if the test is not successful

<div id='download-speed-test'><a name='Download Speed Test'></a></div>

### **6.2.3	Download Speed Test**

Used to evaluate the user’s download speed from a given www-host address.
``` Objective-C
[[vlxPublic sharedInstance] startWithDownloadURL:<downloadURL>
andSampleCount:<sampleCount> andThreadCount:<threadCount>];
```
-	**``<downloadURL>``** : download URL
-	**``<sampleCount>``** : number of samples
-	**``<threadCount>``** : number of download threads to be used when testing

if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishDownloadSpeedTest:(NSMutableDictionary *)_sampleDict
andIsSuccess:(BOOL)isSuccess withMessage:(NSString *)_message
```
-	**_sampleDict** : contains test samples in bytes
-	**isSuccess** : indicates the success status of the test
-	**_message** : status message if the test is not successful

<div id='upload-speed-test'><a name='Upload Speed Test'></a></div>

### **6.2.4	Upload Speed Test**

Used to evaluate the user’s data upload speed by uploading some amount of sample data to a defined end point.
``` Objective-C
[[vlxPublic sharedInstance] startWithUploadURL:<uploadURL>
andSampleCount:<sampleCount> andThreadCount:<threadCount>];
```
-	**``<uploadURL>``**: upload URL
-	**``<sampleCount>``** : number of samples
-	**``<threadCount>``** : number of download threads to be used when testing

if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishUploadSpeedTest:(NSMutableDictionary *)_sampleDict
andIsSuccess:(BOOL)isSuccess withMessage:(NSString *)_message
```
-	**_sampleDict** : contains test samples in bytes
-	**isSuccess** : indicates the success status of the test
-	**_message** : status message if the test is not successful

<div id='ping'><a name='Ping'></a></div>

### **6.2.5	Ping**

Used to carry out ping test to a host address defined with the “address” parameter.
``` Objective-C
[[vlxPublic sharedInstance] startPingTestToAddress:<address>
withCount:<sampleCount>];
```
-	**``<address>``** : address to ping. You should send “gateway” to ping gateway and “isp” to ping isp
-	**``<sampleCount>``** : number of samples

if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishPingTest:(NSMutableDictionary *)_sampleDict
andIsSuccess:(BOOL)isSuccess withMessage:(NSString *)_message
```
-	**_sampleDict** : contains test samples in milliseconds
-	**isSuccess** : indicates the success status of the test
-	**_message** : status message if the test is not successful

<div id='delay-test'><a name='Delay Test'></a></div>

### **6.2.6	Delay Test**

Used to evaluate the user’s delay to a given host entry by each endpoint (gateway, ISP and host). The function finds the gateway IP address and ISP address internally.
``` Objective-C
[[vlxPublic sharedInstance] startDelayTestWithContentAddress:<contentAddress>
andCount:<sampleCount>];
```
-	**``<contentAddress>``** : content address to ping
-	**``<sampleCount>``** : number of samples

if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishDelayTest:(NSMutableDictionary *)_sampleDict
andIsSuccess:(BOOL)isSuccess withMessage:(NSString *)_message
```
-	**_sampleDict** : contains test samples in milliseconds
-	**isSuccess** : indicates the success status of the test
-	**_message** : status message if the test is not successful

<div id='traceroute-test'><a name='Traceroute Test'></a></div>

### **6.2.7	Traceroute Test**

Used to make a trace route to a given address.
``` Objective-C
[[vlxPublic sharedInstance] startTraceRouteToAddress:<address>];
```
-	**``<address>``** : address to traceroute

if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishTraceRoute:(NSMutableDictionary *)_dict
andIsSuccess:(BOOL)isSuccess withMessage:(NSString *)_message
```
-	**_dict**: contains test samples
-	**isSuccess** : indicates the success status of the test
-	**_message** : status message if the test is not successful

<div id='network-trace-test'><a name='Delay Test'></a></div>

### **6.2.8	Network Trace Test**

Used to find all alive IP’s on the LAN.
``` Objective-C
[[vlxPublic sharedInstance] startNetworkTraceTest];
```
if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishNetworkTrace:(NSMutableDictionary*)_sampleDict
```
**_sampleDict** : contains all the alive IPs.

<div id='resource-usage-test'><a name='Resource Usage Test'></a></div>

### **6.2.9	Resource Usage Test**

Used to get resource usage of the device.
``` Objective-C
[[vlxPublic sharedInstance] startGettingResourceUsage];
```
if delegate is set successfully, delegate method will be called after the test is done:
``` Objective-C
-(void)didFinishGettingResourceUsage:(NSMutableDictionary*)_sampleDict
```
**_sampleDict**:  contains the cpu and memory usage of the device

<div id='sample-application'><a name='Sample Application'></a></div>

# **7 Sample Application**

If you want to check the basic implementations of the SDK, you can investigate these repositories (and pull them out if you need) from Bitbucket:

**iOS Sample Application:**

-**master branch (basic implementation with standart permission dialog for 8.0+ projects):**

https://bitbucket.org/veloxity-inc/veloxity-sdk-ios-sample/

-**StaticFramework branch (for the projects which compiles under 8.0):**

https://bitbucket.org/veloxity-inc/veloxity-sdk-ios-sample/branch/StaticFramework
