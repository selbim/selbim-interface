> Pick one of the options below to download Veloxity Android SDK.

#### 1. Remote Maven Repository

* Define remote Maven URL to the build.gradle:

```javascript
repositories {
    maven{
        url 'https://maven.veloxity.net/nexus/content/repositories/releases/'
    }
}
```

* Add the library as dependency to the build.gradle file of your application:

```javascript
dependencies {
    compile 'net.veloxity.sdk:veloxitysdk:2.8.0'
}
```

<hr />

#### 2. Local AAR file

* [Download veloxitysdk-2.8.0.aar](https://maven.veloxity.net/nexus/content/repositories/releases/net/veloxity/sdk/veloxitysdk/2.8.0/veloxitysdk-2.8.0.aar "Download Veloxity Aar") to the libs folder of your project.
* Define local libs folder path to the <strong>build.gradle</strong>:

```javascript
repositories {
  flatDir {
        dirs 'libs'
  }
}
```
* Add these libraries as dependency to the <strong>build.gradle</strong> file of your application:

```javascript
dependencies {
    compile 'com.google.android.gms:play-services-gcm:11.0.4'
    compile 'com.google.android.gms:play-services-location:11.0.4'
    compile 'com.google.android.gms:play-services-awareness:11.0.4'
    compile(name: 'veloxitysdk-2.8.0', ext: 'aar')
}
```
Note: There’s no need to add anything to the AndroidManifest.xml file. Permissions and definitions will be merged after adding SDK AAR file.

> For more detailed information visit [documentation](./documentation) page.
