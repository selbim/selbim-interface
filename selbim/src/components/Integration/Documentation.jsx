import React from 'react';
import marked from 'marked';
import Row from 'react-bootstrap/lib/Row';
import { injectIntl } from 'react-intl';
import androidDoc from './android_sdk.md';
import iosDoc from './ios_sdk.md';
import ContentHeader from '../Layout/ContentHeader';
import ContentWrapper from '../Layout/ContentWrapper';

class Documentation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { markdown: '' };
    marked.setOptions({
      renderer: new marked.Renderer(),
      gfm: true,
      tables: true,
      breaks: false,
      pedantic: false,
      sanitize: false,
      smartLists: true,
      smartypants: false,
    });
    this.selectediOS = this.selectediOS.bind(this);
    this.selectedAndroid = this.selectedAndroid.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentWillMount() {
    fetch(androidDoc).then(response => response.text()).then((text) => {
      this.setState({
        markdown: marked(text),
      });
    });
  }

  selectediOS() {
    fetch(iosDoc).then(response => response.text()).then((text) => {
      this.setState({
        markdown: marked(text),
      });
    });
  }

  selectedAndroid() {
    fetch(androidDoc).then(response => response.text()).then((text) => {
      this.setState({
        markdown: marked(text),
      });
    });
  }

  handleSelect(eventKey) {
    switch (eventKey) {
      case 'IOS':
        this.selectediOS();
        break;

      case 'ANDROID':
        this.selectedAndroid();
        break;
      default:
    }
  }

  render() {
    /*eslint-disable*/
    const { markdown } = this.state;

    return (
      <ContentWrapper>
        <ContentHeader title={this.props.intl.formatMessage({ id: 'sidebar.items.documentation' })} />
        <Row>
          some docs here
        </Row>
      </ContentWrapper>
    );
    /*eslint-enable */
  }
}

Documentation.defaultProps = {
  properties: [],
};

Documentation.propTypes = {
  // properties: PropTypes.object,
};

export default injectIntl(Documentation);
