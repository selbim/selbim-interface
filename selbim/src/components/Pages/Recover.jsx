import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import SweetAlert from 'react-bootstrap-sweetalert';
import RecoverForm from '../Forms/RecoverForm';
import ChangePasswordForm from '../Forms/ChangePasswordForm';
import { resetPassword, invalidateResetPassword, resetPasswordComplete } from '../../actions/authActions';

class Recover extends Component {
  constructor(props) {
    super(props);
    this.state = { alert: null, isErrorShowed: null };
    this.handleRecoverSubmit = this.handleRecoverSubmit.bind(this);
    this.handleCompleteSubmit = this.handleCompleteSubmit.bind(this);
  }

  componentWillMount() {
    this.props.invalidateResetPassword();
  }

  handleRecoverSubmit(value) {
    this.props.resetPassword(value);
  }

  handleCompleteSubmit(value) {
    let token = queryString.parse(this.props.location.search);
    token = queryString.stringify(token).split('token=').pop();
    this.props.resetPasswordComplete(value.newPassword, token);

    if (this.props.resetPasswordCompleteError) {
      this.setState({ isErrorShowed: null });
    }
  }
  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger mt-lg">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
    return null;
  }

  render() {
    return (
      <div className="mt-xl wd-xl">
        <div className="panel panel-dark panel-flat">
          <div className="panel-heading text-center">
            <span className="fa fa-envelope fa-5x text-muted" />
            <h3 className="text-center text-nowrap text-bold">Password Reset</h3>
          </div>
          <div className="panel-body">
            {this.props.location.search.includes('?token')
              ? <ChangePasswordForm onSubmit={this.handleCompleteSubmit} />
              : <div>
                <RecoverForm onSubmit={this.handleRecoverSubmit} />{this.renderAlert()}
                <br />
                <ul className="list-inline text-center text-sm mb-xl">
                  <li><Link to="login" className="text-muted">Login</Link></li>
                  <li className="text-muted">|</li>
                  <li><Link to="register" className="text-muted">Register</Link></li>
                </ul>
              </div>}
          </div>
        </div>
        {this.props.resetPasswordSuccess
          ? <SweetAlert
            success
            title="New password is sent!"
            confirmBtnText="Go to Login page."
            onConfirm={() => {
              this.props.history.push({
                pathname: '/login',
              });
            }}>
              Please check your inbox. If you did not receive this email, check your junk/spam folder.<br />
          </SweetAlert>
          : ''}
        {this.props.resetPasswordCompleteSuccess
          ? <SweetAlert
            success
            title="Password is changed!"
            confirmBtnText="Go to Login page."
            onConfirm={() => {
              this.setState({ alert: null });
              this.props.history.push({
                pathname: '/login',
              });
            }} />
          : ''}
        {this.props.resetPasswordCompleteError && this.state.isErrorShowed === null
          ? <SweetAlert
            danger
            title="Error Occured"
            onConfirm={() => {
              this.setState({ isErrorShowed: 'showed' });
            }}>
            {this.props.resetPasswordCompleteErrorMessage}
          </SweetAlert>
          : ''}
        {this.state.alert}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    resetPasswordSuccess: state.auth.resetPasswordSuccess,
    resetPasswordCompleteSuccess: state.auth.resetPasswordCompleteSuccess,
    resetPasswordCompleteError: state.auth.resetPasswordCompleteError,
    resetPasswordCompleteErrorMessage: state.auth.resetPasswordCompleteErrorMessage,
    errorMessage: state.auth.resetPasswordErrorMessage,
  };
}

Recover.defaultProps = {
  errorMessage: '',
  resetPasswordSuccess: false,
  resetPasswordCompleteSuccess: false,
  resetPasswordCompleteError: false,
  resetPasswordCompleteErrorMessage: '',
};

Recover.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  resetPasswordSuccess: PropTypes.bool,
  resetPasswordComplete: PropTypes.func.isRequired,
  resetPasswordCompleteSuccess: PropTypes.bool,
  resetPasswordCompleteError: PropTypes.bool,
  resetPasswordCompleteErrorMessage: PropTypes.string,
  history: PropTypes.object.isRequired,
  errorMessage: PropTypes.string.isRequired,
  invalidateResetPassword: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, { resetPassword, invalidateResetPassword, resetPasswordComplete })(Recover);
