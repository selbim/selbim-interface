import React from 'react';
import marked from 'marked';
import ContentWrapper from '../Layout/ContentWrapper';
import ContentHeader from '../Layout/ContentHeader';
import privacyDoc from './privacy.md';

class Privacy extends React.Component {
  constructor(props) {
    super(props);
    this.state = { markdown: '' };
    marked.setOptions({
      renderer: new marked.Renderer(),
      gfm: true,
      tables: true,
      breaks: false,
      pedantic: false,
      sanitize: false,
      smartLists: true,
      smartypants: false,
    });
  }
  componentWillMount() {
    fetch(privacyDoc).then(response => response.text()).then((text) => {
      this.setState({
        markdown: marked(text),
      });
    });
  }

  render() {
    const { markdown } = this.state;
    /*eslint-disable*/
    return (
      <ContentWrapper>
        <ContentHeader title="Privacy Policy" />
        <div className="panel b">
          <div className="panel-body">
            <article dangerouslySetInnerHTML={{ __html: markdown }} />
          </div>
        </div>
      </ContentWrapper>
    );
    /*eslint-enable */
  }
}

export default Privacy;
