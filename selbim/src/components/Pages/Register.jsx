import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { change, untouch } from 'redux-form/lib/actions';
import isSubmitting from 'redux-form/lib/isSubmitting';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import marked from 'marked';
import SweetAlert from 'react-bootstrap-sweetalert';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import { signupUser, invalidateError, clearAuthSuccessState } from '../../actions/authActions';
import RegisterForm from './RegisterForm';
import privacyDoc from './privacy.md';
import termsDoc from './terms.md';

class Register extends Component {
  constructor(props) {
    super(props);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.state = {
      alert: null,
      modal: null,
      termsAccepted: false,
      privacy: '',
      terms: '',
      showPrivacyModal: false,
      showTermsModal: false,
    };
    marked.setOptions({
      renderer: new marked.Renderer(),
      gfm: true,
      tables: true,
      breaks: false,
      pedantic: false,
      sanitize: false,
      smartLists: true,
      smartypants: false,
    });
  }

  componentWillMount() {
    this.props.clearAuthSuccessState();
    fetch(privacyDoc)
      .then(response => response.text())
      .then((text) => {
        this.setState({
          privacy: marked(text),
        });
      });
    fetch(termsDoc)
      .then(response => response.text())
      .then((text) => {
        this.setState({
          terms: marked(text),
        });
      });
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.submitting && nextProps.errorMessage) {
      this.props.change('register', 'password', '');
      this.props.change('register', 'passwordConfirm', '');
      this.props.untouch('register', 'password', 'passwordConfirm');
    }
    return true;
  }

  componentWillUnmount() {
    this.props.invalidateError();
  }

  handleFormSubmit(formProps) {
    this.props.signupUser(formProps);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
    return null;
  }
  render() {
    /*eslint-disable*/
    return (
      <div className="mt-xl wd-xl">
        <div className="panel panel-dark panel-flat">
          <div className="panel-heading text-center">
            <img className="block-center thumb80" src="img/blinnk_basic_256.png" alt="logo" />
            <h3 className="text-center text-nowrap text-bold">Welcome to Blinnk</h3>
            <p className="text-center  text-muted">
              <strong>REGISTER TO GET INSTANT ACCESS.</strong>
            </p>
          </div>
          <div className="panel-body">
            <RegisterForm
              onSubmit={this.handleFormSubmit}
              termsAccepted={this.state.termsAccepted}
              terms={
                <div className="clearfix">
                  <div className="checkbox c-checkbox pull-left mt0">
                    <small className="text-muted">
                      <label>
                        <input
                          type="checkbox"
                          checked={this.state.termsAccepted}
                          onChange={event => {
                            this.setState({ termsAccepted: event.target.checked });
                          }}
                        />
                        <em className="fa fa-check" />
                        I have read and agree to the{' '}
                        <a onClick={() => this.setState({ showPrivacyModal: true })}>Privacy Policy</a>
                        <Modal
                          show={this.state.showPrivacyModal}
                          onHide={() => this.setState({ showPrivacyModal: false })}
                          bsSize="large"
                          aria-labelledby="contained-modal-title-lg"
                        >
                          <Modal.Header closeButton>
                            <Modal.Title>Privacy Policy</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <article dangerouslySetInnerHTML={{ __html: this.state.privacy }} />
                          </Modal.Body>
                          <Modal.Footer>
                            <Button onClick={() => this.setState({ showPrivacyModal: false })}>Close</Button>
                          </Modal.Footer>
                        </Modal>{' '}
                        and <a onClick={() => this.setState({ showTermsModal: true })}>Terms of Conditions</a>
                        <Modal
                          show={this.state.showTermsModal}
                          onHide={() => this.setState({ showTermsModal: false })}
                          bsSize="large"
                          aria-labelledby="contained-modal-title-lg"
                        >
                          <Modal.Header closeButton>
                            <Modal.Title>Terms of Conditions</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <article dangerouslySetInnerHTML={{ __html: this.state.terms }} />
                          </Modal.Body>
                          <Modal.Footer>
                            <Button onClick={() => this.setState({ showTermsModal: false })}>Close</Button>
                          </Modal.Footer>
                        </Modal>
                      </label>
                    </small>
                  </div>
                </div>
              }
            />
            {this.renderAlert()}
            <p className="pt-lg text-center">Have an account?</p>
            {this.props.submitting ? (
              <Link to="/login" className="btn btn-block btn-default disabled">
                Log In
              </Link>
            ) : (
              <Link to="/login" className="btn btn-block btn-default">
                Log In
              </Link>
            )}
          </div>
        </div>
        {this.props.requestSuccess ? (
          <SweetAlert
            success
            title="Acccount Created!"
            confirmBtnText="Go to Login page."
            onConfirm={() => {
              this.props.history.push({
                pathname: '/login',
              });
            }}
          >
            Before you can login, you must <strong>activate</strong> your account from your email address. If you did
            not receive this email, please check your junk/spam folder.
          </SweetAlert>
        ) : (
          ''
        )}
        {this.state.modal}
      </div>
    );
    /*eslint-enable*/
  }
}

Register.defaultProps = {
  requestSuccess: false,
};

Register.propTypes = {
  signupUser: PropTypes.func.isRequired,
  invalidateError: PropTypes.func.isRequired,
  errorMessage: PropTypes.string.isRequired,
  submitting: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  untouch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  requestSuccess: PropTypes.bool,
  clearAuthSuccessState: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    requestSuccess: state.auth.requestSuccess,
    errorMessage: state.auth.error,
    submitting: isSubmitting('register')(state),
  };
}

export default connect(mapStateToProps, { signupUser, invalidateError, change, untouch, clearAuthSuccessState })(
  Register,
);
