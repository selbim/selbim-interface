import React, { Component } from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import { activateUser, resendActivation } from '../../actions/authActions';
import ResendForm from '../Forms/ResendForm';

const SuccesfulActivation = (
  <div className="text-center m-lg">
    <div className="panel-heading text-center">
      <span className="fa fa-envelope fa-5x text-muted" />
    </div>
    <h3 className="text-center text-nowrap text-bold">Account is activated.</h3>
    <p>You will be directed to login page.</p>
  </div>
);

const UnsuccefulActivation = ({ handleSubmit }) =>
  (<div className="text-center m-lg">
    <div className="panel-heading text-center">
      <span className="fa fa-envelope fa-5x text-muted" />
    </div>
    <h3 className="text-center text-nowrap text-bold">Account is not activated.</h3>
    <p>Use the box below to resend your activation.</p>
    <ResendForm onSubmit={handleSubmit} buttonColor="bg-danger" />
  </div>);

UnsuccefulActivation.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

class VerifyToken extends Component {
  constructor(props) {
    super(props);
    this.handleResend = this.handleResend.bind(this);
    this.state = { alert: null };
  }

  componentWillMount() {
    this.props.activateUser(queryString.parse(this.props.location.search).check);
  }

  handleResend(formProps) {
    this.props.resendActivation(formProps.email);
    this.setState({
      alert: (
        <SweetAlert
          success
          title="Activation mail is sent!"
          confirmBtnText="Go to Login page."
          onConfirm={() => {
            this.setState({ alert: null });
            this.props.history.push({
              pathname: '/login',
            });
          }}>
          Please check your inbox. If you did not receive this email, check your junk/spam folder.<br />
        </SweetAlert>
      ),
    });
  }

  render() {
    const { accountActivation, accountActivationError } = this.props;
    const isSuccesfulActivation = !isEmpty(accountActivation) && !accountActivationError;

    if (isSuccesfulActivation) {
      setTimeout(() => {
        this.props.history.push({
          pathname: '/login',
        });
      }, 5000);
    }

    return (
      <div className="mt-xl wd-xxl">
        <div className="panel panel-dark panel-flat">
          <div className="wd-xxl">
            {isSuccesfulActivation ? SuccesfulActivation : <UnsuccefulActivation handleSubmit={this.handleResend} />}
            <ul className="list-inline text-center text-sm mb-xl">
              <li>
                <Link to="/" className="text-muted">
                  Go to App
                </Link>
              </li>
              <li className="text-muted">|</li>
              <li>
                <Link to="login" className="text-muted">
                  Login
                </Link>
              </li>
              <li className="text-muted">|</li>
              <li>
                <Link to="register" className="text-muted">
                  Register
                </Link>
              </li>
            </ul>
          </div>
        </div>
        {this.state.alert}
      </div>
    );
  }
}

VerifyToken.defaultProps = {
  accountActivation: {},
  accountActivationLoading: false,
  accountActivationError: false,
};

VerifyToken.propTypes = {
  location: PropTypes.object.isRequired,
  activateUser: PropTypes.func.isRequired,
  accountActivation: PropTypes.object,
  accountActivationError: PropTypes.bool,
  resendActivation: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    accountActivation: state.auth.accountActivation,
    accountActivationLoading: state.auth.accountActivationLoading,
    accountActivationError: state.auth.accountActivationError,
  };
}

export default connect(mapStateToProps, { activateUser, resendActivation })(VerifyToken);
