import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { change, untouch } from 'redux-form/lib/actions';
import isSubmitting from 'redux-form/lib/isSubmitting';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { signinUser, invalidateError } from '../../actions/authActions';
import LoginForm from './LoginForm';

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.submitting && nextProps.errorMessage) {
      this.props.change('login', 'password', '');
      this.props.untouch('login', 'password');
    }
    return true;
  }

  componentWillUnmount() {
    this.props.invalidateError();
  }

  handleFormSubmit(values) {
    // Do something with the form values
    if (this.props.isVerified) {
      return this.props.signinUser(values, this.props);
    }
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
    return null;
  }

  render() {
    const { submitting } = this.props;

    return (
      <div className="mt-xl wd-xl">
        <div className="panel panel-dark panel-flat">
          <div className="panel-heading text-center">
            <h3 className="text-center text-nowrap text-bold">Welcome to SELBIM PORT</h3>
            <p className="text-center  text-muted">
              <strong>LOG IN TO CONTINUE.</strong>
            </p>
          </div>
          <div className="panel-body">
            <LoginForm onSubmit={this.handleFormSubmit} />
            <div className="mt-lg">{this.renderAlert()}</div>
            <p className="pt-lg text-center">Need to Register?</p>
            {submitting ? (
              <Link to="register" className="btn btn-block btn-default" disabled>
                Register Now
              </Link>
            ) : (
              <Link to="register" className="btn btn-block btn-default">
                Register Now
              </Link>
            )}
            <div className="text-center mt-lg">
              <small className="text-muted">
                Creating an account means you’re okay with our privacy policy{' '}
                <strong>
                  <a href="" rel="noopener noreferrer" target="_blank">
                    Terms of Service
                  </a>
                </strong>
                ,{' '}
                <strong>
                  <a href="" rel="noopener noreferrer" target="_blank">
                    Privacy Policy
                  </a>
                </strong>
              </small>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.defaultProps = {
  errorMessage: '',
};

Login.propTypes = {
  submitting: PropTypes.bool.isRequired,
  signinUser: PropTypes.func.isRequired,
  invalidateError: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
  isVerified: PropTypes.bool.isRequired,
  untouch: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    isVerified: state.auth.isCaptchaCompleted,
    submitting: isSubmitting('login')(state),
  };
}

export default connect(mapStateToProps, {
  signinUser,
  invalidateError,
  untouch,
  change,
})(Login);
