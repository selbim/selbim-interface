## WEBSITE TERMS OF USE

PLEASE READ THE FOLLOWING TERMS OF USE CAREFULLY, AS THEY CONSTITUTE A BINDING LEGAL AGREEMENT BETWEEN YOU AND VELOXITY, INC. (“VELOXITY”) BY ACCESSING, BROWSING, DOWNLOADING MATERIALS AND/OR OTHERWISE USING THE VELOXITY SITE, YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, AND, ON BEHALF OF YOURSELF AND ANY ENTITY ON WHOSE BEHALF YOU ARE USING THE VELOXITY SITE, AGREE TO BE BOUND BY THE FOLLOWING TERMS AS DEFINED BELOW. IF YOU DO NOT AGREE TO THESE TERMS, THEN PLEASE DO NOT USE THE VELOXITY SITE.


### 1. RESTRICTIONS ON USE

The audio and visual information, documentation, data, software, products, services, material and related graphics available on this site (“Materials”) are protected by copyright laws, international copyright treaties, and other intellectual property laws and treaties.

As between Veloxity and you, Veloxity alone shall own all rights, title and interest evidenced by, embodied in, and/or attached/connected/related to the Materials (other than the User Materials), as well as the compilation of all Materials on this site. Veloxity owns names, trademarks, service marks and logos presented on the site. You may not reproduce, edit, modify, display, distribute or make any other use of the Materials, in any form or by any means without Veloxity’s prior written consent.

Veloxity grants you permission to download and display the Materials posted on this site solely for informational and personal use, provided that you do not modify such Materials and provided further that you retain all copyright and proprietary notices as they appear in such Materials.

The permission granted to you herein shall terminate if you breach any of these terms and conditions. Upon termination you agree to destroy any Materials downloaded from this site. Unauthorized use of any of these Materials is expressly prohibited by law, and may result in civil and criminal penalties.

You may not use any content contained in the Materials in any manner that may give a false or misleading impression or statement as to Veloxity or any third party referenced in the Materials. You agree to use the site and the Materials accessible via the site only for lawful purposes.

### 2. WARRANTIES AND DISCLAIMERS

Veloxity intends for the Materials created on this site to be accurate and reliable. However, such Materials could contain technical inaccuracies, typographical errors or other mistakes. Veloxity may make changes to the Materials at any time without notice. The Materials may be out of date and Veloxity makes no commitment to update such Materials. Further, Veloxity shall not be responsible in any way to third party Materials.

THIS SITE AND MATERIALS ON THIS SITE ARE PROVIDED ON AN “AS IS” WITHOUT ANY WARRANTY OF ANY KIND, WITH ALL FAULTS AND “AS AVAILABLE” BASIS. THE USE OF MATERIALS IS MADE AT YOUR OWN DISCRETION AND RISK AND WITH YOUR AGREEMENT THAT YOU WILL SOLELY BE RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR MOBILE DEVICE, LOSS OF DATA, OR OTHER HARM THAT RESULTS FROM SUCH ACTIVITIES. THE VELOXITY ENTITIES MAKE NO CLAIMS OR PROMISES ABOUT THE QUALITY, ACCURACY, OR RELIABILITY OF THE SITE, ITS SAFETY OR SECURITY, OR THE MATERIALS. VELOXITY DISCLAIMS ALL WARRANTIES OR REPRESENTATIONS ABOUT THE MATERIALS, INCLUDING ANY WITHOUT LIMITATION IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT OF ANY THIRD PARTY INTELLECTUAL PROPERTY RIGHT.

### 3. LIMITATION OF LIABILITY

TO THE FULLEST EXTENT POSSIBLE UNDER APPLICABLE LAW, IN NO EVENT SHALL VELOXITY, ITS SUBSIDIARIES, AFFILIATES, AGENTS, OFFICERS, DIRECTORS, EMPLOYEES, PARTNERS OR SUPPLIERS BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DAMAGES WHATSOEVER, INCLUDING BUT NOT LIMITED TO, INDIRECT, PUNITIVE, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION RESULTING FROM USE OF THIS SITE OR OF THE MATERIALS, REGARDLESS OF WHETHER VELOXITY OR AN AUTHORIZED VELOXITY REPRESENTATIVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

### 4. INDEMNITIES AND LIABILITY

You agree to indemnify and hold Veloxity, and its subsidiaries, affiliates, officers, agents, co-branders or other partners, employees and suppliers harmless from any claim or demand, including reasonable attorneys’ fees, made by any third party due to or arising out of (a) files or other material containing images, photographs, software or other material you upload, submit or otherwise transmit, (b) your use of Materials, (c) your violation of these Terms and Conditions or (d) your violation of any law, rule or regulation or the rights of any other person or entity.

### 5. THIRD PARTY SITES AND CONTENT

This site may provide links to websites and/or web pages operated by third parties offering materials, information, and services (the “Third Party Sites”). The links are only provided as a convenience and Veloxity does not endorse any of these websites. Veloxity is not responsible for the contents of any linked site or any changes or updates to such sites. Veloxity assumes no responsibility or liability for any material that may be accessed on other websites or reached through this website. Third party sites are not under the control of Veloxity. Your use of the Third Party Sites and the use of information collected by the operators of such sites are governed by the terms of use and privacy policies published by such sites. VELOXITY’S PUBLICATION OF INFORMATION REGARDING THIRD-PARTY PRODUCTS OR SERVICES DOES NOT CONSTITUTE AN ENDORSEMENT REGARDING THE SUITABILITY OF SUCH PRODUCTS OR SERVICES OR A WARRANTY, REPRESENTATION OR ENDORSEMENT OF SUCH PRODUCTS OR SERVICES, EITHER ALONE OR IN COMBINATION WITH ANY VELOXITY PRODUCT OR SERVICE AND VELOXITY FULLY DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATIONS IN RELATION THERETO.

### 6. COPYRIGHTS AND TRADEMARK INFORMATION

The trademarks, service marks and logos used and displayed on this site are registered and unregistered trademarks and service marks of Veloxity and others. All other registered and unregistered trademarks used on the site are the property of their respective owners. Except as provided herein, you are not granted, expressly or by implication, estoppel or otherwise, any license or right to use any Veloxity trademark, service mark or logo used or displayed on the site without the prior express written permission of Veloxity. When used with Veloxity’s permission, all trademarks must be identified as trademarks of Veloxity using the appropriate symbol at the first occurrence in the text of any published printed or electronic communications.

### 7. JURISDICTIONS AND GOVERNING LAW

These terms and conditions shall be exclusively governed by the laws of California, USA, without regard to the choice or conflicts of law provisions thereof, and any disputes, actions, claims or causes of action arising out of or in connection with these terms and conditions shall be subject to the exclusive jurisdiction of the competent Federal and State courts in the Santa Clara County, California, USA. You agree to waive all defenses of lack of personal jurisdiction and forum non-convenient and agree that process may be served in a manner authorized by applicable law or court rule.

### 8. PRIVACY POLICY

Veloxity respects your privacy and we are committed to protect the personal information that you share with us. You can browse through most of our website without giving us any information about yourself. To access certain information, to receive certain services, you may need to provide some information about yourself. The use of the site and any Materials therein shall at all times be subject to Veloxity’s Privacy Policy which constitutes an integral part of these terms and conditions. The Privacy Policy is available at the [Privacy Policy](/privacy). Veloxity reserves the right to modify the Privacy Policy in its discretion from time to time. Therefore it is recommended that you read it periodically. Continued use of the website after any such changes are made shall constitute your consent to such changes.

### 9. CONTACT INFORMATION

If you have any questions regarding these Terms and Conditions or if you request further information or permission to reproduce any portions of the Materials in addition to the permission granted above please contact Veloxity at [info@blinnk.com](mailto:info@blinnk.com)
