import React from 'react';

const Maintenance = () => (
  <div className="abs-center">
    <div className="text-center mv-lg">
      <h1 className="mb-lg">
        <sup>
          <em className="fa fa-cog fa-2x text-muted fa-spin text-info" />
        </sup>
        <em className="fa fa-cog fa-5x text-muted fa-spin text-purple" />
        <em className="fa fa-cog fa-lg text-muted fa-spin text-success" />
      </h1>
      <div className="text-bold text-lg mb-lg">SITE IS UNDER MAINTENANCE</div>
      <p className="lead m0">We will back online shortly!</p>
    </div>
  </div>
);

export default Maintenance;
