import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Page404 = ({ authKey }) =>
  (<div className="mt-xl wd-xxl">
    <div className="panel panel-dark panel-flat">
      <div className="wd-xxl">
        <div className="text-center">
          <div className="panel-heading text-center">
            <div className="mb-lg mt-lg">
              <span className="fa fa-exclamation fa-5x text-muted" />
            </div>
            <div className="text-lg mb-lg">404</div>
          </div>
          <p className="lead m0">We could not find this page.</p>
          <p>The page you are looking for does not exists.</p>
        </div>
        <div className="list-inline text-center text-sm mb-xl text-muted">
          {isEmpty(authKey)
            ? <div>
              <Link to="/" className="text-muted">
                  Go to App
                </Link>{' '}
                |{' '}
              <Link to="login" className="text-muted">
                  Login
                </Link>{' '}
                |{' '}
              <Link to="register" className="text-muted">
                  Register
                </Link>
            </div>
            : <Link to="/" className="text-muted">
                Go to App
              </Link>}
        </div>
      </div>
    </div>
  </div>);

Page404.defaultProps = {
  authKey: '',
};
Page404.propTypes = {
  authKey: PropTypes.string,
};
function mapStateToProps(state) {
  return {
    authKey: state.user.authenticationKey,
  };
}

export default connect(mapStateToProps)(Page404);
