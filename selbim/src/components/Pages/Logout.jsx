import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal';
import { FormattedMessage } from 'react-intl';
import { signoutUser } from '../../actions/authActions';

class Logout extends Component {
  constructor(props) {
    super(props);
    this.logoutUser = this.logoutUser.bind(this);
  }

  logoutUser() {
    this.props.signoutUser().then(() => this.props.history.push('/login'));
  }

  render() {
    return (
      <Modal show className="">
        <Modal.Header>
          <Modal.Title className="text-center"><FormattedMessage id="logout.alert.title" /></Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-center">
          <div className="m-xl">
            <i className="fa fa-sign-out fa-3x" />
          </div>
          <div>
            <FormattedMessage id="logout.alert.description" />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="primary" onClick={this.logoutUser}>
            <FormattedMessage id="engage.publish.alert.warning.confirmbtnText" />
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

Logout.propTypes = {
  signoutUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export default connect(null, { signoutUser })(Logout);
