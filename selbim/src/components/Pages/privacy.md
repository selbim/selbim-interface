## WEBSITE PRIVACY POLICY

This Privacy Policy explains what information we gather from you in connection with your use of the Veloxity Website (“Site”), and how we use and disclose that information. This privacy policy does not apply to any of Veloxity’s mobile applications or services that do not display or link to this statement or link to a different privacy statements. By using our Site, you expressly consent to the information handling policies described in this policy.

### Information We Collect

User-Provided Information. You may provide to us what is generally called “personally identifiable” information (such as your name, e-mail address, etc.) when you submit the “Contact Us” form.

### Cookies

We may automatically collect information using “cookies.” Cookies are small data files stored on your hard drive by a website. Among other things, cookies help us improve our Site and your experience. We use cookies to see which areas and features are popular and to count visits to our Site.

### Web beacons

When you use our Site we may automatically record certain information from your web browser by using different types of technology, including web beacons. Web beacons are electronic images that may be used on our Site or in our emails. We use web beacons to deliver cookies, count visits, and understand usage.

### Google Analytics

We use Google Analytics to help analyze how users use our Site. Google Analytics uses cookies to collect information such as how often users visit our Site, what pages they visit, and what other sites they used prior to coming to our Site. We use the information we get from Google Analytics only to improve our Site and Services. Google Analytics collects only the IP address assigned to you on the date you visit the Site, rather than your name or other personally identifying information. We do not combine the information generated through the use of Google Analytics with your Personal Data. Although Google Analytics plants a persistent cookie on your web browser to identify you as a unique user the next time you visit the Site, the cookie cannot be used by anyone but Google. The Google Analytics Terms of Use and the Google Privacy Policy restrict Google’s ability to use and share information collected by Google Analytics about your visits to our Site.

### Use of the Collected Information

Veloxity may use personal information collected through our Site for the purposes of:

- responding to you when you submit the “Contact Us” form
- understanding and analyzing the usage trends and preferences to provide, maintain, support, personalize, modify and improve our Site.

### Opting Out

Most Web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our Site.

### Security of Your Information

We take reasonable steps to help protect your personal information in an effort to prevent loss, misuse, and unauthorized access, disclosure, alteration, and destruction.

### Changes to Our Privacy Policy

Our Privacy Policy may change from time to time. We will post all changes to this Privacy Policy on this page and will indicate at the top of the page the modified policy’s effective date. We therefore encourage you to refer to this page on an ongoing basis so that you are aware of our current privacy policy.

### Contact Information

If you have any questions about this policy, please contact us at [info@blinnk.com](mailto:info@blinnk.com).
