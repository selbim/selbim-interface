import React from 'react';
import PropTypes from 'prop-types';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';

const validate = (formProps) => {
  const errors = {};
  if (!formProps.email) {
    errors.email = 'Please enter an email';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formProps.email)) {
    errors.email = 'Invalid email address';
  }
  if (!formProps.password) {
    errors.password = 'Please enter a password';
  }
  if (!formProps.passwordConfirm) {
    errors.passwordConfirm = 'Please enter a password confirmation';
  }
  if (formProps.password !== formProps.passwordConfirm) {
    errors.password = 'Passwords must match';
  }
  return errors;
};

const RenderFieldMail = ({ input, label, type, meta: { touched, error } }) =>
  (<div>
    <div className=" has-feedback">
      <input className="form-control mb-lg" {...input} placeholder={label} type={type} />
      <span className="fa fa-envelope form-control-feedback text-muted" />
    </div>
    <div className="alert-danger">{touched && error ? error : '\r'}</div>
  </div>);

const RenderFieldPassword = ({ input, label, type, meta: { touched, error } }) =>
  (<div>
    <div className=" has-feedback">
      <input className="form-control mb-lg" {...input} placeholder={label} type={type} />
      <span className="fa fa-lock form-control-feedback text-muted" />
    </div>
    <div className="alert-danger mb-lg">{touched && error ? error : '\r'}</div>
  </div>);

const RegisterForm = (props) => {
  const { handleSubmit, pristine, submitting, terms, termsAccepted } = props;
  return (
    <form onSubmit={handleSubmit} className="mb-lg">
      <Field name="email" type="text" component={RenderFieldMail} label="Email" placeholder="Email" />
      <Field name="password" type="password" component={RenderFieldPassword} label="Password" placeholder="Password" />
      <Field
        name="passwordConfirm"
        type="password"
        component={RenderFieldPassword}
        label="Confirm Password"
        placeholder="Repeat Password" />
      {terms}
      <button
        type="submit"
        className="btn btn-block btn-primary mt-lg"
        disabled={pristine || !termsAccepted || submitting}>
        {submitting ? 'Creating Account...' : 'Create Account'}
      </button>
    </form>
  );
};

RenderFieldPassword.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

RenderFieldMail.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

RegisterForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  terms: PropTypes.object.isRequired,
  termsAccepted: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'register',
  validate,
})(RegisterForm);
