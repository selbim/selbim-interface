import React from 'react';
import { Link } from 'react-router-dom';

const Lock = () => (
  <div className="abs-center wd-xl">
    <div className="p">
      <img
        src="img/user/13.jpg"
        alt="Avatar"
        width="60"
        height="60"
        className="img-thumbnail img-circle center-block" />
    </div>
    <div className="panel widget b0">
      <div className="panel-body">
        <p className="text-center">Please login to unlock your screen.</p>
        <form>
          <div className="form-group has-feedback">
            <input id="exampleInputPassword1" type="password" placeholder="Password" className="form-control" />
            <span className="fa fa-lock form-control-feedback text-muted" />
          </div>
          <div className="clearfix">
            <div className="pull-left mt-sm">
              <Link to="recover" className="text-muted">
                <small>Forgot your password?</small>
              </Link>
            </div>
            <div className="pull-right">
              <Link to="/" className="btn btn-sm btn-primary">Unlock</Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
);

export default Lock;
