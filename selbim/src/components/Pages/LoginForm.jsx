import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import Recaptcha from 'react-recaptcha';
import PropTypes from 'prop-types';
import reduxForm from 'redux-form/lib/reduxForm';
import Field from 'redux-form/lib/Field';

import { isCaptchaVerified, isCaptchaLoaded } from '../../actions';

// generated from https://www.google.com/recaptcha for console.blinnk.com
const sitekey = '6Lf3dyUUAAAAAPPAMSmVemcxeB_2LWNoswPjuaQ0';

const LoginForm = (props) => {
  const { handleSubmit, pristine, submitting } = props;

  let recaptchaInstance;

  const expiredCallback = () => {
    recaptchaInstance.reset();
  };

  let captcha = null;
  if (props.loginCount >= 3) {
    captcha = (
      <Recaptcha
        ref={function (e) {
          recaptchaInstance = e;
          return recaptchaInstance;
        }}
        sitekey={sitekey}
        render="explicit"
        verifyCallback={props.isCaptchaVerified}
        onloadCallback={props.isCaptchaLoaded}
        expiredCallback={expiredCallback} />
    );
  }

  return (
    <form onSubmit={handleSubmit} className="mb-lg">
      <div className=" has-feedback login-margin-bottom">
        <Field name="email" className="form-control" component="input" type="text" placeholder="Email" />
        <span className="fa fa-envelope form-control-feedback text-muted" />
      </div>
      <div className="has-feedback">
        <Field name="password" className="form-control" component="input" type="password" placeholder="Password" />
        <span className="fa fa-lock form-control-feedback text-muted" />
      </div>
      <div className="pull-right mt-lg">
        <Link to="recover" className="text-muted">Forgot your password?</Link>
      </div>
      <Row>
        <Col lg={12}>
          <div className="captcha mt-lg">
            {captcha}
          </div>
        </Col>
      </Row>
      <div className="col-6">
        <button type="submit" className="btn btn-block btn-primary mt-lg" disabled={pristine || submitting}>
          {submitting ? 'Logging In...' : 'Login'}
        </button>
      </div>
    </form>
  );
};

function mapStateToProps(state) {
  return {
    loginCount: state.auth.faultyLoginCount,
    isVerified: state.auth.isCaptchaCompleted,
  };
}

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  loginCount: PropTypes.number.isRequired,
  isCaptchaVerified: PropTypes.func.isRequired,
  isCaptchaLoaded: PropTypes.func.isRequired,
};

const form = reduxForm({
  form: 'login',
})(LoginForm);

export default connect(mapStateToProps, {
  isCaptchaVerified,
  isCaptchaLoaded,
})(form);
