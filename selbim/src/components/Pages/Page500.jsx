import React from 'react';
import { Link } from 'react-router-dom';

const Page500 = () =>
  (<div className="block-center mt-xl wd-xxl">
    <div className="panel panel-dark panel-flat">
      <div className="wd-xxl">
        <div className="text-center mb-xl">
          <div className="panel-heading text-center">
            <div className="mb-lg mt-lg">
              <em className="fa fa-wrench fa-5x text-muted" />
            </div>
            <div className="text-lg mb-lg">500</div>
          </div>
          <p className="lead m0">Oh! Something went wrong :(</p>
          <p>Dont worry, we are now checking this.</p>
          <p className="ml-xl mr-xl">In the meantime, please try one of those links below or come back in a moment</p>
        </div>
        <ul className="list-inline text-center text-sm mb-xl">
          <li>
            <Link to="/" className="text-muted">
              Go to App
            </Link>
          </li>
          <li className="text-muted">|</li>
          <li>
            <Link to="login" className="text-muted">
              Login
            </Link>
          </li>
          <li className="text-muted">|</li>
          <li>
            <Link to="register" className="text-muted">
              Register
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </div>);

export default Page500;
