import React from 'react';
import PropTypes from 'prop-types';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover from 'react-bootstrap/lib/Popover';
import { FormattedMessage } from 'react-intl';

const Tooltip = ({ tooltipText }) => (
  <Popover id="pp1">
    <FormattedMessage id={tooltipText} />
  </Popover>
);

const TooltipHover = ({ children, tooltipText }) => (
  <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={Tooltip({ tooltipText })}>
    {children}
  </OverlayTrigger>
);

Tooltip.defaultProps = {
  tooltipText: '',
};

Tooltip.propTypes = {
  tooltipText: PropTypes.string,
};

TooltipHover.defaultProps = {
  tooltipText: '',
};

TooltipHover.propTypes = {
  children: PropTypes.element.isRequired,
  tooltipText: PropTypes.string,
};

export default TooltipHover;
