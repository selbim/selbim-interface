import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import findIndex from 'lodash/findIndex';
import Dropdown from 'react-bootstrap/lib/Dropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import { changeLocale } from '../../actions';

class LocaleToggle extends React.Component {
  constructor(props) {
    super(props);
    this.onLocaleToggle = this.onLocaleToggle.bind(this);
  }

  onLocaleToggle(e) {
    this.props.changeLocale(e);
  }

  render() {
    const { locale } = this.props;
    const appLocales = [{ value: 'en', label: 'English' }, { value: 'tr', label: 'Türkçe' }];
    const defaultLocale = findIndex(appLocales, o => o.value === locale);
    return (
      <div className="pull-right">
        <Dropdown id="dropdown-tr" pullRight>
          <Dropdown.Toggle>
            {appLocales[defaultLocale].label}
          </Dropdown.Toggle>
          <Dropdown.Menu className="animated fadeInUpShort">
            {appLocales.map(key =>
              <MenuItem key={key.value} eventKey={key.value} onSelect={this.onLocaleToggle}>{key.label}</MenuItem>,
            )}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    );
  }
}

LocaleToggle.propTypes = {
  changeLocale: PropTypes.func.isRequired,
  locale: PropTypes.string.isRequired,
};

const mapStateToProps = function (state) {
  return {
    locale: state.lang.locale,
  };
};

export default connect(mapStateToProps, { changeLocale })(LocaleToggle);
