import React from 'react';
import { render } from 'react-dom';
import 'babel-polyfill';

import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import 'react-table/react-table.css';
import 'rc-time-picker/assets/index.css';
import 'react-day-picker/lib/style.css';
import 'react-bootstrap-daterangepicker/css/daterangepicker.css';

import Root from './containers/Root';
import configureStore from './store/configureStore';
import { AUTH } from './actions/actionTypes';
import registerServiceWorker from './registerServiceWorker';

// Application Styles
import './styles/bootstrap.scss';
import './styles/app.scss';
import './styles/themes/theme-g.scss';

import '../node_modules/screenfull/dist/screenfull';
import '../node_modules/leaflet/dist/leaflet.css';
import '../node_modules/leaflet-draw/dist/leaflet.draw.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min';
import '../node_modules/flipclock/compiled/flipclock.css';
import '../node_modules/flipclock/compiled/flipclock';

const intlPolyfill = require('intl');
const intlEnPolyfill = require('intl/locale-data/jsonp/en.js');

const store = configureStore();

const user = JSON.parse(localStorage.getItem('user'));
// If we have a token, consider the user to be signed in
if (user) {
  // we need to update application state
  store.dispatch({ type: AUTH.SUCCESS, payload: { user } });
}

if (!global.Intl) {
  // Safari fix: Ensure that Intl Polyfill is loaded before starting app.
  require.ensure([], () => intlPolyfill, intlEnPolyfill, 'international');
}

render(<Root store={store} />, document.getElementById('app'));
registerServiceWorker();
