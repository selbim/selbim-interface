import React from 'react';
import PropTypes from 'prop-types';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';

// Wrapper component for bootstrap navigation and component highlighting.
class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { eventKey: '' };
  }
  render() {
    const { children, eventKey } = this.props;

    return (
      <Nav className="offer-nav">
        <NavItem
          eventKey={eventKey}
          componentClass="span"
          onClick={() => this.setState({ eventKey: this.props.eventKey })}>
          {children}
        </NavItem>
      </Nav>
    );
  }
}

Navigation.propTypes = { children: PropTypes.object.isRequired, eventKey: PropTypes.string.isRequired };
export default Navigation;
