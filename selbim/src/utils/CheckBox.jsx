import React from 'react';
import PropTypes from 'prop-types';

class CheckBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { checked, onChange, disabled } = this.props;
    return <input type="checkbox" checked={checked} onChange={onChange} disabled={disabled} />;
  }
}
CheckBox.defaultProps = {
  checked: false,
  disabled: false,
};

CheckBox.propTypes = {
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};
export default CheckBox;
