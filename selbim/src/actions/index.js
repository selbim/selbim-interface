import { CHANGE_LOCALE, CAPTCHA_VERIFIED, CAPTCHA_LOADED } from './actionTypes';

export function isCaptchaVerified() {
  return {
    type: CAPTCHA_VERIFIED,
  };
}

export function isCaptchaLoaded() {
  return {
    type: CAPTCHA_LOADED,
  };
}

export function changeLocale(languageLocale) {
  localStorage.setItem('locale', languageLocale);
  return {
    type: CHANGE_LOCALE,
    locale: languageLocale,
  };
}
