import { CALL_API } from '../middleware';
import {
  AUTH,
  UNAUTH,
  INVALIDATE_AUTH_ERROR,
  ACCOUNT_ACTIVATION,
  ACCOUNT_ACTIVATION_RESEND,
  RESET_PASSWORD,
  RESET_PASSWORD_COMPLETE,
  CLEAR_AUTH_SUCCESS_STATE,
  INVALIDATE_RESET_PASSWORD,
  CHANGE_PASSWORD,
  INVALIDATE_CHANGE_PASSWORD,
} from './actionTypes';

export function signinUser({ email, password }, props) {
  return {
    [CALL_API]: {
      endpoint: '/general/user/login',
      method: 'POST',
      body: JSON.stringify({ email, password }),
      types: [
        AUTH.REQUEST,
        {
          type: AUTH.SUCCESS,
          payload: (action, state, res) => {
            if (res) {
              return res.json().then((json) => {
                if (json.status) {
                  localStorage.setItem('token', json.user.authenticationKey);
                  localStorage.setItem('user', JSON.stringify(json.user));
                  props.history.push('/');
                  return json;
                }
                return {};
              });
            }
            return '';
          },
        },
        AUTH.FAIL,
      ],
    },
  };
}

export function signupUser({ email, password }) {
  return {
    [CALL_API]: {
      endpoint: '/general/user/register',
      method: 'POST',
      body: JSON.stringify({ user: { email, password } }),
      types: [
        AUTH.REQUEST,
        {
          type: AUTH.SUCCESS,
          payload: (action, state, res) => {
            if (res) {
              return res.json();
            }
            return '';
          },
        },
        AUTH.FAIL,
      ],
    },
  };
}

export function signoutUser() {
  return {
    [CALL_API]: {
      endpoint: '/general/user/logout ',
      method: 'POST',
      body: JSON.stringify({ authenticationKey: localStorage.getItem('token') }),
      types: [
        UNAUTH.REQUEST,
        {
          type: UNAUTH.SUCCESS,
          payload: (action, state, res) => {
            if (res) {
              localStorage.removeItem('token');
              localStorage.removeItem('user');
              return res.json().then(json => json);
            }
            return '';
          },
        },
        UNAUTH.FAIL,
      ],
    },
  };
}

export function activateUser(token) {
  return {
    [CALL_API]: {
      endpoint: ' /general/user/activate ',
      method: 'POST',
      body: JSON.stringify({ token }),
      types: [ACCOUNT_ACTIVATION.REQUEST, ACCOUNT_ACTIVATION.SUCCESS, ACCOUNT_ACTIVATION.FAIL],
    },
  };
}

export function resendActivation(email) {
  return {
    [CALL_API]: {
      endpoint: ' /general/user/resend-activation ',
      method: 'POST',
      body: JSON.stringify({ email }),
      types: [ACCOUNT_ACTIVATION_RESEND.REQUEST, ACCOUNT_ACTIVATION_RESEND.SUCCESS, ACCOUNT_ACTIVATION_RESEND.FAIL],
    },
  };
}

export function resetPassword(email) {
  return {
    [CALL_API]: {
      endpoint: ' /general/user/pwr ',
      method: 'POST',
      body: JSON.stringify(email),
      types: [RESET_PASSWORD.REQUEST, RESET_PASSWORD.SUCCESS, RESET_PASSWORD.FAIL],
    },
  };
}

export function resetPasswordComplete(password, urlToken) {
  return {
    [CALL_API]: {
      endpoint: ' /general/user/pwr-complete ',
      method: 'POST',
      body: JSON.stringify({
        token: urlToken,
        password,
      }),
      types: [RESET_PASSWORD_COMPLETE.REQUEST, RESET_PASSWORD_COMPLETE.SUCCESS, RESET_PASSWORD_COMPLETE.FAIL],
    },
  };
}

export function changePassword({ newPassword }) {
  return {
    [CALL_API]: {
      endpoint: ' /general/user/pwc ',
      method: 'POST',
      body: JSON.stringify({ authenticationKey: localStorage.getItem('token'), password: newPassword }),
      types: [CHANGE_PASSWORD.REQUEST, CHANGE_PASSWORD.SUCCESS, CHANGE_PASSWORD.FAIL],
    },
  };
}

export function invalidateError() {
  return {
    type: INVALIDATE_AUTH_ERROR,
  };
}

export function clearAuthSuccessState() {
  return {
    type: CLEAR_AUTH_SUCCESS_STATE,
  };
}

export function invalidateResetPassword() {
  return {
    type: INVALIDATE_RESET_PASSWORD,
  };
}

export function invalidateChangePassword() {
  return {
    type: INVALIDATE_CHANGE_PASSWORD,
  };
}
