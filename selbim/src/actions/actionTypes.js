import isEmpty from 'lodash/isEmpty';
import localeData from '../locales/data.json';

const asyncActionType = type => ({
  REQUEST: `${type}_REQUEST`,
  SUCCESS: `${type}_SUCCESS`,
  FAIL: `${type}_FAIL`,
});

export const DEVICE_TYPES = {
  ANDROID: 'ANDROID',
  iOS: 'IOS',
  BOTH: 'BOTH',
};

// Root Url of Web Services
export const ROOT_URL = 'htpps';

// Captcha Action
export const CAPTCHA_VERIFIED = 'CAPTCHA_VERIFIED';
export const CAPTCHA_LOADED = 'CAPTCHA_LOADED';

// Authorization Actions
export const AUTH = asyncActionType('AUTH');
export const UNAUTH = asyncActionType('UNAUTH');
export const INVALIDATE_AUTH_ERROR = 'INVALIDATE_AUTH_ERROR';
export const ACCOUNT_ACTIVATION = asyncActionType('ACCOUNT_ACTIVATION');
export const ACCOUNT_ACTIVATION_RESEND = asyncActionType('ACCOUNT_ACTIVATION_RESEND');
export const RESET_PASSWORD = asyncActionType('RESET_PASSWORD');
export const RESET_PASSWORD_COMPLETE = asyncActionType('RESET_PASSWORD_COMPLETE');
export const CLEAR_AUTH_SUCCESS_STATE = 'CLEAR_AUTH_SUCCESS_STATE';
export const INVALIDATE_RESET_PASSWORD = 'INVALIDATE_RESET_PASSWORD';
export const CHANGE_PASSWORD = asyncActionType('CHANGE_PASSWORD');
export const INVALIDATE_CHANGE_PASSWORD = 'INVALIDATE_CHANGE_PASSWORD';

// Application Actions
export const FETCH_APP_STAT = asyncActionType('FETCH_APP_STAT');
export const FETCH_DEVICE_STAT = asyncActionType('FETCH_DEVICE_STAT');
export const FETCH_APP_USAGE = asyncActionType('FETCH_APP_USAGE');

// Application Segmentation Actions
export const FETCH_LOCATION_SEG = asyncActionType('FETCH_LOCATION_SEG');
export const FETCH_APP_USAGE_SEG = asyncActionType('FETCH_APP_USAGE_SEG');
export const FETCH_APP_INSTALL_SEG = asyncActionType('FETCH_APP_INSTALL_SEG');
export const FETCH_NETWORK_SEG = asyncActionType('FETCH_NETWORK_SEG');

export const LOAD_MORE_LOCATION_SEG = 'LOAD_MORE_LOCATION_SEG';
export const LOAD_MORE_APP_USAGE_SEG = 'LOAD_MORE_APP_USAGE_SEG';
export const LOAD_MORE_APP_INSTALL_SEG = 'LOAD_MORE_APP_INSTALL_SEG';
export const LOAD_MORE_NETWORK_SEG = 'LOAD_MORE_NETWORK_SEG';

// Audience/Event/Offer Actions
export const FETCH_COUNTRIES = asyncActionType('FETCH_COUNTRIES');
export const FETCH_OPERATORS = asyncActionType('FETCH_OPERATORS');
export const AUDIENCE_SEGMENTATION = asyncActionType('AUDIENCE_SEGMENTATION');
export const EVENT_SEGMENTATION = asyncActionType('EVENT_SEGMENTATION');
export const POTENTIAL_AUDIENCE = asyncActionType('POTENTIAL_AUDIENCE');

// Campaign Actions
export const SAVE_SEGMENTATION_DATA = 'SAVE_SEGMENTATION_DATA';
export const SAVE_TIME_AND_DATE = 'SAVE_TIME_AND_DATE';
export const UPLOAD_HTML_PROMOTION = asyncActionType('UPLOAD_HTML_PROMOTION');
export const DEVICE_TYPE_CHANGED = 'DEVICE_TYPE_CHANGED';
export const AGE_CHANGED = 'AGE_CHANGED';
export const GENDER_CHANGED = 'GENDER_CHANGED';
export const OPERATOR_CHANGED = 'OPERATOR_CHANGED';
export const HOSTAPP_CHANGED = 'HOSTAPP_CHANGED';
export const COUNTRY_CHANGED = 'COUNTRY_CHANGED';
export const CAMPAIGN_TYPE_CHANGED = 'CAMPAIGN_TYPE_CHANGED';
export const APPLICATIONS_CHANGED = 'APPLICATIONS_CHANGED';
export const OFFER_PROPERTY_CHANGED = 'OFFER_PROPERTY_CHANGED';
export const OFFER_PROPERTY_INVALIDATE = 'OFFER_PROPERTY_INVALIDATE';
export const SAVE_MAP_GEOFENCES = 'SAVE_MAP_GEOFENCES';
export const SAVE_FORM_GEOFENCES = 'SAVE_FORM_GEOFENCES';
export const SAVE_CSV_GEOFENCES = 'SAVE_CSV_GEOFENCES';
export const CHANGE_CAMPAIGN_DETAILS = 'CHANGE_CAMPAIGN_DETAILS';
export const INVALIDATE_CAMPAIGN = 'INVALIDATE_CAMPAIGN';

// RTCO Actions
export const RTCO_CAMPAIGN_CREATE = asyncActionType('RTCO_CAMPAIGN_CREATE');
export const RTCO_CAMPAIGN_LIST = asyncActionType('RTCO_CAMPAIGN_LIST');
export const RTCO_CAMPAIGN_STATISTICS = asyncActionType('RTCO_CAMPAIGN_STATISTICS');
export const RTCO_CAMPAIGN_ACTIVATE = asyncActionType('RTCO_CAMPAIGN_ACTIVATE');
export const RTCO_CAMPAIGN_DEACTIVATE = asyncActionType('RTCO_CAMPAIGN_DEACTIVATE');
export const RTCO_CAMPAIGN_DELETE = asyncActionType('RTCO_CAMPAIGN_DELETE');
export const RTCO_CLEAR_REQUEST_STATE = 'RTCO_CLEAR_REQUEST_STATE';
export const RTCO_CLEAR_ERROR_STATE = 'RTCO_CLEAR_ERROR_STATE';

const navigatorLanguage =
  (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;

// Split locales with a region code
const languageWithoutRegionCode = navigatorLanguage.toLowerCase().split(/[_-]+/)[0];

function decideLocale() {
  const locale = localStorage.getItem('locale');
  if (!isEmpty(locale) && locale !== 'undefined') {
    return localStorage.getItem('locale');
  } else if (localeData[languageWithoutRegionCode]) {
    return languageWithoutRegionCode;
  } else if (localeData[navigatorLanguage]) {
    return navigatorLanguage;
  }
  return 'en';
}

// Locale Actions
export const CHANGE_LOCALE = 'CHANGE_LOCALE';
export const DEFAULT_LOCALE = decideLocale();

// My Apps Actions
export const APPLIST = asyncActionType('APPLIST');
export const ADD_APPLICATION_TAB = 'ADD_APPLICATION_TAB';
export const CREATE_LICENSE_KEY = asyncActionType('CREATE_LICENSE_KEY');
export const EDIT_LICENSE_KEY = asyncActionType('EDIT_LICENSE_KEY');
export const UPDATE_PN_DETAILS = asyncActionType('UPDATE_PN_DETAILS');
export const FETCH_AVERAGE_USAGE_VALUES = asyncActionType('FETCH_AVERAGE_USAGE_VALUES');
export const FETCH_APPLICATION_APP_STORE = asyncActionType('FETCH_APPLICATION_APP_STORE');
export const DELETE_APPLICATION = asyncActionType('DELETE_APPLICATION');
export const DELETE_APPLICATION_INVALIDATE = 'DELETE_APPLICATION_INVALIDATE';
export const CLEAR_LICENSE_KEY_STATE = 'CLEAR_LICENSE_KEY_STATE';
export const CLEAR_APP_STORE_QUERY = 'CLEAR_APP_STORE_QUERY';
export const UPLOAD_CERTIFICATE = asyncActionType('UPLOAD_CERTIFICATE');

// Filter size for Segmentation Data. Component will show load more if size is bigger than.
export const SEGMENTATION_FILTER_LIMIT = 6;

// Engage Actions
export const SEND_APP_EMAIL = asyncActionType('SEND_APP_EMAIL');

// Pn Preview Actions
export const GET_DEVICE_LIST = asyncActionType('GET_DEVICE_LIST');
export const ADD_DEVICE = asyncActionType('ADD_DEVICE');
export const REMOVE_DEVICE = asyncActionType('REMOVE_DEVICE');
export const SEND_PN_PREVIEW = asyncActionType('SEND_PN_PREVIEW');
export const REFRESH_ADD_DEVICE_SUCCESS_STATE = 'REFRESH_ADD_DEVICE_SUCCESS_STATE';
export const REFRESH_REMOVE_DEVICE_SUCCESS_STATE = 'REFRESH_REMOVE_DEVICE_SUCCESS_STATE';
