import { createSelector } from 'reselect';
import filter from 'lodash/filter';
import map from 'lodash/map';
import { DEVICE_TYPES } from '../actions/actionTypes';

const data = state => state.engage.eventSegmentation;

const isFilter = state => state.engage.selectedDeviceType;

/* eslint-disable */
const getEventSegmentationsWithFilter = (data, isFilter) => {
  switch (isFilter) {
    case DEVICE_TYPES.BOTH:
      return data.data;

    case DEVICE_TYPES.ANDROID:
      return filter(data.data, value => {
        let state;
        map(value.availablePlatforms, object => {
          if (object === DEVICE_TYPES.ANDROID) {
            state = true;
          }
        });
        if (state) return value;
      });

    case DEVICE_TYPES.iOS:
      return filter(data.data, value => {
        let state;
        map(value.availablePlatforms, object => {
          if (object === DEVICE_TYPES.iOS) {
            state = true;
          }
        });
        if (state) return value;
      });

    default:
      return data;
  }
};
/* eslint-enable */

export default createSelector(data, isFilter, getEventSegmentationsWithFilter);
