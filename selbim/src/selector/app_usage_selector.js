import { createSelector } from 'reselect';
import filter from 'lodash/filter';
import { SEGMENTATION_FILTER_LIMIT } from '../actions/actionTypes';

const data = state => state.dashboard.appUsageSeg;

const isFilter = state => state.dashboard.appUsageSegIsFiltered;

/* eslint-disable */
const getAppUsageSegData = (data, isFilter) => {
  switch (isFilter) {
    case 'FILTERED':
      return filter(data, (value, index) => {
        if (index < SEGMENTATION_FILTER_LIMIT) {
          return value;
        }
      });

    default:
      return data;
  }
};
/* eslint-enable */

export default createSelector(data, isFilter, getAppUsageSegData);
