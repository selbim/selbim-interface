import { createSelector } from 'reselect';
import filter from 'lodash/filter';
import { SEGMENTATION_FILTER_LIMIT } from '../actions/actionTypes';

const data = state => state.dashboard.locationSeg;

const isFilter = state => state.dashboard.locationSegIsFiltered;

/* eslint-disable */
const getLocationSegData = (data, isFilter) => {
  switch (isFilter) {
    case 'FILTERED':
      return filter(data, (value, index) => {
        if (index < SEGMENTATION_FILTER_LIMIT) {
          return value;
        }
      });

    default:
      return data;
  }
};
/* eslint-enable */

export default createSelector(data, isFilter, getLocationSegData);
