import { createSelector } from 'reselect';
import isEmpty from 'lodash/isEmpty';

const appId = (state, match) => match.params.id;
const appList = state => state.myapps.appList;

/* eslint-disable */
const getApplicationDetails = (appId, appList) => {
  let application = {};
  if (!isEmpty(appList)) {
    const app = appList[appId];
    if (app) {
      application = {
        categories: app.application.categories,
        iconUrl: app.application.iconUrl,
        label: app.application.label,
        uid: app.application.uid,
        url: app.application.url,
        group: app.group,
        id: app.id,
        instancePriority: app.instancePriority,
        license: app.license,
        platform: app.platform,
        webServiceEndpoint: app.webServiceEndpoint,
      };
      if (app.pnDetails) {
        Object.assign(application, {
          gcmSenderId: app.pnDetails.gcmSenderID,
          gcmServerKey: app.pnDetails.gcmServerKey,
          serviceUrl: app.pnDetails.serviceUrl,
          certificate: app.pnDetails.certificate,
          sandbox: app.pnDetails.sandbox,
        });
      }
    }
  }

  return application;
};
/* eslint-enable */

export default createSelector(appId, appList, getApplicationDetails);
