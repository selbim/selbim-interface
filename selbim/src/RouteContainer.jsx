import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import Base from './components/Layout/Base';
import BasePage from './components/Layout/BasePage';

function checkAuth() {
  const token = localStorage.getItem('token');
  // If we have a token, consider the user to be signed in
  if (!token) {
    return false;
  }
  return true;
}

/* eslint-disable */
export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      (checkAuth()
        ? <Base><Component {...props} /></Base>
      : <Base><Component {...props} /></Base>)}
  />
);

export const InnerRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      (checkAuth()
        ? <Component {...props} />
        : <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />)}
  />
);
/* eslint-enable */

export const DefaultRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <BasePage>
        <Component {...props} />
      </BasePage>
    )} />
);

PrivateRoute.defaultProps = {
  location: {},
};

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.object,
};

DefaultRoute.propTypes = {
  component: PropTypes.func.isRequired,
};
