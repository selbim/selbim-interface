import React, { Component } from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import isEqual from 'lodash/isEqual';
import filter from 'lodash/filter';
import Select from 'react-select';
import { connect } from 'react-redux';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import Table from 'react-bootstrap/lib/Table';
import { injectIntl, FormattedMessage } from 'react-intl';
import { saveTimeAndDate } from '../actions/campaignActions';

class TimePickerModal extends Component {
  constructor(props) {
    super(props);

    const { initialValues } = this.props;

    this.state = {
      startTime: moment('07:00', 'HH:mm'),
      endTime: moment('23:59', 'HH:mm'),
      selectedDates: initialValues,
      modalActive: false,
      days: [],
    };

    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.saveDate = this.saveDate.bind(this);
    this.deleteDate = this.deleteDate.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!isEqual(this.state.selectedDates, nextState.selectedDates)) {
      this.props.saveTimeAndDate(nextState.selectedDates);
    }
    return true;
  }

  hideModal() {
    this.setState({ show: false });
  }

  showModal() {
    this.setState({ show: true });
  }

  saveDate() {
    if (this.state.days.length === 0) return;
    this.setState({
      selectedDates: this.state.selectedDates.concat([
        { days: this.state.days, startTime: this.state.startTime, endTime: this.state.endTime },
      ]),
    });
  }

  deleteDate(event, index) {
    event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();
    this.setState(this.state.selectedDates.splice(index, 1));
  }

  blurActiveElement() {
    document.activeElement.blur();
  }

  render() {
    const { backdrop, placeholder } = this.props;

    const days = [
      { label: <FormattedMessage id="engage.event.timepickermodal.allweek" />, value: 'ALL' },
      { label: <FormattedMessage id="engage.event.timepickermodal.weekdays" />, value: 'WEEKDAYS' },
      { label: <FormattedMessage id="engage.event.timepickermodal.weekends" />, value: 'WEEKENDS' },
      { label: <FormattedMessage id="engage.event.timepickermodal.monday" />, value: 'MONDAY' },
      { label: <FormattedMessage id="engage.event.timepickermodal.tuesday" />, value: 'TUESDAY' },
      { label: <FormattedMessage id="engage.event.timepickermodal.wednesday" />, value: 'WEDNESDAY' },
      { label: <FormattedMessage id="engage.event.timepickermodal.thursday" />, value: 'THURSDAY' },
      { label: <FormattedMessage id="engage.event.timepickermodal.friday" />, value: 'FRIDAY' },
      { label: <FormattedMessage id="engage.event.timepickermodal.saturday" />, value: 'SATURDAY' },
      { label: <FormattedMessage id="engage.event.timepickermodal.sunday" />, value: 'SUNDAY' },
    ];

    const dateOutput = map(this.state.selectedDates, (value, i) =>
      (<tr key={Math.random()}>
        <td>{i + 1}</td>
        <td>
          {map(value.days, day => <span key={Math.random()}>{day.label}{' '}</span>)}
        </td>
        <td>
          {`${value.startTime.format('HH:mm')}-${value.endTime.format('HH:mm')}`}
        </td>
        <td>
          <button className="btn btn-danger" onClick={event => this.deleteDate(event, i)}>
            <FormattedMessage id="engage.event.timepickermodal.delete" />
          </button>
        </td>
      </tr>),
    );

    return (
      <div className="custom--multi" onClick={this.showModal}>
        <div className="custom-select-control">
          <span
            className={`custom-multi-value-wrapper ${this.props.intl.formatMessage(
              {
                id: 'pull.left',
              },
              { padding: 'pr-sm' },
            )}`}>
            {this.state.selectedDates.length === 0
              ? <div className={`custom-placeholder ${this.props.intl.formatMessage({ id: 'text.left' })}`}>
                {placeholder}
              </div>
              : map(this.state.selectedDates, (value, i) =>
                (<div key={Math.random()} className="custom-select-value">
                  <span
                    className="custom-select-value-icon"
                    aria-hidden="true"
                    onClick={event => this.deleteDate(event, i)}>
                      x
                    </span>
                  <span className="custom-select-value-label text-right" onClick={this.showModal}>
                    {map(value.days, day => <span key={Math.random()}>{day.label}{' '}</span>)}
                      ({value.startTime.format('HH:mm')}-{value.endTime.format('HH:mm')})
                    </span>
                </div>),
                )}
          </span>
          <div className="dropdown-content" id="modal">
            <Modal show={this.state.show} onHide={this.hideModal} backdrop={backdrop} bsSize="large">
              <Modal.Header className="text-center">
                <Modal.Title><FormattedMessage id="engage.event.timepickermodal.title" /></Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Row className="flex-container">
                  <Col className="pt-lg" sm={4} lg={4} md={4} xs={12}>
                    <Col className="no-padding" sm={12} lg={12} md={12} xs={12}>
                      <h4 className="time-picker-title"><FormattedMessage id="engage.event.timepickermodal.days" /></h4>
                    </Col>
                    <Col className="no-padding" sm={12} lg={12} md={12} xs={12}>
                      <Select
                        multi
                        options={days}
                        name="selected-days"
                        placeholder={this.props.placeholder}
                        value={this.state.days}
                        onChange={(value) => {
                          if (value.length === 0) return this.setState({ days: value });
                          const lastValue = value[value.length - 1];
                          if (
                            lastValue.value === 'ALL' ||
                            lastValue.value === 'WEEKDAYS' ||
                            lastValue.value === 'WEEKENDS'
                          ) {
                            this.setState({ days: [value[value.length - 1]] });
                            this.blurActiveElement();
                          } else {
                            this.setState({
                              days: filter(value, (day) => {
                                if (day.value !== 'ALL' && day.value !== 'WEEKDAYS' && day.value !== 'WEEKENDS') {
                                  return day;
                                }
                              }),
                            });
                          }
                        }} />
                    </Col>
                  </Col>
                  <Col className="pt-lg" sm={3} lg={3} md={3} xs={12}>
                    <Col className="no-padding" sm={12} lg={12} md={12} xs={12}>
                      <h4 className="time-picker-title">
                        <FormattedMessage id="engage.event.timepickermodal.starttime" />
                      </h4>
                    </Col>
                    <Col className="no-padding" sm={12} lg={12} md={12} xs={12}>
                      <TimePicker
                        defaultValue={this.state.startTime}
                        showSecond={false}
                        onChange={(value) => {
                          this.setState({ startTime: value });
                        }} />
                    </Col>
                  </Col>
                  <Col className="pt-lg" sm={3} lg={3} md={3} xs={12}>
                    <Col className="no-padding" sm={12} lg={12} md={12} xs={12}>
                      <h4 className="time-picker-title">
                        <FormattedMessage id="engage.event.timepickermodal.endtime" />
                      </h4>
                    </Col>
                    <Col className="no-padding" sm={12} lg={12} md={12} xs={12}>
                      <TimePicker
                        defaultValue={this.state.endTime}
                        showSecond={false}
                        onChange={value => this.setState({ endTime: value })} />
                    </Col>
                  </Col>
                  <Col className="pt-lg" sm={2} lg={2} md={2} xs={12}>
                    <button className="btn btn-primary add-time-button" onClick={this.saveDate}>
                      <FormattedMessage id="engage.event.timepickermodal.add" />
                    </button>
                  </Col>
                  <Col className="text-center mt-lg" sm={12} lg={12} md={12} xs={12}>
                    <h4><FormattedMessage id="engage.event.timepickermodal.selecteddates" /></h4>
                  </Col>
                  <Col className="pt-xl" sm={12} lg={12} md={12} xs={12}>
                    <Table className="table-responsive" striped bordered condensed hover>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><FormattedMessage id="engage.event.timepickermodal.daterange" /></th>
                          <th><FormattedMessage id="engage.event.timepickermodal.hoursrange" /></th>
                          <th><FormattedMessage id="engage.event.timepickermodal.removedate" /></th>
                        </tr>
                      </thead>
                      <tbody>
                        {dateOutput}
                      </tbody>
                    </Table>
                  </Col>
                </Row>
              </Modal.Body>
              <Modal.Footer>
                <button className="btn btn-primary" onClick={this.hideModal}>
                  <FormattedMessage id="engage.event.timepickermodal.apply" />
                </button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

TimePickerModal.defaultProps = {
  placeholder: '',
  initialValues: [],
  backdrop: false,
};

TimePickerModal.propTypes = {
  placeholder: PropTypes.string,
  initialValues: PropTypes.array,
  backdrop: PropTypes.bool,
  saveTimeAndDate: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(connect(null, { saveTimeAndDate })(TimePickerModal));
