import React, { Component } from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import Tab from 'react-bootstrap/lib/Tab';
import Tabs from 'react-bootstrap/lib/Tabs';
import Modal from 'react-bootstrap/lib/Modal';
import Col from 'react-bootstrap/lib/Col';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import filter from 'lodash/filter';
import { connect } from 'react-redux';
import { saveSegmentationData } from '../actions/campaignActions';
import { format } from '../components/Formatters/Utils';

let shouldCalculate = false;

class SegmentationModal extends Component {
  constructor(props) {
    super(props);

    const { data, initialValues } = this.props;

    map(initialValues, (value) => {
      map(data.data.segmentCategories, (segmentCategory) => {
        map(segmentCategory.profiles, (profile) => {
          if (profile.id === value.value) value.key = profile.name;
        });
      });
    });

    const widgetData = map(data.data.segmentCategories, value => ({
      title: value.segmentCategory,
      data: map(value.profiles, segmentValue => ({
        name: 'Profile',
        logicalOperator: 'NA',
        key: segmentValue.name,
        value: segmentValue.id,
        count: segmentValue.totalDeviceCount,
      })),
    }));

    map(initialValues, value =>
      map(widgetData, object => ({
        title: object.title,
        data: map(object.data, (obj) => {
          if (obj.value === value.value) {
            obj.logicalOperator = value.logicalOperator;
          }
          return obj;
        }),
      })),
    );

    this.state = {
      selectedValues: initialValues,
      values: widgetData,
      modalActive: false,
      selectedApplicationTab: data.data.segmentCategories[0].segmentCategory,
    };

    this.select = this.select.bind(this);
    this.unSelect = this.unSelect.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.saveSegmentationData = this.saveSegmentationData.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.selectedValues !== nextState.selectedValues) {
      this.saveSegmentationData(nextState.selectedValues);
      shouldCalculate = true;
    }
    return true;
  }

  showModal() {
    this.setState({ show: true });
    this.props.changeShowStatus(true);
  }

  hideModal() {
    if (shouldCalculate) {
      this.props.calculateAudience();
      shouldCalculate = false;
    }
    this.setState({ show: false });
    this.props.changeShowStatus(false);
  }

  saveSegmentationData(selectedValues) {
    const resultObj = [];
    map(selectedValues, (value) => {
      resultObj.push({ name: value.name, value: value.value, logicalOperator: value.logicalOperator });
    });

    this.props.saveSegmentationData(resultObj);
  }

  select(value, activeType) {
    this.setState({
      values: map(this.state.values, data => ({
        title: data.title,
        data: map(data.data, (obj) => {
          if (obj.value === value) {
            if (obj.logicalOperator === 'NA') this.setState({ selectedValues: [...this.state.selectedValues, obj] });
            else if (obj.logicalOperator !== activeType) {
              const newSelectedList = map(this.state.selectedValues, (selectedValue) => {
                if (selectedValue.value === value) {
                  selectedValue.logicalOperator = activeType;
                }

                return selectedValue;
              });

              this.setState({
                selectedValues: newSelectedList,
              });
            }
            obj.logicalOperator = activeType;
          }
          return obj;
        }),
      })),
    });
  }

  handleSelect(key) {
    this.setState({ selectedApplicationTab: key });
  }

  unSelect(event, value) {
    event.preventDefault();

    if (event.stopPropagation) event.stopPropagation();

    this.setState({
      values: map(this.state.values, data => ({
        title: data.title,
        data: map(data.data, (obj) => {
          if (obj.value === value) {
            obj.logicalOperator = 'NA';
          }
          return obj;
        }),
      })),
    });

    this.setState({
      selectedValues: filter(this.state.selectedValues, (object) => {
        if (object.value !== value) {
          return object;
        }
      }),
    });
  }

  render() {
    const { backdrop, placeholder } = this.props;
    return (
      <div className="custom--multi" onClick={this.showModal}>
        <div className="custom-select-control">
          <span className="custom-multi-value-wrapper">
            {this.state.selectedValues.length === 0
              ? <div className="custom-placeholder">
                {placeholder}
              </div>
              : map(this.state.selectedValues, object =>
                (<div key={Math.random()} className="custom-select-value">
                  <span
                    className="custom-select-value-icon"
                    aria-hidden="true"
                    onClick={event => this.unSelect(event, object.value)}>
                      x
                    </span>
                  <span className="custom-select-value-label" onClick={this.showModal}>
                    {object.key}
                  </span>
                </div>),
                )}
          </span>
          <div className="dropdown-content" id="modal">
            <Modal show={this.state.show} onHide={this.hideModal} backdrop={backdrop}>
              <Modal.Header className="text-center">
                <Modal.Title>
                  <FormattedMessage id="engage.audience.segmentationmodal.title" />
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Tabs
                  className="equal-width-tabs"
                  activeKey={this.state.selectedApplicationTab}
                  onSelect={this.handleSelect}
                  id="segmentation-modal"
                  mountOnEnter>
                  {map(this.state.values, (value) => {
                    const title = value.title;
                    const segmentationData = value.data;
                    return (
                      <Tab key={title} eventKey={title} title={title} className="mt-xl" unmountOnExit animation={false}>
                        {map(segmentationData, segmentationValue =>
                          (<div key={Math.random()}>
                            <Col lg={6} xs={12} md={6} sm={6} className="no-padding mb-lg">
                              <div value={segmentationValue.key}>
                                {segmentationValue.key}
                                &nbsp;(
                                {format(segmentationValue.count)}
                                &nbsp;
                                <em className="fa fa-user" />
                                )
                              </div>
                            </Col>
                            <Col lg={6} xs={12} md={6} sm={6} className="no-padding mb-lg">
                              <div data-toggle="buttons" className="btn-group button-group-align">
                                <label
                                  className={`btn btn-primary btn-outline btn-sm w45
                                    ${segmentationValue.logicalOperator === 'AND' ? 'active' : ''}`}
                                  onClick={event => this.select(segmentationValue.value, 'AND', event)}>
                                  <input id="option1" type="radio" name="options" />
                                  AND
                                </label>
                                <label
                                  className={`btn btn-primary btn-outline btn-sm w45
                                    ${segmentationValue.logicalOperator === 'NA' ? 'active' : ''}`}
                                  onClick={event => this.unSelect(event, segmentationValue.value)}>
                                  <input id="option2" type="radio" name="options" />
                                  N/A
                                </label>
                                <label
                                  className={`btn btn-primary btn-outline btn-sm w45
                                    ${segmentationValue.logicalOperator === 'OR' ? 'active' : ''}`}
                                  onClick={event => this.select(segmentationValue.value, 'OR', event)}>
                                  <input id="option3" type="radio" name="options" />OR
                                </label>
                              </div>
                            </Col>
                            <div className="clearer" />
                          </div>),
                        )}
                      </Tab>
                    );
                  })}
                </Tabs>
              </Modal.Body>
              <Modal.Footer>
                <button className="btn btn-primary" onClick={this.hideModal}>
                  <FormattedMessage id="engage.audience.segmentationmodal.apply" />
                </button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

SegmentationModal.defaultProps = {
  backdrop: true,
  placeholder: '',
  initialValues: [],
};

SegmentationModal.propTypes = {
  data: PropTypes.object.isRequired,
  initialValues: PropTypes.array,
  backdrop: PropTypes.bool,
  placeholder: PropTypes.string,
  saveSegmentationData: PropTypes.func.isRequired,
  calculateAudience: PropTypes.func.isRequired,
  changeShowStatus: PropTypes.func.isRequired,
};

export default injectIntl(
  connect(null, {
    saveSegmentationData,
    format,
  })(SegmentationModal),
);
