import React, { Component } from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import isEqual from 'lodash/isEqual';
import isNumber from 'lodash/isNumber';
import isEmpty from 'lodash/isEmpty';
import { injectIntl, FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { reset } from 'redux-form/lib/actions';
import TileLayer from 'react-leaflet/lib/TileLayer';
import FeatureGroup from 'react-leaflet/lib/FeatureGroup';
import Circle from 'react-leaflet/lib/Circle';
import { EditControl } from 'react-leaflet-draw';
import ReactTable from 'react-table';
import Map from 'react-leaflet/lib/Map';
import Dropzone from 'react-dropzone';
import parse from 'csv-parse/lib/sync';
import SweetAlert from 'react-bootstrap-sweetalert';
import { saveMapGeofences, saveFormGeofences, saveCsvGeofences } from '../actions/campaignActions';
import GeofenceForm from '../components/Forms/GeofenceForm';

class GeofenceModal extends Component {
  constructor(props) {
    super(props);

    const { initialMapValues, initialFormValues, initialCsvValues } = this.props;

    this.state = {
      modalActive: false,
      center: {
        lat: 41.023945,
        lng: 29.013691,
      },
      selectedMapCoordinates: initialMapValues,
      selectedFormCoordinates: initialFormValues,
      selectedCsvCoordinates: initialCsvValues,
      geofences: [],
      zoom: 11,
      alert: null,
    };

    this.unSelectMap = this.unSelectMap.bind(this);
    this.unSelectForm = this.unSelectForm.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.onCreate = this.onCreate.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onMounted = this.onMounted.bind(this);
    this.editMapTable = this.editMapTable.bind(this);
    this.editFormTable = this.editFormTable.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.csvValidation = this.csvValidation.bind(this);
    this.unSelectCsv = this.unSelectCsv.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!isEqual(this.state.selectedMapCoordinates, nextState.selectedMapCoordinates)) {
      this.props.saveMapGeofences(nextState.selectedMapCoordinates);
    }
    if (!isEqual(this.state.selectedFormCoordinates, nextState.selectedFormCoordinates)) {
      this.props.saveFormGeofences(nextState.selectedFormCoordinates);
    }
    if (!isEqual(this.state.selectedCsvCoordinates, nextState.selectedCsvCoordinates)) {
      this.props.saveCsvGeofences(nextState.selectedCsvCoordinates);
    }
    return true;
  }

  onDelete(e) {
    map(e.layers._layers, (value) => {
      const filteredList = this.state.selectedMapCoordinates.filter((item) => {
        if (value._leaflet_id !== item.circle.id) return item;
      });
      this.setState({
        selectedMapCoordinates: filteredList,
      });
      return value;
    });
  }

  onCreate(e) {
    this.setState({
      selectedMapCoordinates: this.state.selectedMapCoordinates.concat({
        circle: {
          id: e.layer._leaflet_id,
          coordinates: e.layer._latlng,
          radius: e.layer._mRadius,
        },
      }),
    });
  }

  onEdit(e) {
    let updatedOldValues = this.state.oldValues;
    let updatedList = this.state.selectedMapCoordinates;
    map(e.layers._layers, (value) => {
      updatedList = updatedList.map((item) => {
        if (value._leaflet_id === item.circle.id) {
          return {
            circle: {
              id: value._leaflet_id,
              coordinates: value._latlng,
              radius: value._mRadius,
            },
          };
        }
        return item;
      });

      updatedOldValues = updatedOldValues.map((item) => {
        if (value._leaflet_id === item.circle.id) {
          return {
            circle: {
              id: value._leaflet_id,
              coordinates: value._latlng,
              radius: value._mRadius,
            },
          };
        }
        return item;
      });
      return value;
    });
    this.setState({
      selectedMapCoordinates: updatedList,
      oldValues: updatedOldValues,
    });
  }

  onMounted(e) {
    const updatedList = map(e.options.edit.featureGroup._layers, value => ({
      circle: {
        id: value._leaflet_id,
        coordinates: value._latlng,
        radius: value._mRadius,
      },
    }));
    this.setState({
      selectedMapCoordinates: updatedList,
      oldValues: updatedList,
    });
  }

  unSelectCsv(event, value) {
    event.preventDefault();

    if (event.stopPropagation) event.stopPropagation();

    const filteredList = this.state.selectedCsvCoordinates.filter((item) => {
      if (value !== item.circle.id) return item;
    });

    this.setState({
      selectedCsvCoordinates: filteredList,
    });
  }

  unSelectMap(event, value) {
    event.preventDefault();

    if (event.stopPropagation) event.stopPropagation();

    const filteredList = this.state.selectedMapCoordinates.filter((item) => {
      if (value !== item.circle.id) return item;
    });

    this.setState({
      selectedMapCoordinates: filteredList,
    });
  }

  unSelectForm(event, value) {
    event.preventDefault();

    if (event.stopPropagation) event.stopPropagation();

    const filteredList = this.state.selectedFormCoordinates.filter((item) => {
      if (value !== item.circle.id) return item;
    });

    this.setState({
      selectedFormCoordinates: filteredList,
    });
  }

  hideModal() {
    this.setState({ show: false, alert: '' });
  }

  showModal() {
    this.setState({ oldValues: this.state.selectedMapCoordinates });
    this.setState({ show: true });
  }

  editMapTable(cellInfo) {
    if (cellInfo.column.id === 'radius') {
      return (
        <div
          contentEditable
          suppressContentEditableWarning
          onBlur={(e) => {
            const data = [...this.state.selectedMapCoordinates];
            data[cellInfo.index].circle[cellInfo.column.id] = Number(e.target.textContent);
            if (isNumber(Number(e.target.textContent))) {
              this.setState({ selectedMapCoordinates: data });
              this.props.saveMapGeofences(data);
            }
          }}>
          {Number(this.state.selectedMapCoordinates[cellInfo.index].circle[cellInfo.column.id]).toFixed()}
        </div>
      );
    }
    return (
      <div
        contentEditable
        suppressContentEditableWarning
        onBlur={(e) => {
          const data = [...this.state.selectedMapCoordinates];
          data[cellInfo.index].circle.coordinates[cellInfo.column.id] = Number(e.target.textContent);
          if (isNumber(Number(e.target.textContent))) {
            this.setState({ selectedMapCoordinates: data });
            this.props.saveMapGeofences(data);
          }
        }}>
        {Number(this.state.selectedMapCoordinates[cellInfo.index].circle.coordinates[cellInfo.column.id]).toFixed(6)}
      </div>
    );
  }

  editFormTable(cellInfo) {
    if (cellInfo.column.id === 'id') {
      return (
        <div
          contentEditable
          suppressContentEditableWarning
          onBlur={(e) => {
            const data = [...this.state.selectedFormCoordinates];
            data[cellInfo.index].circle[cellInfo.column.id] = e.target.textContent;
            this.setState({ selectedFormCoordinates: data });
            this.props.saveFormGeofences(data);
          }}>
          {this.state.selectedFormCoordinates[cellInfo.index].circle[cellInfo.column.id].toString()}
        </div>
      );
    } else if (cellInfo.column.id === 'radius') {
      return (
        <div
          contentEditable
          suppressContentEditableWarning
          onBlur={(e) => {
            const data = [...this.state.selectedFormCoordinates];
            data[cellInfo.index].circle[cellInfo.column.id] = Number(e.target.textContent);
            if (isNumber(Number(e.target.textContent))) {
              this.setState({ selectedFormCoordinates: data });
              this.props.saveFormGeofences(data);
            }
          }}>
          {Number(this.state.selectedFormCoordinates[cellInfo.index].circle[cellInfo.column.id]).toFixed()}
        </div>
      );
    }
    return (
      <div
        contentEditable
        suppressContentEditableWarning
        onBlur={(e) => {
          const data = [...this.state.selectedFormCoordinates];
          data[cellInfo.index].circle.coordinates[cellInfo.column.id] = Number(e.target.textContent);
          if (isNumber(Number(e.target.textContent))) {
            this.setState({ selectedMapCoordinates: data });
            this.props.saveFormGeofences(data);
          }
        }}>
        {Number(this.state.selectedFormCoordinates[cellInfo.index].circle.coordinates[cellInfo.column.id]).toFixed(6)}
      </div>
    );
  }

  handleFormSubmit(formProps) {
    this.setState({
      selectedFormCoordinates: this.state.selectedFormCoordinates.concat({
        ...this.state.selectedFormCoordinates.circle,
        circle: {
          id: formProps.name,
          coordinates: { lat: Number(formProps.latitude), lng: Number(formProps.longitude) },
          radius: Number(formProps.radius),
        },
      }),
    });
    this.props.reset('geofenceForm');
  }

  findDuplicates(data) {
    const result = [];

    data.forEach((element, index) => {
      if (!isEmpty(element) && data.indexOf(element, index + 1) > -1) {
        if (result.indexOf(element) === -1) {
          result.push(element);
        }
      }
    });

    return result;
  }

  csvValidation(data) {
    const { intl } = this.props;
    const errormsg = [];
    for (let i = 0; i < data.length; i += 1) {
      const value = data[i];
      const placeName = value.circle.id;
      const latitude = value.circle.coordinates.lat;
      const longitude = value.circle.coordinates.lng;
      const radius = value.circle.radius;

      if (isEmpty(placeName)) {
        errormsg.push(
          `  ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1}  ${intl.formatMessage({ id: 'engage.event.geofencemodal.identifierEmpty' })}`,
        );
        break;
      } else if (placeName.length > 16) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1} ${intl.formatMessage({
              id: 'engage.event.geofencemodal.placename',
            })} ${intl.formatMessage({
              id: 'engage.event.geofencemodal.identifierMaxWarning',
            })}`,
        );
        break;
      }

      if (isEmpty(latitude)) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1} ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.latitudeEmpty' })}`,
        );
        break;
      } else if (!/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/i.test(latitude)) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1} ${intl.formatMessage({
              id: 'engage.event.geofencemodal.csverror.latitudeInvalid',
            })}`,
        );
        break;
      }
      if (isEmpty(longitude)) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1} ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.longitudeEmpty' })}`,
        );
        break;
      } else if (
        !/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/i.test(longitude)
      ) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1}  ${intl.formatMessage({
              id: 'engage.event.geofencemodal.csverror.longitudeInvalid',
            })}`,
        );
        break;
      }
      if (isEmpty(radius)) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1}  ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.radiusEmpty' })} `,
        );
        break;
      }

      const radiusValue = Number(radius);
      if (isNaN(radiusValue)) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1} ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.radiusInvalid' })}`,
        );
        break;
      }
      if (radiusValue > 1000) {
        errormsg.push(
          ` ${intl.formatMessage({ id: 'engage.event.geofencemodal.csverror.rowID' })} :  ${i +
            1} ${intl.formatMessage({
              id: 'engage.event.geofencemodal.csverror.radiusMaxWarning',
            })}`,
        );
        break;
      }
    }

    if (errormsg.length === 0) {
      const placeNames = data.map(value => value.circle.id);
      const dublicateValues = this.findDuplicates(placeNames);
      if (dublicateValues.length > 0) {
        errormsg.push(
          ` ${dublicateValues} : ${intl.formatMessage({
            id: 'engage.event.geofencemodal.csverror.identifierDuplicateError',
          })}`,
        );
      }
    }

    if (errormsg.length > 0) {
      this.setState({
        alert: (
          <SweetAlert
            danger
            title={intl.formatMessage({ id: 'engage.offer.message.errorcsvuploadtitle' })}
            confirmBtnText={intl.formatMessage({ id: 'alert.positive' })}
            onConfirm={() => this.setState({ alert: null })}>
            <div
              style={{
                overflowY: 'scroll',
                maxHeight: '400px',
              }}>
              {map(errormsg, value => (
                <div key={Math.random()}>
                  {value}
                  <br />
                </div>
              ))}
            </div>
          </SweetAlert>
        ),
      });
      return false;
    }

    return true;
  }

  handleFileChange(files, rejectedFile) {
    const { intl } = this.props;
    let errorMessage = '';
    if (isEmpty(rejectedFile)) {
      if (isEmpty(files) || (!(files instanceof Array) || files[0].name.split('.')[1] !== 'csv')) {
        errorMessage = <FormattedMessage id="engage.offer.message.errorcsvuploadmessage" />;
      } else {
        const reader = new FileReader();
        const scope = this;
        reader.onload = function () {
          const parsed = parse(reader.result);
          const geofenceObject = [];
          map(parsed, (value) => {
            const circle = {
              circle: {
                id: value[0].trim(),
                coordinates: { lat: value[1].trim(), lng: value[2].trim() },
                radius: value[3].trim(),
              },
            };
            geofenceObject.push(circle);
          });

          if (scope.csvValidation(geofenceObject)) {
            map(geofenceObject, (value) => {
              value.circle.coordinates.lat = Number(value.circle.coordinates.lat);
              value.circle.coordinates.lng = Number(value.circle.coordinates.lng);
              value.circle.radius = Number(value.circle.radius);
            });
            scope.setState({ selectedCsvCoordinates: geofenceObject });
          }
        };

        return reader.readAsText(files[0]);
      }
    } else {
      errorMessage = <FormattedMessage id="engage.offer.message.errorcsvuploadmessage" />;
    }

    this.setState({
      alert: (
        <SweetAlert
          danger
          title={intl.formatMessage({ id: 'engage.offer.message.errorcsvuploadtitle' })}
          confirmBtnText={intl.formatMessage({ id: 'alert.positive' })}
          onConfirm={() => this.setState({ alert: null })}>
          {errorMessage}
        </SweetAlert>
      ),
    });
  }

  render() {
    const { placeholder, mapGeofences, formGeofences, csvGeofences, intl } = this.props;

    const position = [this.state.center.lat, this.state.center.lng];

    const mapColumns = [
      {
        Header: () => <div className="pull-left">#</div>,
        Cell: row => row.viewIndex + 1,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.latitude" />
          </div>
        ),
        id: 'lat',
        accessor: item => item.circle.coordinates.lat.toFixed(6),
        Cell: this.editMapTable,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.longitude" />
          </div>
        ),
        id: 'lng',
        accessor: item => item.circle.coordinates.lng.toFixed(6),
        Cell: this.editMapTable,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.radius" />
          </div>
        ),
        id: 'radius',
        accessor: item => item.circle.radius.toFixed(),
        Cell: this.editMapTable,
      },
    ];
    const formColumns = [
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.identifier" />
          </div>
        ),
        id: 'id',
        accessor: item => item.circle.id,
        Cell: this.editFormTable,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.latitude" />
          </div>
        ),
        id: 'lat',
        accessor: item => item.circle.coordinates.lat.toFixed(6),
        Cell: this.editFormTable,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.longitude" />
          </div>
        ),
        id: 'lng',
        accessor: item => item.circle.coordinates.lng.toFixed(6),
        Cell: this.editFormTable,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.radius" />
          </div>
        ),
        id: 'radius',
        accessor: item => item.circle.radius.toFixed(),
        Cell: this.editFormTable,
      },
      {
        Header: <FormattedMessage id="engage.event.geofencemodal.actions" />,
        id: 'deleteAction',
        accessor: item => item,
        Cell: row => (
          <Col className="text-center" lg={12} md={12} sm={12} xs={12}>
            <button
              type="button"
              onClick={event => this.unSelectForm(event, row.value.circle.id)}
              className="btn btn-default btn-xs btn-oval">
              <em className="fa fa-trash fa-1x text-muted" />
            </button>
          </Col>
        ),
      },
    ];

    const csvColumns = [
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.identifier" />
          </div>
        ),
        id: 'id',
        accessor: item => item.circle.id,
        Cell: row => <div>{row.value}</div>,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.latitude" />
          </div>
        ),
        id: 'lat',
        accessor: item => item.circle.coordinates.lat.toFixed(6),
        Cell: row => <div>{row.value}</div>,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.longitude" />
          </div>
        ),
        id: 'lng',
        accessor: item => item.circle.coordinates.lng.toFixed(6),
        Cell: row => <div>{row.value}</div>,
      },
      {
        Header: () => (
          <div className="pull-left">
            <FormattedMessage id="engage.event.geofencemodal.radius" />
          </div>
        ),
        id: 'radius',
        accessor: item => item.circle.radius.toFixed(),
        Cell: row => <div>{row.value}</div>,
      },
    ];

    return (
      <div className="custom--multi" onClick={this.showModal}>
        <div className="custom-select-control">
          <span className="custom-multi-value-wrapper">
            {mapGeofences.length === 0 && formGeofences.length === 0 && csvGeofences.length === 0 ? (
              <div className="custom-placeholder" style={{ color: 'red' }}>
                {placeholder}
              </div>
            ) : null}
            {mapGeofences.length === 0 ? null : (
              map(mapGeofences, object => (
                <div key={Math.random()} className="custom-select-value">
                  <span
                    className="custom-select-value-icon"
                    aria-hidden="true"
                    onClick={event => this.unSelectMap(event, object.circle.id)}>
                    x
                  </span>
                  <span className="custom-select-value-label" onClick={this.showModal}>
                    {object.circle.coordinates.lat} {object.circle.coordinates.lng}
                  </span>
                </div>
              ))
            )}
            {formGeofences.length === 0 ? null : (
              map(formGeofences, object => (
                <div key={Math.random()} className="custom-select-value">
                  <span
                    className="custom-select-value-icon"
                    aria-hidden="true"
                    onClick={event => this.unSelectForm(event, object.circle.id)}>
                    x
                  </span>
                  <span className="custom-select-value-label" onClick={this.showModal}>
                    {object.circle.coordinates.lat} {object.circle.coordinates.lng}
                  </span>
                </div>
              ))
            )}
            {csvGeofences.length === 0 ? null : (
              map(csvGeofences, object => (
                <div key={Math.random()} className="custom-select-value">
                  <span
                    className="custom-select-value-icon"
                    aria-hidden="true"
                    onClick={event => this.unSelectCsv(event, object.circle.id)}>
                    x
                  </span>
                  <span className="custom-select-value-label" onClick={this.showModal}>
                    {object.circle.coordinates.lat} {object.circle.coordinates.lng}
                  </span>
                </div>
              ))
            )}
          </span>
          <div className="dropdown-content" id="modal">
            <Modal show={this.state.show} onHide={this.hideModal} bsSize="large">
              <Modal.Header className="text-center">
                <Modal.Title>
                  <FormattedMessage id="engage.event.geofencemodal.title" />
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Tabs defaultActiveKey={1} justified id="tabID" mountOnEnter>
                  <Tab
                    eventKey={1}
                    title={intl.formatMessage({ id: 'engage.event.geofencemodal.tab1.title' })}
                    animation={false}>
                    <Row>
                      <Col sm={12} lg={12} md={12} xs={12}>
                        <Map center={position} zoom={this.state.zoom}>
                          <TileLayer
                            attribution="<a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a>"
                            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
                          <FeatureGroup>
                            <EditControl
                              position="topleft"
                              onCreated={this.onCreate}
                              onEdited={this.onEdit}
                              onDeleted={this.onDelete}
                              onMounted={this.onMounted}
                              draw={{
                                polyline: false,
                                polygon: false,
                                rectangle: false,
                                circlemarker: false,
                                circle: true,
                                marker: false,
                              }}>
                              {map(this.state.oldValues, selectedCoordinate => (
                                <Circle
                                  weight={4}
                                  opacity={0.5}
                                  center={[
                                    selectedCoordinate.circle.coordinates.lat,
                                    selectedCoordinate.circle.coordinates.lng,
                                  ]}
                                  radius={selectedCoordinate.circle.radius} />
                              ))}
                            </EditControl>
                          </FeatureGroup>
                        </Map>
                      </Col>
                      <Col sm={12} lg={12} md={12} xs={12}>
                        <ReactTable
                          className="-highlight"
                          data={this.state.selectedMapCoordinates}
                          columns={mapColumns}
                          previousText={intl.formatMessage({ id: 'react.table.previoustext' })}
                          nextText={intl.formatMessage({ id: 'react.table.nexttext' })}
                          loadingText={intl.formatMessage({ id: 'react.table.loadingtext' })}
                          noDataText={intl.formatMessage({ id: 'react.table.nodatatext' })}
                          pageText={intl.formatMessage({ id: 'react.table.pagetext' })}
                          ofText={intl.formatMessage({ id: 'react.table.oftext' })}
                          rowsText={intl.formatMessage({ id: 'react.table.rowstext' })}
                          defaultPageSize={5} />
                      </Col>
                    </Row>
                  </Tab>
                  <Tab
                    eventKey={2}
                    title={intl.formatMessage({ id: 'engage.event.geofencemodal.tab2.title' })}
                    animation={false}>
                    <Row>
                      <Col sm={12} lg={12} md={12} xs={12}>
                        <ReactTable
                          className="-highlight"
                          data={this.state.selectedFormCoordinates}
                          columns={formColumns}
                          previousText={intl.formatMessage({ id: 'react.table.previoustext' })}
                          nextText={intl.formatMessage({ id: 'react.table.nexttext' })}
                          loadingText={intl.formatMessage({ id: 'react.table.loadingtext' })}
                          noDataText={intl.formatMessage({ id: 'react.table.nodatatext' })}
                          pageText={intl.formatMessage({ id: 'react.table.pagetext' })}
                          ofText={intl.formatMessage({ id: 'react.table.oftext' })}
                          rowsText={intl.formatMessage({ id: 'react.table.rowstext' })}
                          defaultPageSize={10} />
                      </Col>
                      <Col sm={12} lg={12} md={12} xs={12} className="mt-lg pl0 pr0">
                        <GeofenceForm
                          onSubmit={this.handleFormSubmit}
                          geoData={this.state.selectedFormCoordinates}
                          intl={intl} />
                      </Col>
                    </Row>
                  </Tab>
                  <Tab
                    eventKey={3}
                    title={intl.formatMessage({ id: 'engage.event.geofencemodal.tab3.title' })}
                    animation={false}>
                    <Row>
                      <Col sm={12} lg={12} md={12} xs={12}>
                        <Dropzone
                          className="box-placeholder text-center"
                          accept="text/csv, application/vnd.ms-excel"
                          onDrop={this.handleFileChange}
                          multiple={false}>
                          <p>
                            <i className="fa fa-upload fa-fw" />
                            <FormattedMessage id="engage.offer.message.dropcsv" />
                            <br />
                            <br />
                            <FormattedMessage id="engage.offer.message.onlycsv" />
                            <br />
                            <FormattedMessage id="engage.offer.message.identifiermax" />
                            <br />
                            <FormattedMessage id="engage.offer.message.latituderange" />
                            <br />
                            <FormattedMessage id="engage.offer.message.longituderange" />
                            <br />
                            <FormattedMessage id="engage.offer.message.radiusrange" />
                            <br />
                            <Col style={{ marginTop: '15px' }} sm={12} lg={12} md={12} xs={12}>
                              <a className="tooltip-csv" href="#">
                                {intl.formatMessage({ id: 'engage.offer.message.csvexcelformat' })}
                                <span>
                                  <img
                                    width="500px"
                                    style={{ maxWwidth: '100%', height: 'auto' }}
                                    alt="csv excel format sample"
                                    src="../../img/csv_sample.png" />
                                </span>
                                <em style={{ marginLeft: '10px' }} className="fa fa-question-circle" />
                              </a>
                            </Col>
                            <br />
                          </p>
                        </Dropzone>
                      </Col>
                      <Col sm={12} lg={12} md={12} xs={12}>
                        <ReactTable
                          className="-highlight"
                          data={this.state.selectedCsvCoordinates}
                          columns={csvColumns}
                          previousText={intl.formatMessage({ id: 'react.table.previoustext' })}
                          nextText={intl.formatMessage({ id: 'react.table.nexttext' })}
                          loadingText={intl.formatMessage({ id: 'react.table.loadingtext' })}
                          noDataText={intl.formatMessage({ id: 'react.table.nodatatext' })}
                          pageText={intl.formatMessage({ id: 'react.table.pagetext' })}
                          ofText={intl.formatMessage({ id: 'react.table.oftext' })}
                          rowsText={intl.formatMessage({ id: 'react.table.rowstext' })}
                          defaultPageSize={5} />
                      </Col>
                    </Row>
                  </Tab>
                </Tabs>
              </Modal.Body>
              <Modal.Footer>
                <button className="btn btn-primary" onClick={this.hideModal}>
                  <FormattedMessage id="engage.event.geofencemodal.apply" />
                </button>
              </Modal.Footer>
              {this.state.alert}
            </Modal>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    mapGeofences: state.campaign.event.mapGeofences,
    formGeofences: state.campaign.event.formGeofences,
    csvGeofences: state.campaign.event.csvGeofences,
  };
}

GeofenceModal.defaultProps = {
  placeholder: '',
  initialMapValues: [],
  initialFormValues: [],
  initialCsvValues: [],
  mapGeofences: [],
  formGeofences: [],
  csvGeofences: [],
};

GeofenceModal.propTypes = {
  placeholder: PropTypes.string,
  initialMapValues: PropTypes.array,
  initialFormValues: PropTypes.array,
  initialCsvValues: PropTypes.array,
  saveMapGeofences: PropTypes.func.isRequired,
  saveFormGeofences: PropTypes.func.isRequired,
  saveCsvGeofences: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  mapGeofences: PropTypes.array,
  formGeofences: PropTypes.array,
  csvGeofences: PropTypes.array,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(
  connect(mapStateToProps, { saveMapGeofences, saveFormGeofences, saveCsvGeofences, reset })(GeofenceModal),
);
