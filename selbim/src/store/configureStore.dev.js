import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { apiMiddleware } from '../middleware';
import rootReducer from '../reducers';

const nextRootReducer = require('../reducers').default;

const logger = createLogger({
  collapsed: true,
});

export default function configureStore(preloadedState) {
  const store = createStore(rootReducer, preloadedState, compose(applyMiddleware(thunk, apiMiddleware, logger)));

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
