const prodEnv = require('./configureStore.prod');
const devEnv = require('./configureStore.dev');

if (process.env.NODE_ENV === 'production') {
  module.exports = prodEnv;
} else {
  module.exports = devEnv;
}
