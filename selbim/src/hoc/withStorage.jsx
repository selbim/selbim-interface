import React, { Component } from 'react';

export default function withStorage(storageKey, storageValue) {
  return function (WrappedComponent) {
    return class Storage extends Component {
      constructor(props) {
        super(props);
        this.setStorage = this.setStorage.bind(this);
        this.getStorage = this.getStorage.bind(this);
        const savedValue = this.getStorage(storageKey);
        if (savedValue === null || savedValue === 'undefined') {
          this.state = { [storageKey]: storageValue };
          localStorage.setItem(storageKey, storageValue);
        } else {
          const value = this.getStorage(storageKey) !== 'false';
          this.state = { [storageKey]: value };
        }
      }

      setStorage(k, v) {
        localStorage.setItem(k, v);
        this.setState({
          [k]: v,
        });
      }

      getStorage(k) {
        return localStorage.getItem(k);
      }

      render() {
        const props = Object.assign({}, this.props, { setStorage: this.setStorage });
        return <WrappedComponent {...props} {...this.state} />;
      }
    };
  };
}
