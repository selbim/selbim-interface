import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import Login from './components/Pages/Login';
import Logout from './components/Pages/Logout';
import Page404 from './components/Pages/Page404';
import Page500 from './components/Pages/Page500';
import Register from './components/Pages/Register';
import Recover from './components/Pages/Recover';
import Lock from './components/Pages/Lock';
import Maintenance from './components/Pages/Maintenance';
import VerifyToken from './components/Pages/VerifyToken';
import Dashboard from './components/Dashboard/Dashboard';
import Dashboard2 from './components/Dashboard/Dashboard2';
import Insights from './components/Insights/Insights';
import AppActivity from './components/Insights/AppActivity';
import BranchActivity from './components/Insights/BranchActivity';
import Competitors from './components/Insights/Competitors';
import Maps from './components/Insights/Maps';
import Trends from './components/Insights/Trends';
import { DefaultRoute, PrivateRoute } from './RouteContainer';
import Settings from './components/Settings/Settings';
import Terms from './components/Pages/Terms';
import Privacy from './components/Pages/Privacy';
import Documentation from './components/Integration/Documentation';
import GetSdk from './components/Integration/GetSdk';

const MainRouter = () =>
  (<BrowserRouter>
    <Switch>
      <PrivateRoute exact path="/" component={Dashboard} />
      <PrivateRoute path="/dashboard" component={Dashboard} />
      <PrivateRoute exact path="/insights" component={Insights} />
      <PrivateRoute path="/insights/app-activity" component={AppActivity} />
      <PrivateRoute path="/insights/branch-activity" component={BranchActivity} />
      <PrivateRoute path="/insights/competitors" component={Competitors} />
      <PrivateRoute path="/insights/maps" component={Maps} />
      <PrivateRoute path="/insights/trends" component={Trends} />
      <PrivateRoute path="/apps" component={Dashboard2} />
      <PrivateRoute path="/logout" component={Logout} />
      <PrivateRoute path="/settings" component={Settings} />
      <PrivateRoute path="/terms" component={Terms} />
      <PrivateRoute path="/privacy" component={Privacy} />
      <PrivateRoute path="/documentation" component={Documentation} />
      <PrivateRoute path="/getsdk" component={GetSdk} />
      <DefaultRoute path="/login" component={Login} />
      <DefaultRoute path="/register" component={Register} />
      <DefaultRoute path="/recover" component={Recover} />
      <DefaultRoute path="/500" component={Page500} />
      <DefaultRoute path="/lock" component={Lock} />
      <DefaultRoute path="/maintenance" component={Maintenance} />
      <DefaultRoute exact path="/check" component={VerifyToken} />
      <DefaultRoute component={Page404} />
    </Switch>
  </BrowserRouter>);

export default MainRouter;
